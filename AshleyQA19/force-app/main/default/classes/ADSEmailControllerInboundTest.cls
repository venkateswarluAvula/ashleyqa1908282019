@isTest
public class ADSEmailControllerInboundTest {
    @isTest static void testCases1() {
        string Recid;
        Account acc = new Account(Name='Test');
        insert acc;
        Legal_Review_Request__c LRR = new Legal_Review_Request__c();
        LRR.Account__c= acc.Id;
        LRR.CreatedById = userinfo.getUserId();
        insert LRR;

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        email.plainTextBody = 'Here is my plainText body of the email';
        email.fromAddress = 'noreply@gmail.com';
        Messaging.InboundEmail.BinaryAttachment[] binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[1];  
        Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
        Messaging.InboundEmail.textAttachment[] textAttachments = new Messaging.InboundEmail.textAttachment[1];  
        Messaging.InboundEmail.textAttachment textAttachment = new Messaging.InboundEmail.textAttachment();
        
        binaryAttachment.Filename = 'test.txt';
        textAttachment.Filename = 'test1.txt';
        string[] tA = new string[]{'abc@abc.com'};  
        email.toAddresses = tA;
        email.subject = 'RE: Sandbox: Email From Salesforce ADS Team RecId:' + LRR.Id + 'AccId:'+ acc.Id;
        string sub = email.subject;
        String algorithmName = 'HMacSHA1';
        Blob b = Crypto.generateMac(algorithmName, Blob.valueOf('test'),
        Blob.valueOf('test_key'));
        binaryAttachment.Body = b;
        textAttachment.Body = 'test body';
        binaryAttachments[0] =  binaryAttachment;
        email.binaryAttachments = binaryAttachments;
        textAttachments[0] =  textAttachment ;
        email.textAttachments = textAttachments ;
        env.fromAddress = 'noreply@email.com';
        
        email.plainTextBody = 'Here is my plainText body of the email';
        email.fromAddress = 'noreply@gmail.com';
        List<String> tA1 = new List<String>();
        tA.add('qw@gm.com');
        email.toAddresses = tA1;
        
        ADSEmailControllerInbound adsemail = new ADSEmailControllerInbound();
        adsemail.handleInboundEmail(email, env);
          
    }
    

}