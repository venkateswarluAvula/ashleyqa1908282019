public class NDDDeliveryDateAuthorization {
    public class token {
        public String accessToken;
        public String refreshToken;
    }
    public String accessTokenData()
    {
        String body = 'grant_type=password';
        
        body += '&username=' + EncodingUtil.urlEncode(Label.NDDAccesstokenUserName, 'UTF-8');
        body += '&password=' + EncodingUtil.urlEncode(Label.NDDAccesstokenPassword, 'UTF-8');
        
        String ePnt =  Label.NDDAccesstokenEndpoint;
        system.debug('ePnt'+ePnt);       
        Http http = new Http();
        HttpRequest httpReq = new HttpRequest();
        HttpResponse httpResp = new HttpResponse();
        httpReq.setEndpoint(ePnt);
        httpReq.setBody(body);
        system.debug('req-------'+httpReq.getBody());
        httpReq.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        httpReq.setMethod('POST');  
        httpResp = http.send(httpReq); 
        system.debug('resp-------'+httpResp.getBody());
        if(Test.isRunningTest()){
           return httpResp.getBody();
      }else{
              //oAuthJsonDataWrapper oaJson = (oAuthJsonDataWrapper)JSON.deserialize(httpResp.getBody(), oAuthJsonDataWrapper.class); 
            JSONParser parser = JSON.createParser(httpResp.getBody());
            parser.nextToken(); parser.nextToken();parser.nextToken();
             token obj = (token)parser.readValueAs(token.class);
            system.debug('obj.accessToken==>'+obj.accessToken);
            return obj.accessToken;
        }
    }

}