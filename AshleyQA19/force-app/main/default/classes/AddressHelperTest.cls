/* This is a test class for address helper class.*/
@isTest(SeeAllData=true)
public class AddressHelperTest {

    @isTest static void getCustomAddressTest() {
        List<Account> accList = new List<Account>();
        accList = TestDataFactory.initializeAccounts(1);
        insert accList;
        Id accId = accList[0].Id;

        List<Contact> contList = new List<Contact>();
        contList = TestDataFactory.initializeContacts(accId, 1);
        insert contList;

        Test.startTest();
        Map<string, id> addressMap = AddressHelper.getCustomAddress(accId);
        try {
	        AddressHelper.getAddressStr('sAdd1', 'sAdd2', 'sCity', 'sState', 'sZip', 'sType');
            AddressHelper.getCommaAddressStr('sAdd1', 'sAdd2', 'sCity', 'sState', 'sZip');
	        AddressHelper.getAddressId('sAdd1', 'sAdd2', 'sCity', 'sState', 'sZip', accId, addressMap, 'Test class');
        }
        catch(exception e) {}
        Test.stopTest();
    }

    @isTest static void getCustomAddressTest1() {
    	Id appAccId;
    	Address__c adrObj = new Address__c(); 
    	adrObj = [SELECT Id, AccountId__c, Address_Line_1__c, Address_Line_2__c, City__c, StateList__c, Zip_Code__c, Country__c, Address_Type__c, Preferred__c, LastModifiedDate FROM address__c WHERE AccountId__c != null LIMIT 1];
    	if (adrObj.AccountId__c != null) {
    		appAccId = adrObj.AccountId__c;
    	}

        Test.startTest();
        Map<string, id> addressMap = AddressHelper.getCustomAddress(appAccId);
        id adrId = AddressHelper.getAccountAddressId(appAccId);
        List<address__c> addressList = [SELECT Id, AccountId__c FROM address__c WHERE AccountId__c = :appAccId];
        Map<Id, List<Address__c>> custAddressMap = AddressHelper.getAddresses(new Set<string>{appAccId});
        try {
            if ((custAddressMap.size() > 0) && (custAddressMap.get(appAccId) != null)) {
                AddressHelper.getCustomerEDAAddress(appAccId, adrObj.Address_Line_1__c, adrObj.Address_Line_2__c, adrObj.City__c, adrObj.StateList__c, adrObj.Zip_Code__c, custAddressMap.get(appAccId), 'Test class');
                AddressHelper.getCustomerEDAAddress(appAccId, 'sAdd1', 'sAdd2', 'sCity', 'sState', 'sZip', custAddressMap.get(appAccId), 'Test class');
                AddressHelper.getCustomerEDAAddress(appAccId, 'sAdd1', 'sAdd2', 'sCity', 'sState', 'sZip', new list<Address__c>(), 'Test class');
                AddressHelper.getCustomerEDAAddress(appAccId, '', '', '', '', '', custAddressMap.get(appAccId), 'Test class');
            }
            AddressHelper.getAddressId('sAdd1', 'sAdd2', 'sCity', 'sState', 'sZip', appAccId, addressMap, 'Test class');
	        AddressHelper.getCustomerAddressWithType(appAccId, null);
	        AddressHelper.getCustomerAddressWithType(appAccId, adrId);
	        AddressHelper.setCustomerPreferredAddress(appAccId, adrId);
        }
        catch(exception e) {}
        Test.stopTest();
    }

}