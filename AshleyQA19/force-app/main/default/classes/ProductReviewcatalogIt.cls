public class ProductReviewcatalogIt {
        
        @AuraEnabled
        public integer reviewCount {get;set;} 
        @AuraEnabled
        public double averageRating {get;set;} 
        
    }