public class OpportunityHandlerController {


    public static void ADSZipandMrktRegnUpdate(List<Opportunity> OpportunityList) {
    
        /* Destination Logic Start*/
         Map<String, String> getDestZip = new Map<String, String>();
         Map<String, String> getDestMarkt = new Map<String, String>();
         Map<String, String> getDestRegion = new Map<String, String>();
         
         Map<String, String> getOrgnZip = new Map<String, String>();
         Map<String, String> getOrgnMarkt = new Map<String, String>();
         Map<String, String> getOrgnRegion = new Map<String, String>();
         
         List<ADS_Zip__c> lstDestADS = new List<ADS_Zip__c>();
         List<ADS_Zip__c> lstOrgADS = new List<ADS_Zip__c>();
         
         Set<String> destOppSet = new Set<String>();
         Set<String> orgnOppSet = new Set<String>();
         
         String destCityStateADS;
         String orgnCityStateADS;
         
         
       
        for (Opportunity opp : OpportunityList) {
                 
            destOppSet.add(opp.Destination_City__c+''+opp.Destination_State__c);
            System.debug('destOppSet-----------'+destOppSet);
            
            orgnOppSet.add(opp.Origin_City__c+''+opp.Origin_State__c);
            System.debug('orgnOppSet-----------'+orgnOppSet);
        }
        
         /* Destination logic Start*/  
        lstDestADS  =  [SELECT Id,Name,City__c,City_State_Code__c,Opportunity__c,State_Code__c,Zip_Code__c,Zip_Type__c, 
                    Region__c,Market__c FROM ADS_Zip__c Where City_State_Code__c =:destOppSet];
                    
            System.debug('lstDestADS --------'+lstDestADS.size()+'--------'+lstDestADS );
          
        if(!lstDestADS.isEmpty()){
            
            for(ADS_Zip__c objADS :lstDestADS){
                
                destCityStateADS = (objADS.City_State_Code__c).toUpperCase();
                System.debug('destCityStateADS --------'+destCityStateADS );
                
                getDestZip.put(destCityStateADS, objADS.Zip_Code__c);
                getDestMarkt.put(destCityStateADS, objADS.Market__c);
                getDestRegion.put(destCityStateADS, objADS.Region__c);
            }
        }
        /* Destination logic END*/
        
        /* Origin logic Start*/
        
        lstOrgADS  =  [SELECT Id,Name,City__c,City_State_Code__c,Opportunity__c,State_Code__c,Zip_Code__c,Zip_Type__c, 
                    Region__c,Market__c FROM ADS_Zip__c Where City_State_Code__c =:orgnOppSet];
                    
            System.debug('lstOrgADS --------'+lstOrgADS.size()+'--------'+lstOrgADS );
          
        if(!lstOrgADS.isEmpty()){
            
            for(ADS_Zip__c objADSOrg :lstOrgADS){
                
                orgnCityStateADS = (objADSOrg.City_State_Code__c).toUpperCase();
                System.debug('orgnCityStateADS --------'+orgnCityStateADS );
                
                getOrgnZip.put(orgnCityStateADS, objADSOrg .Zip_Code__c);
                getOrgnMarkt.put(orgnCityStateADS, objADSOrg .Market__c);
                getOrgnRegion.put(orgnCityStateADS, objADSOrg .Region__c);
            }
        }
        /* Origin logic END*/
        
        //update values from ADS to Oppty
        for(Opportunity oppty :OpportunityList) {
         
            String DestCtyStatOppty = (oppty.Destination_City__c+''+oppty.Destination_State__c).toUpperCase();
            String OrgnCtyStatOppty = (oppty.Origin_City__c+''+oppty.Origin_State__c).toUpperCase();
              
            System.debug('DestCtyStatOppty  --------'+DestCtyStatOppty  );             
            oppty.Destination_Zip_Code__c  = getDestZip.get(DestCtyStatOppty );
            oppty.Destination_Market__c = getDestMarkt.get(DestCtyStatOppty );
            oppty.Destination_Region__c = getDestRegion.get(DestCtyStatOppty );
            
            oppty.Origin_Zip_Code__c = getOrgnZip.get(OrgnCtyStatOppty);
            oppty.Origin_Market__c = getOrgnMarkt.get(OrgnCtyStatOppty);
            oppty.Origin_Region__c = getOrgnRegion.get(OrgnCtyStatOppty);
            
        }
        
      
     } 
   

}