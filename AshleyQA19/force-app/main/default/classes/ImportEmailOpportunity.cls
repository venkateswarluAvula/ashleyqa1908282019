global class ImportEmailOpportunity implements Messaging.InboundEmailHandler{
    global Static String MyFile;
    global Static String FromEmail;
    public Static String Subject;
    Boolean processSheet = True;
    List<ADS_Lane__c> LaneListtoInsert = new List<ADS_Lane__c>();
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        // List to collect all binary attachments from the email recevied.
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        FromEmail = email.fromAddress;
        System.debug('getting-->'+FromEmail);
        Subject = email.subject;
        for (Messaging.Inboundemail.BinaryAttachment tAttachment : email.binaryAttachments) {
            MyFile = tAttachment.Body.toString();
            System.debug('Attachment-->'+MyFile);
        }
        // CSV to JSON Conversion Start
        String Builder = '';
        Builder += '[';
        String[] Headers = new String[0];
        String[] Lines = MyFile.split('\n');
        System.debug('Lines-->'+Lines);
        for(Integer i = 0; i < Lines.size(); i++){
            String[] Values = Lines[i].replaceAll('\'', '').split(',');
            System.debug('Values-->'+Values);
            if (i == 0) //Index List
            {
                Headers = Values;
            } else {
                Builder += '{';
                for (Integer j = 0; j < Values.size() && j < Headers.size(); j++) {
                    String Jsonvalue = '\"' + Headers[j] + '\":\"' + Values[j] + '\"';
                    if (j != Values.size() - 1) { //If not last value of values...
                        Jsonvalue += ',';
                    }
                    Builder += Jsonvalue;
                    System.debug('Json-->'+Builder);
                }
                Builder += '}';
                if (i != Lines.size() - 1) {
                    Builder += ',';
                }
            }
        }
        Builder += ']';
        Builder = Builder.replaceAll('\n','').replaceAll('\r','').replaceAll('__c','');
        // CSV to JSON Conversion END
        System.debug('Converted JSON-->' + Builder);
        List<fieldWrapper> datalist = (List<fieldWrapper>) JSON.deserialize(Builder, List<fieldWrapper>.class);
        system.debug('Datalist-->' + datalist);
        system.debug('DatalistSize-->' + datalist.size());
        for(fieldWrapper wrapper: datalist)
        {
            System.debug('wrapper = ' + wrapper);
            ADS_Lane__c lane =new  ADS_Lane__c();
            if(wrapper.of_Trips_Per_Week != '' && wrapper.of_Trips_Per_Week != null && wrapper.of_Trips_Per_Week != 'undefined'){
                lane.of_Trips_Per_Week__c = Decimal.valueOf(wrapper.of_Trips_Per_Week);
            }
            if(wrapper.Awarded != '' && wrapper.Awarded != null && wrapper.Awarded != 'undefined'){
                lane.Awarded__c = Boolean.valueOf(wrapper.Awarded);
            }
            lane.ADS_Lane_Destination_City__c = wrapper.ADS_Lane_Destination_City;
            lane.Destination_Drop_Type__c = wrapper.Destination_Drop_Type;
            lane.ADS_Lane_Destination_Market__c = wrapper.ADS_Lane_Destination_Market;
            lane.Destination_Region__c = wrapper.Destination_Region;
            lane.ADS_Lane_Destination_State__c = wrapper.ADS_Lane_Destination_State;
            if(wrapper.ADS_Lane_Destination_Zip != 'undefined')
                lane.ADS_Lane_Destination_Zip__c = wrapper.ADS_Lane_Destination_Zip;
            else
                lane.ADS_Lane_Destination_Zip__c = null;
            if(wrapper.Minimum_Charge != '' && wrapper.Minimum_Charge != null && wrapper.Minimum_Charge != 'undefined'){
                lane.Minimum_Charge__c = Decimal.valueOf(wrapper.Minimum_Charge);
            }
            if(wrapper.Opportunity == ''){
                lane.Opportunity__c = CreateNewOpp().Id; 
            }else{
                lane.Opportunity__c = wrapper.Opportunity;
            }
            lane.ADS_Lane_Origin_City__c = wrapper.ADS_Lane_Origin_City;
            lane.Origin_Drop_Type__c = wrapper.Origin_Drop_Type;
            lane.ADS_Lane_Origin_Market__c = wrapper.ADS_Lane_Origin_Market;
            lane.Origin_Region__c = wrapper.Origin_Region;
            lane.ADS_Lane_Origin_State__c = wrapper.ADS_Lane_Origin_State;
            if(wrapper.ADS_Lane_Origin_Zip != 'undefined')
                lane.ADS_Lane_Origin_Zip__c = wrapper.ADS_Lane_Origin_Zip;
            else
                lane.ADS_Lane_Origin_Zip__c = null;
            if(wrapper.Id != null && wrapper.Id != '' && wrapper.Id != 'undefined'){
                lane.Id = Id.valueOf(wrapper.Id);
            }
            System.debug('lane : '+lane);
            System.debug('lane Opportunity__c : '+lane.Opportunity__c);
            LaneListtoInsert.add(lane);
        }
        if(LaneListtoInsert.size() > 0)
        {
            try {
                System.debug('Lane_List_to_Insert : '+LaneListtoInsert[0]);
                upsert LaneListtoInsert;
                System.debug('LaneListtoInsert : '+LaneListtoInsert[0].Id);
                QueueSobject Qso = new QueueSobject();
                Qso = [SELECT Id,QueueId,SobjectType,Queue.Name,Queue.Email FROM QueueSobject WHERE Queue.Name = 'ADS Lane Import'];
                String us = Qso.Queue.Email;
                System.debug('queue'+us);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new String[] {FromEmail});
                mail.setCcAddresses(new String[] {us});
                mail.setSubject(Subject);
                mail.setPlainTextBody('From Attachment '+LaneListtoInsert.size()+' Lane records are created.');
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                result.success = true;
            }
            catch(DMLException ex)
            {
                System.debug('Error in DML exception..' + ex);
            }
        }
        //}
        return result;
    }
    // Creating New Opportunity
    public static Opportunity CreateNewOpp(){
        System.debug('getting here-->'+FromEmail);
        Account existAcc = new Account();
        existAcc = CreateNewAcc();
        System.debug('Namess-->'+existAcc.Name);
        List<Opportunity> opp = new List<Opportunity>();
        opp = [SELECT Id, Name,AccountId,Account.Name, Contact__r.Email, Contact__r.Name 
               FROM Opportunity Where AccountId =:existAcc.id];
        Opportunity NewOpp = new Opportunity();
        NewOpp.StageName = 'Open' ;
        NewOpp.CloseDate = System.today().addDays(7);
        NewOpp.AccountId = existAcc.Id;
        if(opp.size() > 0){
            Integer oppNumber = opp.size();
            System.debug('fieldNumber Old-->'+oppNumber);
            oppNumber++;
            System.debug('Number New-->'+oppNumber);
            NewOpp.Name = existAcc.Name +'- Emailed Opportunity-' + oppNumber++; 
        }else{
            NewOpp.Name = 'Unknown Contact - Emailed Opportunity-' + '1'; 
        }
        
        Insert NewOpp;
        system.debug('Opportunities Id'+NewOpp.Id);
        return NewOpp;
    }
    // Creating New Dummy Account with Sender Email.
    public static Account CreateNewAcc(){
        System.debug('getting-->'+FromEmail);
        List<Account> accList = new List<Account>();
        accList = [SELECT Id,Name,Email__c,RecordType.Name From Account Where Email__c =:FromEmail AND RecordType.Name ='ADS'];
        List<Contact> Conmail = new List<Contact>();
        Conmail = [SELECT Id, Name, Email, Account.RecordType.Name, AccountId,Account.Name From Contact Where Email =:FromEmail AND Account.RecordType.Name ='ADS'];
        
        Account acc = new Account();
        if(Conmail.size()>0){
            acc.Id = Conmail[0].AccountId;
            system.debug('Account Id'+acc.Id);
            acc.Name = Conmail[0].Account.Name;
            System.debug('matching account---->'+acc.Name);
        }else{
            //get it from the custom label (dummy account)
            acc.Id = System.label.ADS_Dummy_Account;
            acc.Name = 'ADS Test';
        }
        system.debug('Account Id'+acc.Id);
        System.debug('matching account---->'+acc.Name);
        return acc;
        
    }
    public class fieldWrapper {
        public String of_Trips_Per_Week;
        public String Id;
        public String Name;
        public String Awarded;
        public String ADS_Lane_Destination_City;
        public String Destination_Drop_Type;
        public String ADS_Lane_Destination_Market;
        public String Destination_Region;
        public String ADS_Lane_Destination_State;
        public String ADS_Lane_Destination_Zip;
        public String Minimum_Charge;
        public String Opportunity;
        public String ADS_Lane_Origin_City;
        public String Origin_Drop_Type;
        public String ADS_Lane_Origin_Market;
        public String Origin_Region;
        public String ADS_Lane_Origin_State;
        public String ADS_Lane_Origin_Zip;
        
    }
}