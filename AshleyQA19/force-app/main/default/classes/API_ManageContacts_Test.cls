@isTest
public class API_ManageContacts_Test {
  @isTest
	static void contactupserttest(){
        
		Contact con= new Contact();
		con.FirstName ='Test';
        con.LastName = 'Example';
        
		insert con;
		
		API_ManageContacts.ManageContactWrapper acc = new API_ManageContacts.ManageContactWrapper();
		acc.Id      = con.Id;
		acc.FirstName    = 'NameTest';
        acc.LastName = 'Example';
		acc.Phone   = '1198785496';
		
        API_ManageContacts reqst=new API_ManageContacts();
        String myJSON = JSON.serialize(acc);
        Map<String,List<API_ManageContacts.ManageContactWrapper>> JSONreqBody = new Map<String,List<API_ManageContacts.ManageContactWrapper>>();
        List<API_ManageContacts.ManageContactWrapper> JSONm = new List<API_ManageContacts.ManageContactWrapper>();
        JSONm.add(acc);
        JSONreqBody.put('Contacts', JSONm);
        myJSON=JSON.serialize(JSONreqBody);

        RestRequest request = new RestRequest();
        request.requestUri ='https://ashley--kcsdev.lightning.force.com/services/apexrest/ManageContacts';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestContext.request = request;
        API_ManageContacts.CreateContact(acc);
        API_ManageContacts.doPost();
		
		
	}
}