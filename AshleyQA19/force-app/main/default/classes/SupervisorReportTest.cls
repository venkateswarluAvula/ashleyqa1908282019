@isTest
public class SupervisorReportTest {

    
    @isTest 
	public static void testMock1() {
        Profile pf= [Select Id from profile where Name='System Administrator']; 
        
        List<User> myuserslist = new List<User>();
        for(integer i=0;i<20;i++){
         User u = new User(Username='testuser1'+i+'@Ashley.com', firstname='test', LastName='Washburn', Email='awashburn@ashley.com', Alias='awash', 
                          CommunityNickname='test1'+i,LanguageLocaleKey='en_US', LocaleSidKey='en_US', TimeZoneSidKey='America/Chicago',
                          EmailEncodingKey='UTF-8', ProfileId = pf.Id,ManagerId = userinfo.getUserId());
        
        myuserslist.add(u);
        }
        insert myuserslist;
        List<Case> mycaselist = new List<Case>();
        Case ca = new Case(Status = 'New');
        mycaselist.add(ca);
        Case caif = new Case(Status = 'New');
         mycaselist.add(caif);
         Case ca1 = new Case(Status = 'Working');
        mycaselist.add(ca1);
        //insert ca1;
        Case caif1 = new Case(Status = 'Working');
        mycaselist.add(caif1);
       // insert caif1;
         Case ca2 = new Case(Status = 'Awaiting Customer Reply');
        mycaselist.add(ca2);
      //  insert ca2;
        Case ca2if = new Case(Status = 'Awaiting Customer Reply');
        mycaselist.add(ca2if);
       // insert ca2if;
           Case ca3 = new Case(Status = 'New Customer Reply');
        mycaselist.add(ca3);
        //insert ca3;
        Case caif3 = new Case(Status = 'New Customer Reply');
        mycaselist.add(caif3);
        //insert caif3;
         Case ca4 = new Case(Status = 'Sent to Store');
        mycaselist.add(ca4);
        //insert ca4;
        Case caif4 = new Case(Status = 'Sent to Store');
        mycaselist.add(caif4);
       // insert caif4;
           Case ca5 = new Case(Status = 'Ready for Review');
        mycaselist.add(ca5);
       // insert ca5;
        Case caif5 = new Case(Status = 'Ready for Review');
        mycaselist.add(caif5);
      //  insert caif5;
         Case ca6 = new Case(Status = 'Closed', Type_of_Resolution__c = 'test', Resolution_Notes__c = 'test');
        mycaselist.add(ca6);
       // insert ca6;
        Case caif6 = new Case(Status = 'Closed', Type_of_Resolution__c = 'test', Resolution_Notes__c = 'test');
        mycaselist.add(caif6);
       // insert caif6;
           Case ca7 = new Case(Status = 'Closed in Salesforce', Type_of_Resolution__c = 'test', Resolution_Notes__c = 'test');
       mycaselist.add(ca7);
       // insert ca7;
        Case caif7 = new Case(Status = 'Closed in Salesforce', Type_of_Resolution__c = 'test', Resolution_Notes__c = 'test');
       mycaselist.add(caif7);
       // insert caif7;
              Case ca8 = new Case(Status = 'Open');
        mycaselist.add(ca8);
       // insert ca8;
        Case caif8 = new Case(Status = 'Open');
        mycaselist.add(caif8);
       // insert caif8;
       // 
       insert mycaselist;
        
        List<Task> mytasklist = new List<Task>();
        
        Task tsk=new Task();
        tsk.Subject='call';
        tsk.calldurationInseconds=20;
        tsk.calltype='Inbound';
        tsk.OwnerId=myuserslist[0].Id;
        tsk.whatId = mycaselist[0].Id;
        
        Task tskif=new Task();
        tskif.Subject='call';
        tskif.calldurationInseconds=20;
        tskif.calltype='Inbound';
        tskif.OwnerId=myuserslist[0].Id;
        tskif.whatId = mycaselist[1].Id;
        
        Task tsktest=new Task();
        tsktest.Subject='call';
        tsktest.calldurationInseconds=20;
        tsktest.calltype='Outbound';
        tsktest.OwnerId=myuserslist[10].Id;
        tsktest.whatId = mycaselist[1].Id;
        
        Task tsktestif1=new Task();
        tsktestif1.Subject='call';
        tsktestif1.calldurationInseconds=20;
        tsktestif1.calltype='Outbound';
        tsktestif1.OwnerId=myuserslist[10].Id;
        tsktestif1.whatId = mycaselist[0].Id;
        
        
        
        mytasklist.add(tsktestif1);
        mytasklist.add(tsktest);
        mytasklist.add(tskif);
        mytasklist.add(tsk);
        
       
        
        Task tsk1=new Task();
        tsk1.Subject='call';
        tsk1.calldurationInseconds=20;
        tsk1.calltype='Inbound';
        tsk1.OwnerId=myuserslist[1].Id;
        tsk1.whatId = ca1.Id;
        
        mytasklist.add(tsk1);
        
        Task tsk1if=new Task();
        tsk1if.Subject='call';
        tsk1if.calldurationInseconds=20;
        tsk1if.calltype='Inbound';
        tsk1if.OwnerId=myuserslist[1].Id;
        tsk1if.whatId = caif1.Id;
        
        mytasklist.add(tsk1if);
        
        Task tsk10=new Task();
        tsk10.Subject='call';
        tsk10.calldurationInseconds=20;
        tsk10.calltype='Outbound';
        tsk10.OwnerId=myuserslist[11].Id;
        tsk10.whatId = ca1.Id;
        
        mytasklist.add(tsk10);
        
        Task tsk10if=new Task();
        tsk10if.Subject='call';
        tsk10if.calldurationInseconds=20;
        tsk10if.calltype='Outbound';
        tsk10if.OwnerId=myuserslist[11].Id;
        tsk10if.whatId = caif1.Id;
        
        mytasklist.add(tsk10if);
        
       
        Task tsk2=new Task();
        tsk2.Subject='call';
        tsk2.calldurationInseconds=20;
        tsk2.calltype='Inbound';
        tsk2.OwnerId=myuserslist[2].Id;
        tsk2.whatId = mycaselist[2].Id;
        
        mytasklist.add(tsk2);
        
        Task tsk2if=new Task();
        tsk2if.Subject='call';
        tsk2if.calldurationInseconds=20;
        tsk2if.calltype='Inbound';
        tsk2if.OwnerId=myuserslist[2].Id;
        tsk2if.whatId = mycaselist[3].Id;
        
        mytasklist.add(tsk2if);
                
        Task tsk20=new Task();
        tsk20.Subject='call';
        tsk20.calldurationInseconds=20;
        tsk20.calltype='Outbound';
        tsk20.OwnerId=myuserslist[12].Id;
        tsk20.whatId = mycaselist[2].Id;
        
        mytasklist.add(tsk20);
        
        Task tsk20if=new Task();
        tsk20if.Subject='call';
        tsk20if.calldurationInseconds=20;
        tsk20if.calltype='Outbound';
        tsk20if.OwnerId=myuserslist[12].Id;
        tsk20if.whatId = mycaselist[3].Id;
        
        mytasklist.add(tsk20if);
        
     
        
        Task tsk3=new Task();
        tsk3.Subject='call';
        tsk3.calldurationInseconds=20;
        tsk3.calltype='Inbound';
        tsk3.OwnerId=myuserslist[3].Id;
        tsk3.whatId = mycaselist[4].Id;
        
        mytasklist.add(tsk3);
        
        Task tsk3if=new Task();
        tsk3if.Subject='call';
        tsk3if.calldurationInseconds=20;
        tsk3if.calltype='Inbound';
        tsk3if.OwnerId=myuserslist[3].Id;
        tsk3if.whatId = mycaselist[5].Id;
        
        mytasklist.add(tsk3if);
        
        Task tsk30=new Task();
        tsk30.Subject='call';
        tsk30.calldurationInseconds=20;
        tsk30.calltype='Outbound';
        tsk30.OwnerId=myuserslist[13].Id;
        tsk30.whatId = mycaselist[4].Id;
        
        mytasklist.add(tsk30);
        
        Task tsk30if=new Task();
        tsk30if.Subject='call';
        tsk30if.calldurationInseconds=20;
        tsk30if.calltype='Outbound';
        tsk30if.OwnerId=myuserslist[13].Id;
        tsk30if.whatId = mycaselist[5].Id;
        
        mytasklist.add(tsk30if);
        
       
        
        Task tsk4=new Task();
        tsk4.Subject='call';
        tsk4.calldurationInseconds=20;
        tsk4.calltype='Inbound';
        tsk4.OwnerId=myuserslist[4].Id;
        tsk4.whatId = mycaselist[6].Id;
        
        mytasklist.add(tsk4);
        
        Task tsk4if=new Task();
        tsk4if.Subject='call';
        tsk4if.calldurationInseconds=20;
        tsk4if.calltype='Inbound';
        tsk4if.OwnerId=myuserslist[4].Id;
        tsk4if.whatId = mycaselist[7].Id;
        
        mytasklist.add(tsk4if);
        
        Task tsk40=new Task();
        tsk40.Subject='call';
        tsk40.calldurationInseconds=20;
        tsk40.calltype='Outbound';
        tsk40.OwnerId=myuserslist[14].Id;
        tsk40.whatId = mycaselist[6].Id;
        
        mytasklist.add(tsk40);
        
        Task tsk40if=new Task();
        tsk40if.Subject='call';
        tsk40if.calldurationInseconds=20;
        tsk40if.calltype='Outbound';
        tsk40if.OwnerId=myuserslist[14].Id;
        tsk40if.whatId = mycaselist[7].Id;
        
        mytasklist.add(tsk40if);
        
     
        
        Task tsk5=new Task();
        tsk5.Subject='call';
        tsk5.calldurationInseconds=20;
        tsk5.calltype='Inbound';
        tsk5.OwnerId=myuserslist[5].Id;
        tsk5.whatId = mycaselist[8].Id;
        
        mytasklist.add(tsk5);
        
        Task tsk5if=new Task();
        tsk5if.Subject='call';
        tsk5if.calldurationInseconds=20;
        tsk5if.calltype='Inbound';
        tsk5if.OwnerId=myuserslist[5].Id;
        tsk5if.whatId = mycaselist[9].Id;
        
        mytasklist.add(tsk5if);
        
        Task tsk50=new Task();
        tsk50.Subject='call';
        tsk50.calldurationInseconds=20;
        tsk50.calltype='Outbound';
        tsk50.OwnerId=myuserslist[15].Id;
        tsk50.whatId = mycaselist[8].Id;
        
        mytasklist.add(tsk50);
        
        Task tsk50if=new Task();
        tsk50if.Subject='call';
        tsk50if.calldurationInseconds=20;
        tsk50if.calltype='Outbound';
        tsk50if.OwnerId=myuserslist[15].Id;
        tsk50if.whatId = mycaselist[9].Id;
        
        mytasklist.add(tsk50if);
        
       
        
        Task tsk6=new Task();
        tsk6.Subject='call';
        tsk6.calldurationInseconds=20;
        tsk6.calltype='Inbound';
        tsk6.OwnerId=myuserslist[6].Id;
        tsk6.whatId = mycaselist[10].Id;
        
        mytasklist.add(tsk6);

        Task tsk6if=new Task();
        tsk6if.Subject='call';
        tsk6if.calldurationInseconds=20;
        tsk6if.calltype='Inbound';
        tsk6if.OwnerId=myuserslist[6].Id;
        tsk6if.whatId = mycaselist[11].Id;
        
        mytasklist.add(tsk6if);        
        
        Task tsk60=new Task();
        tsk60.Subject='call';
        tsk60.calldurationInseconds=20;
        tsk60.calltype='Outbound';
        tsk60.OwnerId=myuserslist[16].Id;
        tsk60.whatId = mycaselist[10].Id;
        
        mytasklist.add(tsk60);
        
        Task tsk60if=new Task();
        tsk60if.Subject='call';
        tsk60if.calldurationInseconds=20;
        tsk60if.calltype='Outbound';
        tsk60if.OwnerId=myuserslist[16].Id;
        tsk60if.whatId = mycaselist[11].Id;
        
        mytasklist.add(tsk60if);
        
     
        
        Task tsk7=new Task();
        tsk7.Subject='call';
        tsk7.calldurationInseconds=20;
        tsk7.calltype='Inbound';
        tsk7.OwnerId=myuserslist[7].Id;
        tsk7.whatId = mycaselist[12].Id;
        
        mytasklist.add(tsk7);
        
        Task tsk7if=new Task();
        tsk7if.Subject='call';
        tsk7if.calldurationInseconds=20;
        tsk7if.calltype='Inbound';
        tsk7if.OwnerId=myuserslist[7].Id;
        tsk7if.whatId = mycaselist[13].Id;
        
        mytasklist.add(tsk7if);
        
        Task tsk70=new Task();
        tsk70.Subject='call';
        tsk70.calldurationInseconds=20;
        tsk70.calltype='Outbound';
        tsk70.OwnerId=myuserslist[17].Id;
        tsk70.whatId = mycaselist[12].Id;
        
        mytasklist.add(tsk70);
        
        Task tsk70if=new Task();
        tsk70if.Subject='call';
        tsk70if.calldurationInseconds=20;
        tsk70if.calltype='Outbound';
        tsk70if.OwnerId=myuserslist[17].Id;
        tsk70if.whatId = mycaselist[13].Id;
        
        mytasklist.add(tsk70if);
        
  
        
        Task tsk8=new Task();
        tsk8.Subject='call';
        tsk8.calldurationInseconds=20;
        tsk8.calltype='Inbound';
        tsk8.OwnerId=myuserslist[8].Id;
        tsk8.whatId = mycaselist[14].Id;
        
        mytasklist.add(tsk8);
        
        Task tsk8if=new Task();
        tsk8if.Subject='call';
        tsk8if.calldurationInseconds=20;
        tsk8if.calltype='Inbound';
        tsk8if.OwnerId=myuserslist[8].Id;
        tsk8if.whatId = mycaselist[15].Id;
        
        mytasklist.add(tsk8if);
        
        Task tsk80=new Task();
        tsk80.Subject='call';
        tsk80.calldurationInseconds=20;
        tsk80.calltype='Outbound';
        tsk80.OwnerId=myuserslist[18].Id;
        tsk80.whatId = mycaselist[14].Id;
        
        mytasklist.add(tsk80);
        
        Task tsk80if=new Task();
        tsk80if.Subject='call';
        tsk80if.calldurationInseconds=20;
        tsk80if.calltype='Outbound';
        tsk80if.OwnerId=myuserslist[18].Id;
        tsk80if.whatId = mycaselist[15].Id;
        
        mytasklist.add(tsk80if);
        
        Task tsk81=new Task();
        tsk81.Subject='call';
        tsk81.calldurationInseconds=0;
        tsk81.calltype='Outbound';
        tsk81.OwnerId=myuserslist[9].Id;
        tsk81.whatId = mycaselist[14].Id;
        mytasklist.add(tsk81);
        
        Task tsk81if=new Task();
        tsk81if.Subject='call';
        tsk81if.calldurationInseconds=0;
        tsk81if.calltype='Outbound';
        tsk81if.OwnerId=myuserslist[9].Id;
        tsk81if.whatId = mycaselist[14].Id;
        mytasklist.add(tsk81if);
        
        insert mytasklist;
        
        Test.StartTest();
        SupervisorReport.getData();
        SupervisorReport.getspecificUserData('test');
        Test.StopTest();
        
    }
    
@isTest 
	public static void testMock2() {
        Profile pf= [Select Id from profile where Name='System Administrator']; 
        
        List<User> myuserslist = new List<User>();
        for(integer i=0;i<20;i++){
         User u = new User(Username='testuser2'+i+'@Ashley.com', firstname='test', LastName='Washburn', Email='awashburn@ashley.com', Alias='awash', 
                          CommunityNickname='test2'+i,LanguageLocaleKey='en_US', LocaleSidKey='en_US', TimeZoneSidKey='America/Chicago',
                          EmailEncodingKey='UTF-8', ProfileId = pf.Id,ManagerId = userinfo.getUserId());
        
        myuserslist.add(u);
        }
        insert myuserslist;
        List<Case> mycaselist = new List<Case>();
        Case ca = new Case(Status = 'New');
        mycaselist.add(ca);
        Case caif = new Case(Status = 'New');
         mycaselist.add(caif);
         Case ca1 = new Case(Status = 'Working');
        mycaselist.add(ca1);
        //insert ca1;
        Case caif1 = new Case(Status = 'Working');
        mycaselist.add(caif1);
       // insert caif1;
         Case ca2 = new Case(Status = 'Awaiting Customer Reply');
        mycaselist.add(ca2);
      //  insert ca2;
        Case ca2if = new Case(Status = 'Awaiting Customer Reply');
        mycaselist.add(ca2if);
       // insert ca2if;
           Case ca3 = new Case(Status = 'New Customer Reply');
        mycaselist.add(ca3);
        //insert ca3;
        Case caif3 = new Case(Status = 'New Customer Reply');
        mycaselist.add(caif3);
        //insert caif3;
         Case ca4 = new Case(Status = 'Sent to Store');
        mycaselist.add(ca4);
        //insert ca4;
        Case caif4 = new Case(Status = 'Sent to Store');
        mycaselist.add(caif4);
       // insert caif4;
           Case ca5 = new Case(Status = 'Ready for Review');
        mycaselist.add(ca5);
       // insert ca5;
        Case caif5 = new Case(Status = 'Ready for Review');
        mycaselist.add(caif5);
      //  insert caif5;
         Case ca6 = new Case(Status = 'Closed', Type_of_Resolution__c = 'test', Resolution_Notes__c = 'test');
        mycaselist.add(ca6);
       // insert ca6;
        Case caif6 = new Case(Status = 'Closed', Type_of_Resolution__c = 'test', Resolution_Notes__c = 'test');
        mycaselist.add(caif6);
       // insert caif6;
           Case ca7 = new Case(Status = 'Closed in Salesforce', Type_of_Resolution__c = 'test', Resolution_Notes__c = 'test');
       mycaselist.add(ca7);
       // insert ca7;
        Case caif7 = new Case(Status = 'Closed in Salesforce', Type_of_Resolution__c = 'test', Resolution_Notes__c = 'test');
       mycaselist.add(caif7);
       // insert caif7;
              Case ca8 = new Case(Status = 'Open');
        mycaselist.add(ca8);
       // insert ca8;
        Case caif8 = new Case(Status = 'Open');
        mycaselist.add(caif8);
       
       insert mycaselist;
        
        List<Task> mytasklist = new List<Task>();
        
        Task tsk=new Task();
        tsk.Subject='call';
        tsk.calldurationInseconds=20;
        tsk.calltype='Inbound';
        tsk.OwnerId=myuserslist[0].Id;
        tsk.whatId = mycaselist[0].Id;
        
        Task tskif=new Task();
        tskif.Subject='call';
        tskif.calldurationInseconds=20;
        tskif.calltype='Inbound';
        tskif.OwnerId=myuserslist[0].Id;
        tskif.whatId = mycaselist[1].Id;
        
        Task tsktest=new Task();
        tsktest.Subject='call';
        tsktest.calldurationInseconds=20;
        tsktest.calltype='Outbound';
        tsktest.OwnerId=myuserslist[0].Id;
        tsktest.whatId = mycaselist[1].Id;
        
        Task tsktestif1=new Task();
        tsktestif1.Subject='call';
        tsktestif1.calldurationInseconds=20;
        tsktestif1.calltype='Outbound';
        tsktestif1.OwnerId=myuserslist[0].Id;
        tsktestif1.whatId = mycaselist[0].Id;
        
        
        
        mytasklist.add(tsktestif1);
        mytasklist.add(tsktest);
        mytasklist.add(tskif);
        mytasklist.add(tsk);
        
       
        
        Task tsk1=new Task();
        tsk1.Subject='call';
        tsk1.calldurationInseconds=20;
        tsk1.calltype='Inbound';
        tsk1.OwnerId=myuserslist[1].Id;
        tsk1.whatId = ca1.Id;
        
        mytasklist.add(tsk1);
        
        Task tsk1if=new Task();
        tsk1if.Subject='call';
        tsk1if.calldurationInseconds=20;
        tsk1if.calltype='Inbound';
        tsk1if.OwnerId=myuserslist[1].Id;
        tsk1if.whatId = caif1.Id;
        
        mytasklist.add(tsk1if);
        
        Task tsk10=new Task();
        tsk10.Subject='call';
        tsk10.calldurationInseconds=20;
        tsk10.calltype='Outbound';
        tsk10.OwnerId=myuserslist[1].Id;
        tsk10.whatId = ca1.Id;
        
        mytasklist.add(tsk10);
        
        Task tsk10if=new Task();
        tsk10if.Subject='call';
        tsk10if.calldurationInseconds=20;
        tsk10if.calltype='Outbound';
        tsk10if.OwnerId=myuserslist[1].Id;
        tsk10if.whatId = caif1.Id;
        
        mytasklist.add(tsk10if);
        
       
        Task tsk2=new Task();
        tsk2.Subject='call';
        tsk2.calldurationInseconds=20;
        tsk2.calltype='Inbound';
        tsk2.OwnerId=myuserslist[2].Id;
        tsk2.whatId = mycaselist[2].Id;
        
        mytasklist.add(tsk2);
        
        Task tsk2if=new Task();
        tsk2if.Subject='call';
        tsk2if.calldurationInseconds=20;
        tsk2if.calltype='Inbound';
        tsk2if.OwnerId=myuserslist[2].Id;
        tsk2if.whatId = mycaselist[3].Id;
        
        mytasklist.add(tsk2if);
                
        Task tsk20=new Task();
        tsk20.Subject='call';
        tsk20.calldurationInseconds=20;
        tsk20.calltype='Outbound';
        tsk20.OwnerId=myuserslist[2].Id;
        tsk20.whatId = mycaselist[2].Id;
        
        mytasklist.add(tsk20);
        
        Task tsk20if=new Task();
        tsk20if.Subject='call';
        tsk20if.calldurationInseconds=20;
        tsk20if.calltype='Outbound';
        tsk20if.OwnerId=myuserslist[2].Id;
        tsk20if.whatId = mycaselist[3].Id;
        
        mytasklist.add(tsk20if);
        
     
        
        Task tsk3=new Task();
        tsk3.Subject='call';
        tsk3.calldurationInseconds=20;
        tsk3.calltype='Inbound';
        tsk3.OwnerId=myuserslist[3].Id;
        tsk3.whatId = mycaselist[4].Id;
        
        mytasklist.add(tsk3);
        
        Task tsk3if=new Task();
        tsk3if.Subject='call';
        tsk3if.calldurationInseconds=20;
        tsk3if.calltype='Inbound';
        tsk3if.OwnerId=myuserslist[3].Id;
        tsk3if.whatId = mycaselist[5].Id;
        
        mytasklist.add(tsk3if);
        
        Task tsk30=new Task();
        tsk30.Subject='call';
        tsk30.calldurationInseconds=20;
        tsk30.calltype='Outbound';
        tsk30.OwnerId=myuserslist[3].Id;
        tsk30.whatId = mycaselist[4].Id;
        
        mytasklist.add(tsk30);
        
        Task tsk30if=new Task();
        tsk30if.Subject='call';
        tsk30if.calldurationInseconds=20;
        tsk30if.calltype='Outbound';
        tsk30if.OwnerId=myuserslist[3].Id;
        tsk30if.whatId = mycaselist[5].Id;
        
        mytasklist.add(tsk30if);
        
       
        
        Task tsk4=new Task();
        tsk4.Subject='call';
        tsk4.calldurationInseconds=20;
        tsk4.calltype='Inbound';
        tsk4.OwnerId=myuserslist[4].Id;
        tsk4.whatId = mycaselist[6].Id;
        
        mytasklist.add(tsk4);
        
        Task tsk4if=new Task();
        tsk4if.Subject='call';
        tsk4if.calldurationInseconds=20;
        tsk4if.calltype='Inbound';
        tsk4if.OwnerId=myuserslist[4].Id;
        tsk4if.whatId = mycaselist[7].Id;
        
        mytasklist.add(tsk4if);
        
        Task tsk40=new Task();
        tsk40.Subject='call';
        tsk40.calldurationInseconds=20;
        tsk40.calltype='Outbound';
        tsk40.OwnerId=myuserslist[4].Id;
        tsk40.whatId = mycaselist[6].Id;
        
        mytasklist.add(tsk40);
        
        Task tsk40if=new Task();
        tsk40if.Subject='call';
        tsk40if.calldurationInseconds=20;
        tsk40if.calltype='Outbound';
        tsk40if.OwnerId=myuserslist[4].Id;
        tsk40if.whatId = mycaselist[7].Id;
        
        mytasklist.add(tsk40if);
        
     
        
        Task tsk5=new Task();
        tsk5.Subject='call';
        tsk5.calldurationInseconds=20;
        tsk5.calltype='Inbound';
        tsk5.OwnerId=myuserslist[5].Id;
        tsk5.whatId = mycaselist[8].Id;
        
        mytasklist.add(tsk5);
        
        Task tsk5if=new Task();
        tsk5if.Subject='call';
        tsk5if.calldurationInseconds=20;
        tsk5if.calltype='Inbound';
        tsk5if.OwnerId=myuserslist[5].Id;
        tsk5if.whatId = mycaselist[9].Id;
        
        mytasklist.add(tsk5if);
        
        Task tsk50=new Task();
        tsk50.Subject='call';
        tsk50.calldurationInseconds=20;
        tsk50.calltype='Outbound';
        tsk50.OwnerId=myuserslist[5].Id;
        tsk50.whatId = mycaselist[8].Id;
        
        mytasklist.add(tsk50);
        
        Task tsk50if=new Task();
        tsk50if.Subject='call';
        tsk50if.calldurationInseconds=20;
        tsk50if.calltype='Outbound';
        tsk50if.OwnerId=myuserslist[5].Id;
        tsk50if.whatId = mycaselist[9].Id;
        
        mytasklist.add(tsk50if);
        
       
        
        Task tsk6=new Task();
        tsk6.Subject='call';
        tsk6.calldurationInseconds=20;
        tsk6.calltype='Inbound';
        tsk6.OwnerId=myuserslist[6].Id;
        tsk6.whatId = mycaselist[10].Id;
        
        mytasklist.add(tsk6);

        Task tsk6if=new Task();
        tsk6if.Subject='call';
        tsk6if.calldurationInseconds=20;
        tsk6if.calltype='Inbound';
        tsk6if.OwnerId=myuserslist[6].Id;
        tsk6if.whatId = mycaselist[11].Id;
        
        mytasklist.add(tsk6if);        
        
        Task tsk60=new Task();
        tsk60.Subject='call';
        tsk60.calldurationInseconds=20;
        tsk60.calltype='Outbound';
        tsk60.OwnerId=myuserslist[6].Id;
        tsk60.whatId = mycaselist[10].Id;
        
        mytasklist.add(tsk60);
        
        Task tsk60if=new Task();
        tsk60if.Subject='call';
        tsk60if.calldurationInseconds=20;
        tsk60if.calltype='Outbound';
        tsk60if.OwnerId=myuserslist[6].Id;
        tsk60if.whatId = mycaselist[11].Id;
        
        mytasklist.add(tsk60if);
        
     
        
        Task tsk7=new Task();
        tsk7.Subject='call';
        tsk7.calldurationInseconds=20;
        tsk7.calltype='Inbound';
        tsk7.OwnerId=myuserslist[7].Id;
        tsk7.whatId = mycaselist[12].Id;
        
        mytasklist.add(tsk7);
        
        Task tsk7if=new Task();
        tsk7if.Subject='call';
        tsk7if.calldurationInseconds=20;
        tsk7if.calltype='Inbound';
        tsk7if.OwnerId=myuserslist[7].Id;
        tsk7if.whatId = mycaselist[13].Id;
        
        mytasklist.add(tsk7if);
        
        Task tsk70=new Task();
        tsk70.Subject='call';
        tsk70.calldurationInseconds=20;
        tsk70.calltype='Outbound';
        tsk70.OwnerId=myuserslist[7].Id;
        tsk70.whatId = mycaselist[12].Id;
        
        mytasklist.add(tsk70);
        
        Task tsk70if=new Task();
        tsk70if.Subject='call';
        tsk70if.calldurationInseconds=20;
        tsk70if.calltype='Outbound';
        tsk70if.OwnerId=myuserslist[7].Id;
        tsk70if.whatId = mycaselist[13].Id;
        
        mytasklist.add(tsk70if);
        
  
        
        Task tsk8=new Task();
        tsk8.Subject='call';
        tsk8.calldurationInseconds=20;
        tsk8.calltype='Inbound';
        tsk8.OwnerId=myuserslist[8].Id;
        tsk8.whatId = mycaselist[14].Id;
        
        mytasklist.add(tsk8);
        
        Task tsk8if=new Task();
        tsk8if.Subject='call';
        tsk8if.calldurationInseconds=20;
        tsk8if.calltype='Inbound';
        tsk8if.OwnerId=myuserslist[8].Id;
        tsk8if.whatId = mycaselist[15].Id;
        
        mytasklist.add(tsk8if);
        
        Task tsk80=new Task();
        tsk80.Subject='call';
        tsk80.calldurationInseconds=20;
        tsk80.calltype='Outbound';
        tsk80.OwnerId=myuserslist[8].Id;
        tsk80.whatId = mycaselist[14].Id;
        
        mytasklist.add(tsk80);
        
        Task tsk80if=new Task();
        tsk80if.Subject='call';
        tsk80if.calldurationInseconds=20;
        tsk80if.calltype='Outbound';
        tsk80if.OwnerId=myuserslist[8].Id;
        tsk80if.whatId = mycaselist[15].Id;
        
        mytasklist.add(tsk80if);
        
        Task tsk81=new Task();
        tsk81.Subject='call';
        tsk81.calldurationInseconds=0;
        tsk81.calltype='Outbound';
        tsk81.OwnerId=myuserslist[9].Id;
        tsk81.whatId = mycaselist[14].Id;
        mytasklist.add(tsk81);
        
        Task tsk81if=new Task();
        tsk81if.Subject='call';
        tsk81if.calldurationInseconds=0;
        tsk81if.calltype='Outbound';
        tsk81if.OwnerId=myuserslist[9].Id;
        tsk81if.whatId = mycaselist[14].Id;
        mytasklist.add(tsk81if);
        
        insert mytasklist;
        
        Test.StartTest();
        SupervisorReport.getData();
        SupervisorReport.getspecificUserData('test');
        SupervisorReport.getManager();
        Test.StopTest();
        
    }    
   
}