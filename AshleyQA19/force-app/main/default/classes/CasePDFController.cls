public class CasePDFController{
    
    public case caseRecord{get; set;}
    public List<ProductLineItem__c> pli{get; set;}
    public List<Case_Line_Item__c> cli{get; set;}
    public List<Legacy_Comment__c> Legacycomments{get; set;}
    public string opendate{get; set;}
    
    public string TechSchDate{get; set;}
    public string FolupDate{get; set;}
    
    
    
    @AuraEnabled
    public static case getcaserecords(String RecId){
        system.debug('recordId----'+RecId);
        
        case lstOfRec = [select id from case where id =: RecId];
        //Recidval = lstOfRec.id;
        
        return lstOfRec;
    }
    
    
    
    //ApexPages.StandardController controller
    
    public CasePDFController(){
        //caseRecord = [SELECT Id,Status FROM case WHERE Id = :ApexPages.currentPage().getParameters().get('id')];       '5000n000005PH1CAAW'
        
        system.debug('recordId1----'+ApexPages.currentPage().getParameters().get('id'));
        
        caseRecord = [SELECT Id,Status,Address__c,Address_Line_1__c,Address_Line_2__c,City__c,State__c,ZIP__c,Technician_ServiceReqId__c,Technician_Company__c,
                                Technician_Name__c,TechnicianNameScheduled__c,Legacy_Service_Request_ID__c,Category_Reason_Codes__c,
                                Estimated_time_for_stop__c,Technician_Schedule_Date__c,CreatedDate,Follow_up_Date__c,followup_Priority_EstimatedTime__c,
                                CreatedById,Owner.Name,Type_of_Resolution__c,CreatedBy.Name,
                                Company__c,Legacy_Technician__c,Tech_Scheduled_Date__c 
                                FROM case WHERE Id =: ApexPages.currentPage().getParameters().get('id')];
         DateTime d = caseRecord.CreatedDate;                       
         opendate = d.format('MM/dd/yyyy'); 
         
         if(caseRecord.Technician_Schedule_Date__c != null){
             Datetime dt = caseRecord.Technician_Schedule_Date__c + 1; 
             If(dt != null){ TechSchDate = dt.format('MM/dd/yyyy'); }
         }else if(caseRecord.Tech_Scheduled_Date__c != null){
             Datetime dt = caseRecord.Tech_Scheduled_Date__c + 1; 
             If(dt != null){ TechSchDate = dt.format('MM/dd/yyyy'); }
         }
         
         
         If(caseRecord.Follow_up_Date__c == null){
             DateTime dat;
             if(caseRecord.Technician_Schedule_Date__c != null){ dat = caseRecord.Technician_Schedule_Date__c + 2; }
             else if(caseRecord.Tech_Scheduled_Date__c != Null){ dat = caseRecord.Tech_Scheduled_Date__c + 2; }
             
             If(dat != null){ FolupDate = dat.format('MM/dd/yyyy');  } 
         }   
         else {
             Datetime dt = caseRecord.Follow_up_Date__c + 1; 
             If(dt != null){ FolupDate = dt.format('MM/dd/yyyy'); }
         }
                          
         pli = [ select id,Item_SKU__c,Part_Order_Tracking_Number__c,Delivery_Date__c,Item_Serial_Number__c,
                                        Legacy_Service_Request_Item_ID__c from ProductLineItem__c where Case__c =:caseRecord.Id];
                                        
          cli = [ select id,Item_SKU__c,Part_Order_Tracking_Number__c,Delivery_Date__c,Item_Serial_Number__c, 
                                          Legacy_Service_Request_Item_ID__c from Case_Line_Item__c where Case__c =:caseRecord.Id];
                                         
         Legacycomments = [ select id,Comment__c,LastModifiedDate from Legacy_Comment__c where Case__c =: caseRecord.Id]; 
         system.debug(Legacycomments.size());              
         
    }
    
}