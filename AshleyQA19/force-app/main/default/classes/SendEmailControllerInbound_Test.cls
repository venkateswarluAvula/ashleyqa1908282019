@isTest
public class SendEmailControllerInbound_Test {
    @isTest static void testCases1() {
        
        Account newAcc = new Account();
        newAcc.name='test';
        insert newAcc;
        
        Contact con=new Contact();
        con.lastname='Testing';
        con.email='test@test.com';
        insert con;
        
        Case c = new Case();
        c.Subject='Test record';
        c.Type = 'General Inquiry';
        c.Description = 'test record';
        c.Origin = 'Phone';
        c.Status = 'New';
        c.Priority='Medium';
        c.Request_Status__c = 'New';
        c.AccountId = newAcc.Id;
        c.ContactId = con.Id;
        insert c;
        case cs = [select id, casenumber From case where id = : c.id];
        System.debug('case number' + cs.casenumber);
        System.debug('cases'+c);
        // Create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        email.plainTextBody = 'Here is my plainText body of the email';
        email.fromAddress = 'noreply@gmail.com';
        List<String> tA = new List<String>();
        tA.add('qw@gm.com');
        email.toAddresses = tA;
        email.subject = 'My test subject('+cs.CaseNumber+')';
        System.debug('emails-->'+email.subject);
        
        SendEmailControllerInbound caseObj = new SendEmailControllerInbound();
        caseObj.handleInboundEmail(email, env);
    }
    @isTest static void testCases2() {
        QueueSobject Qso = new QueueSobject();
        
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        email.plainTextBody = 'Here is my plainText body of the email';
        email.fromAddress = 'noreply@gmail.com';
        List<String> tA = new List<String>();
        tA.add('qw@gm.com');
        email.toAddresses = tA;
        
        SendEmailControllerInbound caseObj = new SendEmailControllerInbound();
        caseObj.handleInboundEmail(email, env);
        
    }
    
}