@isTest
public class SendEmailController_Test {
    public static testmethod void testvalidate(){
        
        Account newAcc = new Account();
        newAcc.name='test';
        insert newAcc;
        
        Contact con=new Contact();
        con.lastname='Testing';
        con.email='test@test.com';
        insert con;
        
        Case c = new Case();
        c.AccountId = newAcc.Id;
        c.ContactId = con.Id;
        insert c;
        
        EmailMessage em = new EmailMessage();
        em.Subject = 'Test';
        em.HtmlBody = 'plaintext';
        em.ValidatedFromAddress= 'ashleycustomercare@ashleyfurniture.com';
        em.ToAddress = 'example@email.com';
        em.FromName = 'DisplayName';
        em.Subject = 'Test';
        em.HtmlBody ='Html body';   
        em.Incoming= True;
        em.TextBody = 'Plaintext';  
        em.status = '3';
        em.ParentId = c.Id;
        
        insert em;
        SendEmailController.getTemplateDetails('00X6A000000UYz5UAG');
        SendEmailController.getTemplates();
        SendEmailController.sendMailMethod('example@email.com', 'Test', 'Html body', c.Id, '00X6A000000UYz5UAG', 'ashleycustomercare@ashleyfurniture.com');
    }
    
}