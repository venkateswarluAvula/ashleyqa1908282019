@isTest
private class SalesOrderLineItemRelatedLstContrl_Test {
	
	@isTest static void testgetLineItems() {

		//get id prefix for sales orders
		Schema.DescribeSObjectResult describeResult = SalesOrder__x.SobjectType.getDescribe();
		string salesOrderIdPrefix = describeResult.getKeyPrefix();
		string smapleSalesOrderId = salesOrderIdPrefix + '2F0000004eetAAA';

		//mock line item data
		SalesOrderDAO.mockedSalesOrders.add(new SalesOrder__x(ExternalId = 'testlieitemid', Id = smapleSalesOrderId));
		SalesOrderDAO.mockedSalesOrderLineItems.add(new SalesOrderItem__x());
		//system.debug('test ' + SalesOrderLineItemRelatedListController.getLineItems(smapleSalesOrderId).size());
		System.assert(SalesOrderLineItemRelatedListController.getLineItems(smapleSalesOrderId).size() != 0);		
	}
    
    @isTest static void testgetLineItemswithImage() {
		//get id prefix for sales orders
		Schema.DescribeSObjectResult describeResult = SalesOrder__x.SobjectType.getDescribe();
		string salesOrderIdPrefix = describeResult.getKeyPrefix();
		string smapleSalesOrderId = salesOrderIdPrefix + '2F0000004eetAAA';

		//mock line item data
		SalesOrderDAO.mockedSalesOrders.add(new SalesOrder__x(ExternalId = 'testlieitemid', Id = smapleSalesOrderId));
		SalesOrderDAO.mockedSalesOrderLineItems.add(new SalesOrderItem__x());		
		System.assert(SalesOrderLineItemRelatedListController.getLineItemswithImage(smapleSalesOrderId).size() != 0);
	}
    
    @isTest static void testgetval(){        
		Schema.DescribeSObjectResult describeResult = SalesOrder__x.SobjectType.getDescribe();
		string salesOrderIdPrefix = describeResult.getKeyPrefix();
		string smapleSalesOrderId = salesOrderIdPrefix + '2F0000004eetAAA';	
		System.assert(SalesOrderLineItemRelatedListController.getval(smapleSalesOrderId).size() != 0);
    }
	@isTest static void testgetSalesOrderLineItem(){        
		Schema.DescribeSObjectResult describeResult = SalesOrder__x.SobjectType.getDescribe();
		string salesOrderIdPrefix = describeResult.getKeyPrefix();
		string smapleSalesOrderId = salesOrderIdPrefix + '2F0000004eetAAA';	
        ID lineitem = 'x00q00000000DMoAAM';
		System.assert(SalesOrderLineItemRelatedListController.getSalesOrderLineItem(lineitem, smapleSalesOrderId).size() != 0);
    }
}