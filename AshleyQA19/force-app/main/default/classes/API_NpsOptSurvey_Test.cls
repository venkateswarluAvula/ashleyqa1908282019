/****** v1 | Description: Test coverage to update customer endpoints methods in NPS Controller | 06/10/2017 | */
@isTest
public class API_NpsOptSurvey_Test {
    /* test valid rest input */
	@isTest
    static void testCustomerEndpoint() {
        String first = 'TestCoverage';
        String last = 'CoveragePerson';
        Account account = new Account(
            name = first + ' ' + last + ' Account'
        );
        insert account;
                
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse(); 
        
        req.requestURI = '/services/apexrest/NpsOptSurvey';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(
           '{' +  
            			'"HomesCustID" : "testAPIId1",' +
            			'"SFPersonAccountID" : "'+account.id+'",' + 
                        '"OptIn" : "START",' +
                        '"SalesOrderNmer" : "200460320",' +
                        '"StoreId" : 12345,' +
                    '}' 
        ); 
        RestContext.request = req;
        RestContext.response = res; 
        
        String sampleResponse = '[{"HomesCustID":"006787240707",	"SFPersonAccountID":"0016A00000rcapFQAQ","OptIn":"STOP","SalesOrderNumber":"700696140","Storeid":"133"}]'; 
        
        String sampleResponse1 = ''; 
        
        Test.startTest();
        NpsResponseWrapper retVal = API_NpsOptSurvey.updateCustomerInfo();
        APINpsHelper.parseNpsRequest(sampleResponse);
        APINpsHelper.parseNpsRequest(sampleResponse1);
        Test.stopTest();
        
        system.assertEquals(retVal.isSuccess, false);

    }
    
    @isTest
    static void testCreateAllCustomers() {
        List<NpsWrapper> customers = new List<NpsWrapper>();
        String first = 'TestCoverage';
        String last = 'CoveragePerson';
        Account acc = new Account(
            name = first + ' ' + last + ' Account'
        );
        insert acc;
        for (Integer i = 0; i < 10; i++) {
            NpsWrapper test = new NpsWrapper();
            test.HomesCustID = 'gid';
            test.SFPersonAccountID = acc.Id;
            test.OptIn = 'START'+i; 
            test.SalesOrderNmer = '200460320'+i;
            test.StoreId = 37067;

        	customers.add(test);
        }
        NpsOptController.updateAllNpsRecords(customers);
       
    }  
}