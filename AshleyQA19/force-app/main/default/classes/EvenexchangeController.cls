public class EvenexchangeController {
    
	@AuraEnabled    
    public static Caseandordervalues getProductLineItem(Id caseId){
        List<ProductLineItem__c> productlineItems = new List<ProductLineItem__c>();        
        productlineItems = [Select Id,Item_Description__c, Invoice_Number__c, Item_Seq_Number__c, Item_SKU__c,Part_Order_Status__c, Sales_Order_Number__c, Item_Serial_Number__c,Serial_Number__c from ProductLineItem__c where Case__c =: caseId];                
        List<Case> caselists = new List<Case>();        
        caselists = [Select Id,Legacy_Service_Request_ID__c,Account.Phone, CaseNumber, Contact.Phone_2__c ,Contact.Phone_3__c,ContactEmail, Address_Line_1__c,Address_Line_2__c,City__c,State__c,Sales_Order__c,ZIP__c from Case where Id =: caseId];
        String externalId = caselists[0].Sales_Order__c;
        if(caselists[0].Legacy_Service_Request_ID__c == null || caselists[0].Legacy_Service_Request_ID__c == ''){
            caselists[0].Legacy_Service_Request_ID__c = '';
        }
        List<SalesOrder__x> orders = [Select Id, ExternalId, phhHot__c,phhOrder_Notes__c, phhDesiredDate__c, fulfillerID__c, 
                                      phhStoreID__c, phhDeliveryType__c , phhProfitcenter__c, phhSaleType__c, phhSalesOrder__c,
                                      phhCustomerName__c, phhStoreLocation__c, phhReasonCode__c, phhGuestID__c, phhDatePromised__c,
                                      phhWindowBegin__c ,phhCustomerID__c,phhWindowEnd__c,phhServiceLevel__c, phhASAP__c,phhSOSource__c,phhERPAccounShipTo__c                   
                                      from SalesOrder__x  
                                      where ExternalId=: externalId];
        
        List<SalesOrderItem__x> lineItems = [Select Id, phdItemSKU__c, phdItemDesc__c,phdShipAddress1__c, phdShipAddress2__c, phdShipCity__c, 
                                             phdShipState__c, phdInvoiceNo__c, phdItemSeq__c, phdSaleType__c, phdItemStatus__c, phdSalesOrder__c,phdShipZip__c 
                                             from SalesOrderItem__x 
                                             where phdSalesOrder__r.ExternalId =: externalId];
        
        for(ProductLineItem__c plitem : productlineItems){
            for(SalesOrderItem__x solitem: lineItems){
                system.debug('Salesorder Item ' + solitem.phdItemSKU__c);
                system.debug('Product Item ' + plitem.Item_SKU__c);
                if(solitem.phdItemSKU__c == plitem.Item_SKU__c){
                    plitem.Part_Order_Status__c = solitem.phdSaleType__c;   
                  //  productlineItems.add(plitem);
                  //   system.debug('productlineItems1' + productlineItems);
                }
            }        
        }
      //  Update productlineItems;
      //  system.debug('productlineItems' + productlineItems);
        if(Test.isRunningTest()){
            SalesOrder__x salesOrder = new SalesOrder__x(fulfillerID__c = '8888300-164',
                                                         ExternalId = '17331400:001q000000raDkvAAE',  
                                                         phhProfitcenter__c = 1234567,
                                                         Phhcustomerid__c = '784584585',
                                                         phhSalesOrder__c = '88845758',
                                                         phhStoreID__c = '133'
                                                         );            
            Caseandordervalues cav = new Caseandordervalues(caselists, productlineItems, salesOrder, lineItems);
            return cav;
        }else{
            Caseandordervalues cav = new Caseandordervalues(caselists, productlineItems, orders[0], lineItems); 
            return cav;
        }       
    }
    
    public class Caseandordervalues {
        @AuraEnabled
        public List<Case> caseAddress {get;set;}
        
        @AuraEnabled
        public List<ProductLineItem__c> productlineItems {get;set;}
        
        @AuraEnabled
        Public SalesOrder__x salesvale {get;set;}
        
        @AuraEnabled
        Public List<SalesOrderItem__x> salesvaleItem {get;set;}
        
        public caseandordervalues(List<case> caseLists, List<ProductLineItem__c> prouctLineitems, SalesOrder__x salesvalue, List<SalesOrderItem__x> salesvaleLineItem){            
            caseAddress = caseLists;
            productlineItems = prouctLineitems;
            salesvale = salesvalue;
            salesvaleItem = salesvaleLineItem;
        }
    }
    
    @AuraEnabled
    public static String getDiscontinuedItem(String Items, String externalId){
        List<String> starFlags = new List<String>();      
        system.debug('Item values ' + Items);
        List<skuWrapper> skws = new List<skuWrapper>();       
        String[] Itemskus = Items.split(',');
        
        for(String st : Itemskus){
            skuWrapper sk = new skuWrapper();
            sk.ItemId = st;            
            skws.add(sk);
        }
        system.debug('Item values Wrapper ' + skws);
        SalesOrder__x order = SalesOrderDAO.getOrderByExternalId(externalId);
        String fulfiller = order.fulfillerID__c;
        //String endpoint = system.label.AshleyApigeeEndpoint + fulfiller + '/salesorders/EESKUItems?';
        String endpoint = system.label.AshleyApigeeEndpoint + fulfiller + '/salesorders/DiscontinuedItems?profitcenter=23&apikey=bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713';
        
        Http http = new http();
        Httprequest req = new HttpRequest();
        system.debug('JSON Body For Dis ' + JSON.serialize(skws));
        
        //req.setHeader('apikey', system.label.AshleyApigeeApiKey);
        req.setBody(JSON.serialize(skws));
        req.setHeader('Content-Type' ,'application/json');
        req.setEndpoint(endpoint);        
        system.debug('enpoint' + endpoint);
        req.setTimeOut(120000);
        req.setMethod('POST');
        if(Test.isRunningTest()){
            String errMsg='No records founds--'; 
            return errMsg;
        }else{
            HttpResponse res = http.send(req);
            System.debug('apiresponse...' + res.getBody());
            system.debug('apiresponse...' + res.getStatusCode());
            if(res.getStatusCode()==200) {
                return res.getBody();
                //starFlags = res.getBody();
            } else {
                String errMsg='No records founds--';  
                AuraHandledException ex = new AuraHandledException(errMsg);
                return errMsg;
            }
        }
    }
    
    @AuraEnabled
    public static String getStarFlags(String externalId){
        List<String> starFlags = new List<String>();
        SalesOrder__x order = SalesOrderDAO.getOrderByExternalId(externalId);
        String fulfiller = order.fulfillerID__c;
        String endpoint = system.label.AshleyApigeeEndpoint + fulfiller + '/salesorders/EESKUItems?';
        Http http = new http();
        Httprequest req = new HttpRequest();
        req.setHeader('apikey', system.label.AshleyApigeeApiKey);
        req.setEndpoint(endpoint);
        system.debug('enpoint' + endpoint);
        req.setTimeOut(120000);
        req.setMethod('GET');
        if(Test.isRunningTest()){
            String errMsg='No records founds--'; 
            return errMsg;
        }else{
            HttpResponse res = http.send(req);
            System.debug('apiresponse...' + res.getBody());
            system.debug('apiresponse...' + res.getStatusCode());
            if(res.getStatusCode()==200) {
                return res.getBody();
                //starFlags = res.getBody();
            } else {
                String errMsg='No records founds--';  
                AuraHandledException ex = new AuraHandledException(errMsg);
                return errMsg;
            }
        }        
    }
    
    @AuraEnabled
    public static SalesOrder__x getOrderDetails(String externalId){
        List<SalesOrder__x> orders = [Select Id, ExternalId, phhHot__c,phhOrder_Notes__c, phhDesiredDate__c, fulfillerID__c, 
                                      phhStoreID__c, phhDeliveryType__c , phhProfitcenter__c, phhSaleType__c, phhSalesOrder__c,
                                      phhCustomerName__c, phhStoreLocation__c, phhReasonCode__c, phhGuestID__c, phhDatePromised__c,
                                      phhWindowBegin__c ,phhWindowEnd__c,phhASAP__c,phhSOSource__c,phhERPAccounShipTo__c                   
                                      from SalesOrder__x  
                                      where ExternalId=: externalId];
        
        return (orders.size() > 0) ? orders[0] : null;
    }    
    
    
    
    @AuraEnabled
    public static ApiResponseWrapper sendEvenexchangeValue(String deliveryDate, List<Case> caselists, List<SalesOrderItem__x> lineItems, List<ProductLineItem__c> selproductlineitems, SalesOrder__x salesorder, String salesordercomment, String AddressType,String Phone1, String Phone2, String Phone3, String emailinfo, Id caseId, String vipdes, String hccdes,List<String> addStars){
        contactWrapper cntinfo = new contactWrapper();        
        addressWrapper SAddress = new addressWrapper();
        List<lineitemWrapper> litemvalues = new List<lineitemWrapper>();     
        cntinfo.HomePhone = phone1;
        cntinfo.WorkPhone = phone2;
        cntinfo.CellPhone = phone3;
        cntinfo.Email = emailinfo;
        system.debug('Test ' + addStars); 
        system.debug('Test ' + lineItems); 
        system.debug('Test2 ' + selproductlineitems);
        if(AddressType == 'Case Address'){
            SAddress.Address1 = caselists[0].Address_Line_1__c;
            SAddress.Address2 = caselists[0].Address_Line_2__c;
            SAddress.City = caselists[0].City__c;
            SAddress.State = caselists[0].State__c;
            SAddress.Zip = caselists[0].ZIP__c;
        }else{            
            SAddress.Address1 = lineItems[0].phdShipAddress1__c;
            SAddress.Address2 = lineItems[0].phdShipAddress2__c;
            SAddress.City = lineItems[0].phdShipCity__c;
            SAddress.State = lineItems[0].phdShipState__c;
            SAddress.Zip = lineItems[0].phdShipZip__c;
        }
        Integer j = 0;
        String skuv = '';
        for (ProductLineItem__c prodlineitem : selproductlineitems){
            lineitemWrapper liw = new lineitemWrapper();
            liw.ItemID = prodlineitem.Item_SKU__c;
            liw.ItemSeq = Integer.valueOf(prodlineitem.Item_Seq_Number__c); 
            liw.Description = prodlineitem.Item_Description__c;
            litemvalues.add(liw);            
        }
        
        lineitemWrapper liwvip = new lineitemWrapper();
        liwvip.ItemID = '*VIP';
        liwvip.ItemSeq = 0; 
        liwvip.Description = vipdes;
        litemvalues.add(liwvip);
        
        lineitemWrapper liwhcc = new lineitemWrapper();
        liwhcc.ItemID = '*HCC';
        liwhcc.ItemSeq = 0; 
        liwhcc.Description = hccdes;
        litemvalues.add(liwhcc);        
        for(String sav: addStars){
            lineitemWrapper liwhcadd = new lineitemWrapper();
            String[] str2 = sav.split(':');            
            liwhcadd.ItemID = str2[0];
            liwhcadd.ItemSeq = 0; 
            liwhcadd.Description = str2[1];
            litemvalues.add(liwhcadd);
        }
        String custId = salesorder.phhCustomerID__c;
        system.debug('Customer Id Values ' + custId);
        evenexchangeWrapper exwrapper = new evenexchangeWrapper(deliveryDate, salesordercomment, custId, litemvalues, cntinfo, SAddress);        
        system.debug('Date' + deliveryDate);
        system.debug('Sales Comments' + salesordercomment);        
        system.debug('Sales Comments' + AddressType);
        String JSONString = JSON.serialize(exwrapper);
        system.debug('Post Json Values ' + JSONString);    
        User u = [select Id, username from User where Id = :UserInfo.getUserId()];
        String UserName = u.username; 
        //String endpoint = system.label.AshleyApigeeEndpoint + deliveryDate + '/salesorders/EESKUItems?';
        String endpoint = system.label.AshleyApigeeEndpoint + salesorder.fulfillerID__c + '/salesorders/' + selproductlineitems[0].Invoice_Number__c + '/EE?apikey='+system.label.AshleyApigeeApiKey +'&username=' + UserName;
        String endpointput = system.label.AshleyApigeeEndpoint + salesorder.fulfillerID__c + '/customer-service/service-requests/' + caselists[0].Legacy_Service_Request_ID__c + '/close?apikey='+system.label.AshleyApigeeApiKey;
        system.debug('Apikey ' + system.label.AshleyApigeeApiKey);
        system.debug('Endpoint ' + endpoint);
        ApiResponseWrapper response = new ApiResponseWrapper(); 
        Http http = new http();
        Http http1 = new http();
        Httprequest req = new HttpRequest();
        Httprequest reqput = new HttpRequest();
        HttpResponse resput = new HttpResponse();
        //req.setHeader('apikey', system.label.AshleyApigeeApiKey);
        req.setEndpoint(endpoint);
        system.debug('enpoint' + endpoint);        
        req.setMethod('POST');
        req.setBody(JSON.serialize(exwrapper));
        req.setHeader('Content-Type' ,'application/json');        
        system.debug('Final value ' + litemvalues.size());
        if(Test.isRunningTest()){
            String errMsg='No records founds--';
            response.isSuccess = false;            
            response.message = errMsg;             
        }else{
            HttpResponse res = http.send(req);
            System.debug('apiresponse...' + res.getBody());
            system.debug('apiresponse...' + res.getStatusCode());
            if(res.getStatusCode()==200) {
                response.isSuccess = true;                
                response.message = res.getBody();
                if(caselists[0].Legacy_Service_Request_ID__c != ''){
                    reqput.setEndpoint(endpointput);
                    reqput.setMethod('PUT');
                    reqput.setBody('{}');
                    reqput.setHeader('Content-Type' ,'application/json');
                    resput = http1.send(reqput);
                    if(resput.getStatusCode()==200){
                       system.debug('Service Request Closed');
                    }else{
                       system.debug('Service Request Not Closed');
                    }
                }
                Case ca = new case();
                ca.Id = caseId;
                ca.Status = 'Closed';
                ca.Resolution_Notes__c = salesordercomment;
                ca.Type_of_Resolution__c = 'Exchange';
                //ca.Delivery_Date__c = deliveryDate;
               // ca.Sales_Order__c = salesorder.phhSalesOrder__c;
                
                update ca;
                //starFlags = res.getBody();
            } else {
                response.isSuccess = false;
                response.message = res.getStatusCode() + ' - ' + res.getBody();
                String errMsg= res.getBody();  
                AuraHandledException ex = new AuraHandledException(errMsg);                
            }
        }
        
        return response;
    }
    public class ApiResponseWrapper implements Attributable{
        @AuraEnabled
        public boolean isSuccess {get;set;}
        @AuraEnabled
        public string message {get;set;}
    }
    
    public class evenexchangeWrapper {
        @AuraEnabled
        public String DeliveryDate {get;set;}        
        @AuraEnabled
        public String SaleComments {get;set;}   
        @AuraEnabled
        public String CustomerID {get;set;}
        @AuraEnabled
        public List<lineitemWrapper> LineItems {get;set;}
        @AuraEnabled
        public contactWrapper ContactInfo {get;set;}
        @AuraEnabled
        public addressWrapper ShipAddress {get;set;}   
        
        public evenexchangeWrapper(String DDate, String SaleComment, String CustomId, List<lineitemWrapper> LineItem, contactWrapper ContInfo, addressWrapper ShipAdd){
            DeliveryDate = DDate;
            SaleComments = SaleComment;
            CustomerID = CustomId;
            LineItems = LineItem;
            ContactInfo = ContInfo;
            ShipAddress = ShipAdd;
        }
        
    }
    public class lineitemWrapper {        
        @AuraEnabled
        public String ItemID {get;set;}
        @AuraEnabled
        public Integer ItemSeq {get;set;}
        @AuraEnabled
        public String Description {get;set;}        
    }
    
    Public class skuWrapper{
        @AuraEnabled
        public String ItemId {get;set;}
    }
    
    public class contactWrapper {
        @AuraEnabled
        public String HomePhone {get;set;}
        @AuraEnabled
        public String WorkPhone {get;set;}
        @AuraEnabled
        public String Email {get;set;}
        @AuraEnabled
        public String CellPhone {get;set;} 
    }
    
    public class addressWrapper {
        @AuraEnabled
        public String Address1 {get;set;}
        @AuraEnabled
        public String Address2 {get;set;}
        @AuraEnabled
        public String City {get;set;}
        @AuraEnabled
        public String State {get;set;} 
        @AuraEnabled
        public String Zip {get;set;}
    }
    
}