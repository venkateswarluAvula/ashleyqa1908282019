public class ShoppingCartDiscountWrapper {
  @AuraEnabled
    public List<SalesOrderLineWrapper> SalesOrderLineList{get;set;}
    
    @AuraEnabled
    public ShoppingCartDeliveryWrapper Delivery{get;set;}
    
    @AuraEnabled
    public String IsDeliveryFeeDiscountApprovedByManager{get;set;}
}