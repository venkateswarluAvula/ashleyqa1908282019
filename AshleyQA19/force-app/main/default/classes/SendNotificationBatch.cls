global class SendNotificationBatch implements Schedulable{
    
    //Variable Section
    global String SFDCAccountId = Label.Ashley_Enterprise_Employee_List;
    
    //Method which schedules the Everyday
    global void execute(SchedulableContext sc) {
        system.debug('sc--->'+sc);
        Notification();
        ScheduleDelete(SFDCAccountId);
        
    }
    public void ScheduleDelete(string SFDCAccountId){
        System.debug('accountid-->'+SFDCAccountId);
        List<Contact> conList = new List<Contact>();
        if(!Test.isRunningTest()){
            conList = [SELECT Id, AccountId,Name,CreatedDate ,LastModifiedDate ,CreatedById,LastModifiedById,LastViewedDate 
                       FROM Contact WHERE AccountId =:SFDCAccountId 
                       AND(LastModifiedDate < LAST_N_DAYS:30 OR LastViewedDate < LAST_N_DAYS:30)];
        }else{
            conList = [SELECT Id, AccountId,Name,CreatedDate ,LastModifiedDate ,CreatedById,LastModifiedById,LastViewedDate 
                       FROM Contact WHERE AccountId =:SFDCAccountId ];
        }
        system.debug('conList--->'+conList);
        if(!conList.isEmpty()){
            System.debug('Cons-->'+conList);
            List<Contact> maillist = new List<Contact>();
            List<Contact> consList = new List<Contact>();
            maillist = conList;
            System.debug('listmail-->'+maillist);
            Delete conList;
            if(!Test.isRunningTest()){
                consList = [SELECT Id, AccountId,Name,CreatedDate ,LastModifiedDate ,CreatedById,LastModifiedById,LastViewedDate 
                            FROM Contact WHERE AccountId =:SFDCAccountId 
                            AND(LastModifiedDate < LAST_N_DAYS:30 OR LastViewedDate < LAST_N_DAYS:30)];
            }else{
                consList = [SELECT Id, AccountId,Name,CreatedDate ,LastModifiedDate ,CreatedById,LastModifiedById,LastViewedDate 
                            FROM Contact WHERE AccountId =:SFDCAccountId ];
            }
            System.debug('conList.size()--->'+conList.size());
            // when conlist is empty means all records have been deleted successfully.
            if(consList.size()==0){
                System.debug('maillist before sending email-->'+maillist);
                string header = 'Record Id, Name ,AccountId, Created Date, Modified Date,Viewed Date \n';
                string finalstr = header ;
                for(Contact a: maillist)
                {
                    string recordString = a.id+','+a.Name+','+a.AccountId+',' +a.CreatedDate+','+a.LastModifiedDate +','+a.LastViewedDate+'\n';
                    finalstr = finalstr +recordString;
                }
                Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
                blob csvBlob = Blob.valueOf(finalstr);
                string csvname= 'Deleted ContactsList.csv';
                csvAttc.setFileName(csvname);
                csvAttc.setBody(csvBlob);
                Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
                String[] toAddresses = new list<string>{};
                    toAddresses.addAll((Label.DevOpsTeamMails).split(';'));
                    String subject ='Deleted ContactsList CSV';
                email.setSubject(subject);
                email.setToAddresses( toAddresses );
                email.setPlainTextBody('From last 30 days '+maillist.size()+' records are not updated. ');
                email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
                Messaging.SendEmailResult [] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            }
        }
        
    }
    public void Notification(){
        System.debug('accountid here-->'+SFDCAccountId);
        List<Contact> conList = new List<Contact>();
        conList = [SELECT Id, AccountId,Name,CreatedDate ,LastModifiedDate ,CreatedById,LastModifiedById,LastViewedDate 
                   FROM Contact WHERE AccountId =:SFDCAccountId 
                   AND (LastModifiedDate = LAST_N_DAYS:3 OR LastViewedDate = LAST_N_DAYS:3)];
        if(conList.isEmpty()){
            System.debug('Cons-->'+conList);
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage(); 
            String[] toAddresses = new String[]{};
                toAddresses.addAll((Label.DevOpsTeamMails).split(';'));//{con.Owner.Email};
                System.debug('to-->'+toAddresses);
            Message.setToAddresses(toAddresses);
            String messageBody = '<html><body>Hi Team,<br>From last 3 days contacts are not inserted/updated .<br>Kindly take appropriate action.<br></body></html>';
            Message.SaveAsActivity = false;
            Message.setHtmlBody(messageBody);
            mailList.add(Message);  
            
            if(!mailList.isEmpty()) {
                try{
                    Messaging.sendEmail(mailList);
                }
                catch (Exception ex) {
                    System.debug('Mail List is Empty.'+ex);
                }
            }
        }
    }
}