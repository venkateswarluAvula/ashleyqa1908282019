@RestResource(urlMapping = '/AccountExportCSV/*')
global with sharing class AccountExportCSV {
	@HttpGet
    global static List<String> getAccountDetails(){
        List<String> accCSVs = new List<String>();
        List<Address__c> Addressv = New List<Address__c>();
        String aDays = System.Label.AccountCSVDays;
        Date endDate = Date.today().addDays(-Integer.valueOf(aDays));
        Addressv = [SELECT 	AccountId__c, Address_Line_1__c, Address_Line_2__c, City__c, State__c, Zip_Code__c FROM Address__c where Preferred__c = true And CREATEDDATE >= :endDate];
        Map<Id, Address__c> accAddress= new Map<Id, Address__c>();
        for (Address__c addss : Addressv){
            accAddress.put(addss.AccountId__c, addss);
        }
        Address__c accaddvalue = new Address__c();
        Contact cont = new contact();
        Opportunity opp = new Opportunity();
        List<Account> accs = [SELECT Id, Name, PersonEmail,Phone,Owner.RSA_ID__c, CreatedDate, Owner.Name, Owner.UserName, Owner.LegacyStoreID__c, Owner.Store_Zip__c, Owner.One_Source_ID__c, CreatedBy.Name, BillingCity,BillingPostalCode,BillingState,BillingStreet,(select Declined_Survey_and_Text_Opt_In__c, Email_Opt_In__c, Text_Message_Opt_In__c from contacts), (select StageName from Opportunities) FROM Account where CREATEDDATE >= :endDate];
        //List<Account> accs = [SELECT Id, Name, PersonEmail,Phone,Owner.RSA_ID__c, CreatedDate, Owner.Name, Owner.UserName, Owner.LegacyStoreID__c, Owner.Store_Zip__c, Owner.One_Source_ID__c, CreatedBy.Name, BillingCity,BillingPostalCode,BillingState,BillingStreet,(select Declined_Survey_and_Text_Opt_In__c, Email_Opt_In__c, Text_Message_Opt_In__c from contacts), (select StageName from Opportunities) FROM Account limit 50];
        String headervalues = 'Account Name'+','+'Person Account: Email'+','+'Phone'+','+'RSA ID'+','+'Full Name'+','+'Account Owner: Full Name'+','+'Username'+','+'LegacyStoreID'+','+'Store Zip'+','+'One Source ID'+','+'Person Account: Declined Survey and Text Opt-In'+','+'Person Account: Text Message Opt-In'+','+'Person Account: Email Opt-In'+','+'Created By: Full Name'+','+'Created Date&Time'+','+'Address Line 1'+','+'Address Line 2'+','+'City'+','+'State'+','+'Zip Code'+','+'Billing Street'+','+'Billing State/Province'+','+'Billing Zip/Postal Code'+','+'Billing City'+','+'Stage';
        accCSVs.add(headervalues);
        for(Account acc: accs){
            accaddvalue = accAddress.get(acc.Id);
            system.debug('Testing ' + accaddvalue);
            system.debug('Contact Details ' + acc.contacts);
            system.debug('Opportunity Details ' + acc.Opportunities);
            system.debug('Opportunity Details ' + acc.Opportunities.size());
            String strMatch = acc.name +',' + acc.PersonEmail +',' + acc.phone +',' + acc.Owner.RSA_ID__c +','+ acc.Owner.Name +','+ acc.Owner.Username +','+ acc.Owner.LegacyStoreID__c +','+ acc.Owner.Store_Zip__c +',' + acc.Owner.One_Source_ID__c +',';
            if(acc.contacts.size() >= 1){
                if(acc.contacts.size() > 1){
                    cont = acc.contacts[0];
                }else{
                    cont = acc.contacts;
                }
                strmatch += cont.Declined_Survey_and_Text_Opt_In__c + ',' + cont.Text_Message_Opt_In__c + ',' + cont.Email_Opt_In__c + ',';
            }
            else{
                strmatch += '' + ',' + '' + ',' + '' + ',';
            }
            strmatch += acc.CreatedBy.Name + ',' + acc.CreatedDate + ',';
            if(accaddvalue != null){
                strmatch += accaddvalue.Address_Line_1__c + ','+ accaddvalue.Address_Line_2__c + ','+ accaddvalue.City__c + ','+ accaddvalue.State__c + ','+ accaddvalue.Zip_Code__c;
            }
            else{
                strmatch += '' + ','+ '' + ','+ '' + ','+ '' + ','+ '';
            }
            strmatch += acc.BillingStreet + ',' + acc.BillingState + ',' + acc.BillingPostalCode + ',' + acc.BillingCity + ',';
            
            if(acc.Opportunities.size() >= 1){
                if (acc.Opportunities.size() > 1 ){
                    opp = acc.Opportunities[0];
                }
                else{
                    opp = acc.Opportunities;
                }
                strmatch += opp.StageName;
            }
            accCSVs.add(strMatch);
        }
        return accCSVs;
    }
}