@RestResource (urlMapping='/ServiceRequests-Case/*')
global class EDAServiceRequestCase {

    global class CaseWrap {
        public string SFDCAccountId { get; set; }
        public string Description { get; set; }
        public string MarketAccount { get; set; }
        public string StoreNameStoreNumberPC { get; set; }
        public string Subject { get; set; }
        public string ProfitCenterDescription { get; set; }
        public string RequestType { get; set; }
        public string RequestSubType { get; set; }
        public string RequestOrigin { get; set; }
        public string AssigneeName { get; set; }
        public string SalesOrderNumber { get; set; }
        public string ServiceTechVendorId { get; set; }
        public string OpenDate { get; set; }
        public string ReasonCodeText { get; set; }
        public string OpenDateAsChar { get; set; }
        public string RequestPriority { get; set; }
        public string AssigneeCode { get; set; }
        public string IsTechResource { get; set; }
        public string ReasonCode { get; set; }
        public string FollowUpDate { get; set; }
        public string FollowUpDateAsChar { get; set; }
        public string ReopenDate { get; set; }
        public string ReopenDateAsChar { get; set; }
        public string ProfitCenterCode { get; set; }
        public string ServiceTechID { get; set; }
        public string ServiceTechDesc { get; set; }
        public string ScheduleDate { get; set; }
        public string ScheduleDateAsChar { get; set; }
        public string RequestSaleOrderNumber { get; set; }
        public string RequestStatus { get; set; }
        public string RequestActiveFlag { get; set; }
        public string CreatedTime { get; set; }
        public string CreatedUserID { get; set; }
        public string LastTime { get; set; }
        public string LastUserID { get; set; }
        public string CustomerPhone1 { get; set; }
        public string CustomerPhone2 { get; set; }
        public string CustomerPhone3 { get; set; }
        public string CustomerEmail { get; set; }
        public string EstimatedTimeForStop { get; set; }
        public string AccountShipto { get; set; }
        public string RequestID { get; set; }
        public string CustomerType { get; set; }
        public string CustomerID { get; set; }
        public string ShipToAddress1 { get; set; }
        public string ShipToAddress2 { get; set; }
        public string ShipToCityName { get; set; }
        public string ShipToStateCode { get; set; }
        public string ShipToZipCode { get; set; }
        public string UniqueID { get; set; }
        public string LastModifiedUserName {get; set;}
    }

    global class testWrap {
        List<CaseWrap> testwrapper;
        testWrap(){
            testwrapper = new List<CaseWrap>();
        }
    }

    @HttpPost
    global static string doPost(){
        RestResponse res = RestContext.response;

        RestRequest req = RestContext.request;
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json');
        string JSONString = req.requestBody.toString();
        Map<string, Object> Someval = (Map<string, Object>)JSON.deserializeUntyped(JSONString);
        system.debug('****** Key val: ' + Someval.keySet());

        List<Map<string, Object>> data = new List<Map<string, Object>>();
        for (Object instance : (List<Object>)Someval.get('CsrMaster')){
            data.add((Map<string, Object>)instance);
            system.debug('****** data: '+data);
        }

        Set<string> reqId = new Set<string>();
        Set<string> AccId = new Set<string>();
        Set<string> AccShipto = new Set<string>();
        List<CaseWrap> caseList = new List<CaseWrap>();
        for(integer i=0; i<data.size(); i++){
            string Someval3 = JSON.serialize(data[i]);
            CaseWrap Someval4 = (CaseWrap)JSON.deserialize(Someval3, CaseWrap.class);
            system.debug('****** data val: ' + Someval4.SFDCAccountId);
            AccId.add(Someval4.SFDCAccountId);
            reqId.add(Someval4.RequestID);
            AccShipto.add(Someval4.AccountShipto);
            caseList.add(Someval4);
        }

		List<Case> myCase = new List<Case>();
		Map<Id, List<Address__c>> custAddressMap = new Map<Id, List<Address__c>>();
        List<Contact> Con = new List<Contact>();
        List<Account> Acc = new List<Account>();
        if(data.size()>0){
            myCase = [SELECT AccountId, Address_Line_1__c, Address_Line_2__c, Address__c, CaseNumber, Case_Email__c, Case_Phone_Number__c, Category_Reason_Codes__c, City__c, Company__c,
                      ContactId, CreatedDate, Description, Estimated_time_for_stop__c, Follow_up_Date__c, Id, Legacy_Account_Ship_To__c, Legacy_Assignee__c, Legacy_Priority__c,
                      Legacy_Service_Request_ID__c, Market__c, Origin, Reason,Request_Status__c, Sales_Order__c, Status, Subject, Sub_Type__c, Tech_Scheduled_Date__c, LastModifiedDate,Type,
                      TechnicianNameScheduled__c, Technician_Schedule_Date__c, Technician_Company__c, followup_Priority_EstimatedTime__c, Technician_Address__c, Technician_ServiceReqId__c,
                      Type_of_Resolution__c, Resolution_Notes__c, Technician_Id__c, Technician_Work_Order_Processed__c
                      FROM Case WHERE Legacy_Account_Ship_To__c IN :AccShipto AND Legacy_Service_Request_ID__c IN :reqId];
            custAddressMap = AddressHelper.getAddresses(AccId);
            con = [SELECT Id, AccountId FROM Contact WHERE AccountId IN :AccId];
            Acc = [SELECT Id FROM Account WHERE ID IN :AccId];
        }

		//get all the market configuration details based on market value and routing queue name
		list<Market_Configuration__mdt> marketConfigList = CaseHelper.getMarketConfig();
		map<string, string> marketMap = new map<string, string>();
		list<string> queueNameList = new list<string>(); 
		for (Market_Configuration__mdt markConfig:marketConfigList) {
			marketMap.put(markConfig.Fulfiller_Id__c, markConfig.Routing_Queue_Name__c);
			queueNameList.add(markConfig.Routing_Queue_Name__c);
		}

		map<string, string> queueMap = new map<string, string>();
		//if the case owner name is set then get the queue name id and set the case owner
		if (queueNameList.size() > 0) {
			list<Group> groupList = [SELECT Id, DeveloperName FROM Group WHERE Type = 'Queue' AND DeveloperName IN :queueNameList];
			if (groupList.size() > 0) {
				for (Group grp:groupList) {
					queueMap.put(grp.DeveloperName, grp.Id);
				}
			}
		}

        boolean caseTechScheduled = false;
        boolean insertFlag = false;
        boolean updateFlag = false;
        for(CaseWrap cw : caseList){
			system.debug('****** CW: ' + cw);
            string cwReqId = cw.RequestID;
            string cwAccId = cw.SFDCAccountId;
            string cwAccShipto = cw.AccountShipto;

            boolean isValidAcc = false;
            if(Acc.size() > 0){
                for(Account a : Acc){
                    if(Id.valueOf(cwAccId) == a.Id)
                        isValidAcc = true;
                }
            }

            if((isValidAcc == true) && (cwAccShipto != null)){

		        boolean caseExist = false;

                for(integer i = 0; i < myCase.size(); i++){
                    if((myCase[i].Legacy_Account_Ship_To__c == cwAccShipto) && (myCase[i].Legacy_Service_Request_ID__c == cwReqId)){
                        caseExist = true;
                        system.debug('****** Case available: ' + myCase[i]);
						//add the logic to verify tech schedule detail, if any of the following fields are changed then update case
						caseTechScheduled = validateTechnicianField(myCase[i], cw);
                        break;
                    } else {
                        caseExist = false;
                    }
                }
                List<Address__c> myAddress = new List<Address__c>();
				if ((custAddressMap.size() > 0) && (custAddressMap.get(cwAccId) != null)) {
					myAddress = custAddressMap.get(cwAccId);
				}

				Case CaseToInsert = new Case();
                if (!caseExist) {
                    CaseToInsert = myCasee(cw, caseExist, cwReqId, cwAccShipto, myAddress, con, myCase, marketMap, queueMap);
                    if (!(cw.LastTime == null || cw.LastTime == '')){
                        if ((cw.CreatedTime != null) && (cw.CreatedTime != '')) {
                            CaseToInsert.CreatedDate = Date.parse(cw.CreatedTime);
                            CaseToInsert.LastModifiedDate = Date.parse(cw.LastTime);
                            system.debug('****** thisCase: ' + CaseToInsert.CreatedDate);
                        }
                    }

                    try {
	                    insert CaseToInsert;
	                    system.debug('****** Insertion successfully: ' + CaseToInsert);
	                    insertFlag = true;
                    } catch (DmlException de) {
                    	CaseHelper.dmlExceptionError(de, 'Case insert Error', 'EDAServiceRequestCase', 'doPost', JSON.serialize(CaseToInsert));
					}
                } else {
                    CaseToInsert = myCasee(cw, caseExist, cwReqId, cwAccShipto, myAddress, con, myCase, marketMap, queueMap);
                    system.debug('****** Update Case: ' + CaseToInsert.Id);

					try {
						update CaseToInsert;
	                    system.debug('****** Updated successfully: ' + CaseToInsert.Id);
	                    updateFlag = true;
                    } catch (DmlException de) {
						CaseHelper.dmlExceptionError(de, 'Case update Error', 'EDAServiceRequestCase', 'doPost', JSON.serialize(CaseToInsert));
					}
                }
            }
        }

        if (caseTechScheduled) {
            if (insertFlag && updateFlag) {
                return 'Insertion and Updation Successful...Cases are having technician scheduled';  
            } else if (insertFlag) {
                return 'Insertion Successful...Cases are having technician scheduled';
            } else if (updateFlag) {
                return 'Updation Successful...Cases are having technician scheduled';
            } else {
                return 'Cases are having technician scheduled';
            }
        } else {
            if (insertFlag && updateFlag) {
                return 'Insertion and Updation Successful...';  
            } else if (insertFlag) {
                return 'Insertion Successful...';
            } else if (updateFlag){
                return 'Updation Successful...';
            }
        }
        system.debug('****** myvals: ' + Someval);
        return 'Insertion Failed...';
    }

    @TestVisible
    private static Case myCasee(CaseWrap Someval, boolean flag, string Reqid, string AccShipto, List<Address__c> Addr, List<Contact> Con, List<Case> caseList, map<string, string> marketMap, map<string, string> queueMap){
        boolean caseTechScheduled = false;
        Case thisCase = new Case();
        system.debug('****** flag: ' + flag);

        if(flag) {
        	//if case exist then check the technician info and get the case details
            system.debug('****** myvals: ' + Someval);
            for(integer i = 0; i < caseList.size(); i++){
            	if(caseList[i].Legacy_Account_Ship_To__c == Someval.AccountShipto && caseList[i].Legacy_Service_Request_ID__c == Someval.RequestID){
                    thisCase = caseList[i];
                    caseTechScheduled = validateTechnicianField(thisCase, Someval);
					break;
                }
            }
        }
        system.debug('****** thisCase: ' + thisCase);

		//
		if (!flag || !caseTechScheduled) {
			//enter's here on insert
			// or update (if the technician is not scheduled)

	        if(!string.isBlank(Someval.SalesOrderNumber)){
	            string SOexternalID = Someval.SalesOrderNumber + ':' + Someval.SFDCAccountId;
	            thisCase.Sales_Order__c = SOexternalID;
	        } else {
	            thisCase.Sales_Order__c = null;
	        }

	        thisCase.AccountId = Id.valueOf(Someval.SFDCAccountId);
	        thisCase.Reason = Someval.ReasonCodeText;
	        thisCase.Category_Reason_Codes__c = Someval.ReasonCode;
	        thisCase.Request_Status__c = Someval.RequestStatus;
	        thisCase.Priority = Someval.RequestPriority;
	        thisCase.Legacy_Service_Request_ID__c = Someval.RequestID;
	        thisCase.Case_Phone_Number__c = Someval.CustomerPhone1;
	        thisCase.SuppliedPhone = Someval.CustomerPhone2;
	        thisCase.Case_Email__c = Someval.CustomerEmail;
            thisCase.Legacy_Last_Modified_User__c = Someval.LastModifiedUserName;
		}
		//

		if (string.isBlank(thisCase.Subject)) {
			thisCase.Subject = Someval.Subject;
		}
		if (string.isBlank(thisCase.Description)) {
			thisCase.Description = Someval.Description;
		}
		if (string.isBlank(thisCase.Origin)) {
			thisCase.Origin = Someval.RequestOrigin;
		}
		if (string.isBlank(thisCase.Type)) {
			thisCase.Type = Someval.RequestType;
		}
		if (string.isBlank(thisCase.Sub_Type__c)) {
			thisCase.Sub_Type__c = Someval.RequestSubType;
		}

		//if (string.isBlank(thisCase.ContactId)) {
	        for(integer i=0; i<Con.size(); i++){
	            if(Con[i].AccountId == Someval.SFDCAccountId)
	                thisCase.ContactId = Con[i].Id;
	        }
		//}

		//get the list of address associated to case account
        boolean AddressFlag = false;
        if(Addr.size() > 0)
            AddressFlag = true;
        /*
        List<Address__c> Addr = new List<Address__c>();
        try{
            if(AddressList.size() > 0){
                for(integer i= 0; i < AddressList.size(); i++){
                    if(Someval.SFDCAccountId == AddressList[i].AccountId__c)
                        Addr.add(AddressList[i]);
                }
            }
            if(Addr.size() > 0)
                AddressFlag = true;
        }
        catch(Exception e){}*/
        system.debug('****** Addr: ' + Addr);

		//if (string.isBlank(thisCase.Address__c)) { // comment out by sekar on 15th Apr'19 to resolve an issue #297986
	     /*   Address__c AddressToInsert = new Address__c();
	        Address__c myAddress = new Address__c();
	        if(AddressFlag){
	        	//if address is available in the pay load
				if((!string.isBlank(Someval.ShipToAddress1)) && 
				(Someval.ShipToCityName != null || Someval.ShipToCityName != '') &&
				(Someval.ShipToStateCode != null || Someval.ShipToStateCode != '') &&
				(Someval.ShipToZipCode != null || Someval.ShipToZipCode != '')){
					//if all address values are available in the pay load
					for(Address__c A: Addr) {
						string adrStr = '', cwStr = '';
						
                        /*if (!string.isBlank(A.Address_Line_1__c)) {
							adrStr = A.Address_Line_1__c + ',';
						}
						if (!string.isBlank(A.Address_Line_2__c)) {
							adrStr += A.Address_Line_2__c + ',';
						}
						if (!string.isBlank(A.City__c)) {
							adrStr += A.City__c + ',';
						}
						if (!string.isBlank(A.StatePL__c)) {
							adrStr += A.StatePL__c + ',';
						}
						if (!string.isBlank(A.Zip_Code__c)) {
							adrStr += A.Zip_Code__c;
						}

						if (!string.isBlank(Someval.ShipToAddress1)) {
							cwStr = Someval.ShipToAddress1 + ',';
						}
						if (!string.isBlank(Someval.ShipToAddress2)) {
							cwStr += Someval.ShipToAddress2 + ',';
						}
						if (!string.isBlank(Someval.ShipToCityName)) {
							cwStr += Someval.ShipToCityName + ',';
						}
						if (!string.isBlank(Someval.ShipToStateCode)) {
							cwStr += Someval.ShipToStateCode + ',';
						}
						if (!string.isBlank(Someval.ShipToZipCode)) {
							cwStr += Someval.ShipToZipCode;
						}
						
						adrStr = getAdrStr(A.Address_Line_1__c, A.Address_Line_2__c, A.City__c, A.StateList__c, A.Zip_Code__c);
						cwStr = getAdrStr(Someval.ShipToAddress1, Someval.ShipToAddress2, Someval.ShipToCityName, Someval.ShipToStateCode, Someval.ShipToZipCode);

						system.debug('****** Adr: ' + adrStr + ' : ' + cwStr);

						if(adrStr == cwStr){
							//if the pay load address matches with customer address
							if(A.Address_Type__c != 'Ship To'){
								//if the matching address type is not equal ship to then set it to ship to
								A.Address_Type__c = 'Ship To';

			                    try {
				                  	if (!Test.isRunningTest()) {
				                  		update A;
				                  	}
			                    } catch (DmlException de) {
			                    	CaseHelper.dmlExceptionError(de, 'Matching Address update Error', 'EDAServiceRequestCase', 'doPost', JSON.serialize(A));
								}
							}
							thisCase.Address__c = A.Id;
							AddressFlag = false;
							system.debug('****** Assign Address to the case: ' + thisCase.Address__c);
							break;
						}
	               }

	               if(AddressFlag){
	               		//if the pay load address not matches with the customer address then insert in address and assign to case
						//insert new address record to the Account
						system.debug('****** Insert Address to account having other Addresses');
						AddressFlag = false;
						AddressToInsert = myAdd(Someval);
						AddressToInsert.AccountId__c = Someval.SFDCAccountId;
						system.debug('****** Insert Address: ' + AddressToInsert);

	                    try {
	                    	if (!Test.isRunningTest()) {
			                  	insert AddressToInsert;
								thisCase.Address__c = AddressToInsert.Id;
	                    	}
							system.debug('****** Insert Address to account having other Addresses: ' + thisCase.Address__c);
	                    } catch (DmlException de) {
	                    	CaseHelper.dmlExceptionError(de, 'No Matching address Payload Address insert Error', 'EDAServiceRequestCase', 'doPost', JSON.serialize(AddressToInsert));
						}
					}
	            } else {
	            	//if any of the address value is not available in the pay load
	                //Assign Address to the Case
	                try{
	                    system.debug('****** myAddress initial-->'+thisCase.Address__c);
	                    for(integer i= 0; i < AddressList.size(); i++){
	                        if(AddressList[i].Preferred__c == true){
	                            thisCase.Address__c = AddressList[i].Id;
	                            Addressflag = true;
	                            system.debug('****** myAddress final: ' + thisCase.Address__c);
	                            break;
	                        }
	                    }
	                }
	                catch(Exception e){
	                    system.debug('****** myAddress initial without preferred address: ' + thisCase.Address__c);
	                    if(AddressList.size() > 0){
	                        thisCase.Address__c = AddressList[0].Id;
	                        Addressflag = true;
	                        system.debug('myAddress final-->'+thisCase.Address__c);
	                    }
	                    else{
	                        thisCase.Address__c = NULL;
	                    }
	                    Addressflag = true;
	                    system.debug('****** myAddress final without preferred address: ' + thisCase.Address__c);
	                }
	            }
	        }
	        else{
	        	//if there is no address available for the customer then insert from the pay load if available
	            if(!string.isBlank(Someval.ShipToAddress1)){
	                //insert new address record to the Acccount from the pay load
	                system.debug('****** Insert Address to account having No Address');
	                AddressToInsert = myAdd(Someval);
	                AddressToInsert.AccountId__c  = Someval.SFDCAccountId;
	                system.debug('****** AddressToInsert: ' + AddressToInsert);

	                try {
		                if (!Test.isRunningTest()) {
			                insert AddressToInsert;
			                thisCase.Address__c = AddressToInsert.Id;
		                }
		                system.debug('****** Insert Address to account having No Address'+thisCase.Address__c);
	                } catch (DmlException de) {
	                	CaseHelper.dmlExceptionError(de, 'Payload Address insert Error', 'EDAServiceRequestCase', 'doPost', JSON.serialize(AddressToInsert));
					}

	            } else {
	            	//no address available in the pay load as well as for the customer
	                thisCase.Address__c = null;
	            }
	        }
			*/
		//}
        thisCase.Address__c = AddressHelper.getCustomerEDAAddress(Someval.SFDCAccountId, Someval.ShipToAddress1, Someval.ShipToAddress2, Someval.ShipToCityName, Someval.ShipToStateCode, Someval.ShipToZipCode, Addr, 'Master');
		boolean closedCase = false;
		if ((Someval.RequestActiveFlag != null) && (Someval.RequestActiveFlag.equalsIgnoreCase('B') || Someval.RequestActiveFlag.equalsIgnoreCase('I'))) {
			// hidden in HOMES, so we can make it as closed in salesforce #280104
			closedCase = true;
            thisCase.Type_of_Resolution__c = 'Yes';
			thisCase.Status = 'Closed';
		} else {
			thisCase.Status = Someval.RequestStatus;
            thisCase.Type_of_Resolution__c = 'No';
            thisCase.Resolution_Notes__c = 'A CC Agent Needs to Review - this Case came from HOMES';
		}

		thisCase.Market__c = Someval.MarketAccount;
		thisCase.Profit_Center__c = Someval.StoreNameStoreNumberPC;

		if (string.isNotBlank(Someval.RequestStatus) && CaseHelper.caseIsClosed(Someval.RequestStatus)) {
			closedCase = true;
		}

		if (closedCase) {
			if (string.isBlank(thisCase.Type_of_Resolution__c)) {
				thisCase.Type_of_Resolution__c = 'Yes';
			}
			if (string.isBlank(thisCase.Resolution_Notes__c)) {
				if ((Someval.RequestActiveFlag != null) && (Someval.RequestActiveFlag.equalsIgnoreCase('B') || Someval.RequestActiveFlag.equalsIgnoreCase('I'))) {
					thisCase.Resolution_Notes__c = 'This Case is closed in Salesforce by EDA as it is hidden in HOMES';
				} else {
					thisCase.Resolution_Notes__c = 'A CC Agent Needs to Review - this Case came from HOMES';
				}
			}
		}

		thisCase.Follow_up_Date__c = null;
		thisCase.Tech_Scheduled_Date__c = null;
		thisCase.Estimated_time_for_stop__c = null;
		thisCase.Technician_Id__c = 0;

		if (string.isNotBlank(Someval.ServiceTechID)) {
			string techId = Someval.ServiceTechID;
			if (techId.isNumeric()) {
				thisCase.Technician_Id__c = integer.valueOf(Someval.ServiceTechID);
			} else {
				//add error log
				List<string> errMsgList = new List<string>();
				errMsgList.add('input data=' + JSON.serialize(Someval));
				CaseHelper.insertError('Payload Technician Id Issue', 'EDAServiceRequestCase', 'doPost', errMsgList);
			}
		}

		thisCase.Legacy_Account_Ship_To__c = Someval.AccountShipto;
		if (string.isNotBlank(Someval.ScheduleDate) && (Date.parse(Someval.ScheduleDate) >= date.today())){
	        // based on the user story #291797, it is set as false
	        thisCase.Technician_Work_Order_Processed__c = false;

			//based on the user story #324501, changing the case owner to a queue based on account ship to
            string queueName;
        	if(string.isNotBlank(Someval.AccountShipto)) {
        		if (marketMap.get(Someval.AccountShipto) != null) {
        			queueName = marketMap.get(Someval.AccountShipto);
        		}
        	} else {
        		if (marketMap.get('Blank') != null) {
        			queueName = marketMap.get('Blank');
        		}
        	}
        	if (string.isNotBlank(queueName) && (queueMap.get(queueName) != null)) {
        		thisCase.OwnerId = queueMap.get(queueName);
        	}
			if (!closedCase) thisCase.Status = 'Open';
        }

		//as per the work item# 247852, clearing legacy section tech schedule details
		if(thisCase.Technician_ServiceReqId__c != null){
			//Case updated from HOMES / Techinican was scheduled from SF
			//blank out all legacy field under 'Legacy data information' section except 'Legacy_Service_Request_ID__c' and update 'Service technician information' section

			//'Service technician information' section field update
            thisCase.TechnicianNameScheduled__c = Someval.ServiceTechDesc;
            thisCase.Technician_Company__c = Someval.ServiceTechVendorId;
            if (!string.isBlank(thisCase.Technician_Company__c)) {
            	thiscase.Technician_Address__c = formatTechScheduleAddress(Someval);
            } else {
            	thiscase.Technician_Address__c = null;
            }

			thisCase.Technician_Schedule_Date__c = null;
			if (!string.isBlank(Someval.ScheduleDate) && (Someval.ScheduleDate != '01/01/1900')){
    	        thisCase.Technician_Schedule_Date__c = Date.parse(Someval.ScheduleDate);
	        }
			thisCase.followup_Priority_EstimatedTime__c = null;
			if (!string.isBlank(Someval.EstimatedTimeForStop)){
				thisCase.followup_Priority_EstimatedTime__c = formatEstTimeforStop(Someval.EstimatedTimeForStop);
			}

			//'Legacy data information' section field update
			thisCase.Legacy_Assignee__c = null;
			//thisCase.Legacy_Account_Ship_To__c = null;
			thisCase.Legacy_Technician__c = null;
			thisCase.Company__c = null;
		}
		else{
			//First time case is created from HOMES / Techinican is yet to schedule from SF
			thisCase.Legacy_Assignee__c = Someval.AssigneeName;
			thisCase.Legacy_Technician__c = Someval.ServiceTechDesc;
			thisCase.Company__c = Someval.ServiceTechVendorId;
	        if (!string.isBlank(Someval.FollowupDate)){
	            thisCase.Follow_up_Date__c = Date.parse(Someval.FollowupDate);
	        }
			if (!string.isBlank(Someval.ScheduleDate) && (Someval.ScheduleDate != '01/01/1900')){
    	        thisCase.Tech_Scheduled_Date__c = Date.parse(Someval.ScheduleDate);
	        }
			if (!string.isBlank(Someval.EstimatedTimeForStop)){
				thisCase.Estimated_time_for_stop__c = formatEstTimeforStop(Someval.EstimatedTimeForStop);
			}
		}

        return thisCase;
    }
	/*
    public static string getAdrStr(string addLine1, string addLine2, string addCity, string addState, string addZip) {
		string adrStr = '';
		if (!string.isBlank(addLine1)) {
			adrStr = addLine1 + ',';
		}
		if (!string.isBlank(addLine2)) {
			adrStr += addLine2 + ',';
		}
		if (!string.isBlank(addCity)) {
			adrStr += addCity + ',';
		}
		if (!string.isBlank(addState)) {
			adrStr += addState + ',';
		}
		if (!string.isBlank(addZip)) {
			adrStr += addZip;
		}
		return adrStr;
	}
	*/
	public static boolean validateTechnicianField(Case caseObj, CaseWrap cw) {
		boolean techScheduled = false;
		//add the logic to verify tech schedule detail, if any of the following fields are changed then update case
		// Technician Name - TechnicianNameScheduled__c
		// Technician Schedule Date - Technician_Schedule_Date__c
		// Technician Scheduled Address - Technician_Address__c
		// Technician Company - Technician_Company__c
		// Estimated time for Stop - followup_Priority_EstimatedTime__c

		if (!string.isBlank(caseObj.TechnicianNameScheduled__c) && (caseObj.Technician_Schedule_Date__c != null)
		&& !string.isBlank(caseObj.Technician_Address__c) && !string.isBlank(caseObj.Technician_Company__c)
		&& !string.isBlank(caseObj.followup_Priority_EstimatedTime__c)) {
			system.debug('****** Technician Scheduled in salesforce');
            techScheduled = true;
		}

		return techScheduled;
	}

	public static string formatTechScheduleAddress(CaseWrap Someval) {
		string adrStr = '';
		if (!string.isBlank(Someval.ShipToAddress1)){
		    adrStr += Someval.ShipToAddress1 + ',';
		}
		if (!string.isBlank(Someval.ShipToAddress2)){
		    adrStr += Someval.ShipToAddress2 + ',';
		}
		if (!string.isBlank(Someval.ShipToCityName)){
		    adrStr += Someval.ShipToCityName + ',';
		}
		if (!string.isBlank(Someval.ShipToStateCode)){
		    adrStr += Someval.ShipToStateCode + ',';
		}
		if (!string.isBlank(Someval.ShipToZipCode)){
		    adrStr += Someval.ShipToZipCode;
		}
		return adrStr;
	}

	public static string formatEstTimeforStop(string ipStr){
		string opStr;
		if (!string.isBlank(ipStr)){
			//2Hour40
			string estTimeStop = ipStr;
			integer dotIndex = estTimeStop.indexOf('.');
			string hrs, tmpMin, mins = ''; 
			if(dotIndex != -1){
				hrs = estTimeStop.substring(0, dotIndex);
				tmpMin = estTimeStop.substring(++dotIndex, estTimeStop.length());
			} else {
				hrs = estTimeStop;
			}

			if((tmpMin == '8') || (tmpMin == '08')){
				mins = '5';
			} else if(tmpMin == '17'){
				mins = '10';
			} else if(tmpMin == '25'){
				mins = '15';
			} else if(tmpMin == '33'){
				mins = '20';
			} else if(tmpMin == '42'){
				mins = '25';
			} else if((tmpMin == '5') || (tmpMin == '50')){
				mins = '30';
			} else if(tmpMin == '58'){
				mins = '35';
			} else if(tmpMin == '67'){
				mins = '40';
			} else if(tmpMin == '75'){
				mins = '45';
			} else if(tmpMin == '83'){
				mins = '50';
			} else if(tmpMin == '92'){
				mins = '55';
			}

			opStr = hrs + 'Hour' + mins;
		}

		return opStr;
	}
	/*
    public static Address__c myAdd(CaseWrap Someval) {
        Address__c Add = new Address__c();
        Add.Address_Line_1__c = Someval.ShipToAddress1;
        Add.Address_Line_2__c = Someval.ShipToAddress2;
        Add.City__c = Someval.ShipToCityName;
        Add.StateList__c = Someval.ShipToStateCode;
        Add.Zip_Code__c = Someval.ShipToZipCode;
        Add.Address_Type__c = 'Ship To';
        Add.Country__c = 'USA';
        return Add;
    }

    public static List<Address__c> getAddresses(Set<string> AccId) {
        List<Address__c> AddressList = new List<Address__c>();
        AddressList = [SELECT Id, AccountId__c, Address_Line_1__c, Address_Line_2__c, City__c,
        			   StateList__c, Zip_Code__c, Country__c, Address_Type__c, Preferred__c, LastModifiedDate 
                       FROM Address__c WHERE AccountId__c IN :AccId ORDER BY LastModifiedDate DESC];
        return AddressList;
    }*/
}