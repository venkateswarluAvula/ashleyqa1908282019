@isTest
private class UpdateSalesOrderController_Test {
    
    @isTest 
    static void testGetOrderWithAshcomm() {
        //set some test data
        SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];

        SalesOrderItem__x testLineItem = TestDataFactory.initializeSalesOrderLineItems(1)[0];

        SalesOrderDAO.mockedSalesOrders.add(testSalesOrder);
        //SalesOrderDAO.mockedSalesOrderLineItems.add(testLineItem);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'order updated', 'sampleResponse',new Map<String, String>())); 
        UpdateSalesOrderController.SalesOrderWrapper response = UpdateSalesOrderController.getOrderData('00x000000000000');     
        Test.stopTest();
        //List<SalesOrderItem__x> lineItemsByOrderExternalId = SalesOrderDAO.getOrderLineItemsByOrderExternalId('xyz'); 
        //System.assert(lineItemsByOrderExternalId.size() == 2);
        //System.assert(response.isAshcommOrder, 'Order should have been Identified as ashcomm');
        
    }   

    @isTest 
    static void testGetOrderWithNonAshcommUndeliveredOrder() {
        //set some test data
        SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
        testSalesOrder.phhStoreID__c = '345550-646';
        testSalesOrder.phhSaleType__c = 'U';

        SalesOrderItem__x testLineItem = TestDataFactory.initializeSalesOrderLineItems(1)[0];               
         
        SalesOrderDAO.mockedSalesOrders.add(testSalesOrder);
        //SalesOrderDAO.mockedSalesOrderLineItems.add(testLineItem);
        UpdateSalesOrderController.SalesOrderWrapper response = UpdateSalesOrderController.getOrderData('00x000000000000');     

        //System.assert(response.isAshcommOrder == false, 'Order should not have been Identified as ashcomm');
        //System.assert(response.isEditable == true, 'Order should  have been Identified as editable');
        
    }   


    @isTest 
    static void testOrderUpdateWithAllAttributeGroupsUpdated() {
        //set some test data
        SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
        testSalesOrder.phhStoreID__c = '345550-646';
        testSalesOrder.phhSaleType__c = 'U';
        testSalesOrder.phhDesiredDate__c = Date.today();
        testSalesOrder.phhDatePromised__c = Date.today();
        testSalesOrder.phhProfitcenter__c = 5;
        testSalesOrder.phhSalesOrder__c = '009090';
        testSalesOrder.phhHot__c = false;
        testSalesOrder.fulfillerID__c = '000090-189';
        testSalesOrder.phhOrder_Notes__c = 'some notes';
        testSalesOrder.phhDeliveryComments__c = 'some comments';
        testSalesOrder.phhCustomerName__c = 'test customer';    
        

        SalesOrderItem__x testLineItem = TestDataFactory.initializeSalesOrderLineItems(1)[0];
        testLineItem.phdDeliveryType__c = 'Homex';
        testLineItem.phdShipAddress1__c = '123 test street+';
        testLineItem.phdShipAddress2__c = '';
        testLineItem.phdShipCity__c = 'Dallas+';
        testLineItem.phdShipState__c = 'TX';
        testLineItem.phdShipZip__c = '76778';

        SalesOrderDAO.mockedSalesOrders.add(testSalesOrder);
        SalesOrderDAO.mockedSalesOrderLineItems.add(testLineItem);

        Id rescheduleReasonCodeId;
        for(Reschedule_Reason_Map__mdt rrm: [Select Id, Increments_Strike_Counter__c from Reschedule_Reason_Map__mdt]){
            if(rescheduleReasonCodeId == null){
                rescheduleReasonCodeId = rrm.Id;
            }
            else if(rrm.Increments_Strike_Counter__c){
                rescheduleReasonCodeId = rrm.Id;
            }
        }

        UpdateSalesOrderController.SalesOrderWrapper orderFromUpdate = new UpdateSalesOrderController.SalesOrderWrapper();
        orderFromUpdate.currentDeliverydate = Date.Today().addDays(5);
        orderFromUpdate.hot = true;
        orderFromUpdate.asap = false;
        orderFromUpdate.deliveryMode = 'DS';
        orderFromUpdate.shipToCountry = 'USA';
        orderFromUpdate.isHotReset = false;
        orderFromUpdate.accountNumber = '8888640';
        orderFromUpdate.rdcId = '164';
        orderFromUpdate.shipToStreet1  = '435 test street+';
        orderFromUpdate.shipToStreet2 = 'APT 100+';
        orderFromUpdate.shipToCity = 'Dallas+';
        orderFromUpdate.shipToState = 'TX';
        orderFromUpdate.shipToPostalcode = '76778';
        orderFromUpdate.deliveryComments = 'some other notes';
        orderFromUpdate.orderDeliveryComments = 'some other delivery comments';
        orderFromUpdate.rescheduleReasonCodeId = rescheduleReasonCodeId;
        
        UpdateSalesOrderController.SalesOrderLineWrapper orderFromUpdateline = new UpdateSalesOrderController.SalesOrderLineWrapper();
        
        orderFromUpdateline.salesforceId = 'x010n0000001PqEAAU';
        orderFromUpdateline.externalId='200464920:001q00000163PU5AAM';
        orderFromUpdateline.deliverydate = system.today();
        orderFromUpdateline.productName = 'anything';
        orderFromUpdateline.sku = 'sku-001234';
        orderFromUpdateline.quantity = '2';
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockCallout(500, 'error', 'sampleResponse',new Map<String, String>())); 
        UpdateSalesOrderController.UpdateSalesOrderResponse response = UpdateSalesOrderController.updateSalesOrder(JSON.serialize(orderFromUpdate),'AS','AS');
        UpdateSalesOrderController.getContactStatus('00x000000000000');
        UpdateSalesOrderController.thresholdenabledcall(true,'00x000000000000');
        Test.stopTest();

        System.assert(response.hasErrors == true);

    }
    
    @isTest 
    static void testOrderUpdateWithAllAttributeGroupsUpdatedOne() {
        //set some test data
        SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
        testSalesOrder.phhStoreID__c = '345550-646';
        testSalesOrder.phhSaleType__c = 'U';
        testSalesOrder.phhDesiredDate__c = Date.today();
        testSalesOrder.phhDatePromised__c = Date.today();
        testSalesOrder.phhProfitcenter__c = 5;
        testSalesOrder.phhSalesOrder__c = '009090';
        testSalesOrder.phhHot__c = false;
        testSalesOrder.fulfillerID__c = '000090-189';
        testSalesOrder.phhOrder_Notes__c = 'some notes';
        testSalesOrder.phhDeliveryComments__c = 'some comments';
        testSalesOrder.phhCustomerName__c = 'test customer';    
        

        SalesOrderItem__x testLineItem = TestDataFactory.initializeSalesOrderLineItems(1)[0];
        testLineItem.phdDeliveryType__c = 'Homex';
        testLineItem.phdShipAddress1__c = '123 test street';
        testLineItem.phdShipAddress2__c = '';
        testLineItem.phdShipCity__c = 'Dallas';
        testLineItem.phdShipState__c = 'TX';
        testLineItem.phdShipZip__c = '76778';

        SalesOrderDAO.mockedSalesOrders.add(testSalesOrder);
        SalesOrderDAO.mockedSalesOrderLineItems.add(testLineItem);

        Id rescheduleReasonCodeId;
        for(Reschedule_Reason_Map__mdt rrm: [Select Id, Increments_Strike_Counter__c from Reschedule_Reason_Map__mdt]){
            if(rescheduleReasonCodeId == null){
                rescheduleReasonCodeId = rrm.Id;
            }
            else if(rrm.Increments_Strike_Counter__c){
                rescheduleReasonCodeId = rrm.Id;
            }
        }

        UpdateSalesOrderController.SalesOrderWrapper orderFromUpdate = new UpdateSalesOrderController.SalesOrderWrapper();
        //UpdateSalesOrderController.ASHCOMM_ORDER_SOURCE_INDICATOR = new UpdateSalesOrderController.ASHCOMM_ORDER_SOURCE_INDICATOR();
        orderFromUpdate.currentDeliverydate = Date.Today().addDays(5);
        orderFromUpdate.asap = true;
        orderFromUpdate.deliveryMode = 'DS';
        orderFromUpdate.shipToCountry = 'USA';
        orderFromUpdate.isHotReset = false;
        orderFromUpdate.accountNumber = '8888640';
        orderFromUpdate.rdcId = '164';
        orderFromUpdate.shipToStreet1  = '435 test street';
        orderFromUpdate.shipToStreet2 = 'APT 100';
        orderFromUpdate.shipToCity = 'Dallas';
        orderFromUpdate.shipToState = 'TX';
        orderFromUpdate.shipToPostalcode = '76778';
        orderFromUpdate.deliveryComments = 'some other notes\" \n \\';
        orderFromUpdate.orderDeliveryComments = 'some other delivery comments\" \n \\';
        orderFromUpdate.rescheduleReasonCodeId = rescheduleReasonCodeId;
        
        UpdateSalesOrderController.SalesOrderLineWrapper orderFromUpdateline = new UpdateSalesOrderController.SalesOrderLineWrapper();
        
        orderFromUpdateline.salesforceId = 'x010n0000001PqEAAU';
        orderFromUpdateline.externalId='200464920:001q00000163PU5AAM';
        orderFromUpdateline.deliverydate = system.today();
        orderFromUpdateline.productName = 'anything';
        orderFromUpdateline.sku = 'sku-001234';
        orderFromUpdateline.quantity = '2';

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'order updated', 'sampleResponse',new Map<String, String>())); 
        UpdateSalesOrderController.UpdateSalesOrderResponse response = UpdateSalesOrderController.updateSalesOrder(JSON.serialize(orderFromUpdate),'AS','AS');
        UpdateSalesOrderController.getStatePl();
        UpdateSalesOrderController.SearchAddress('123','USA',1);
        UpdateSalesOrderController.FormatAddress('http');
        UpdateSalesOrderController.GetDeliveryTypeOLI('00x000000000000');
        UpdateSalesOrderController.thresholdenabledcall(true,'00x000000000000');
        
        Test.stopTest();

       // System.assert(response.hasErrors == false);
        

    }   

    @isTest 
    static void getContactStatusTest() {
    	string jsonData = '[{"StatusDescription": "Test", "StatusId": "AT"}, {"StatusDescription": "Voice Confirmation", "StatusId": "VC"}]';
        Test.startTest();
		SingleRequestMock fakeResponse = new SingleRequestMock(200, 'OK', jsonData, null);
		Test.setMock(HttpCalloutMock.class, fakeResponse);
		try {
        	UpdateSalesOrderController.getContactStatus('8888300-164');
		} catch (Exception e) {}
        Test.stopTest();
    }

    @isTest 
    static void getContactStatus400Test() {
        Test.startTest();
		SingleRequestMock fakeResponse = new SingleRequestMock(400, 'OK', 'Error', null);
		Test.setMock(HttpCalloutMock.class, fakeResponse);
		try {
        	UpdateSalesOrderController.getContactStatus('8888300-164');
		} catch (Exception e) {}
        Test.stopTest();
    }
    
}