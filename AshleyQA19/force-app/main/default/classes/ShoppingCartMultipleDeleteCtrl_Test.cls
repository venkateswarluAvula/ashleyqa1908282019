@isTest
public class ShoppingCartMultipleDeleteCtrl_Test {
    public static testmethod void multipleDleteFromCarttest(){
        
        List<string> selecteditems = new List<string>();
        Account acc =  TestDataFactory.prepareCustomerInfo();
        Opportunity opp =  TestDataFactory.prepareShoppingCart(acc);
        Shopping_cart_line_item__c line = new Shopping_cart_line_item__c();
        line.Product_SKU__c ='1200014';
        line.Quantity__c =1;
        line.Delivery_Mode__c='HD';
        line.List_Price__c = 288.99;
        line.Opportunity__c = opp.Id;
        line.Average_Cost__c = 40.99;
        line.DeliveryDate__c=system.today();
        insert line;
        
        selecteditems.add(line.id);
        Test.startTest();
        string result = ShoppingCartMultipleDeleteCtrl.multipleDleteFromCart(selecteditems,acc.Id);
        system.assert(result == 'Success');
        Test.stopTest();
    }   
      public static testMethod void multipleDleteFromCartExceptionTest(){
        try{
        List<string> selecteditems = new List<string>();  
       string result = ShoppingCartMultipleDeleteCtrl.multipleDleteFromCart(null,null); 
        }catch(Exception ex){  
        }
    }      
    

}