public class ADSCommitmentController{
    
    
    @AuraEnabled
    public static List<string> getMarketValues()
    {
        List<String> marketList = new List<String>();
        for (ADS_Zip__c zip : [SELECT Market__c FROM ADS_Zip__c]) {
            if (!marketList.contains(zip.Market__c)) {
                marketList.add(zip.Market__c);
            }
        }
        return marketList;
    }
    
    
    public static string InsertedRecId;
    @AuraEnabled
    Public Static Commitment__c createRecord(Commitment__c NewCommitment)
    {
        if(NewCommitment !=null){
            insert NewCommitment;
            System.debug('NewCommitment----'+NewCommitment);
            //if(!Test.isRunningTest()){
            InsertedRecId = NewCommitment.id;
            //}
        }
        return NewCommitment;
    } 
    
    @AuraEnabled 
    Public Static Commitment__c updtRecord(Commitment__c updCommitment)  
    { 
        System.debug('updCommitment--'+updCommitment); 
        /*Commitment__c comm = [select id,Status__c from Commitment__c where id=:updCommitment.Id];
        System.debug('comm--'+comm); 
        
        if(comm.Status__c == 'Working'){
            //return returnid;
        }*/
        update updCommitment;
        return updCommitment; 
    }
    
    @AuraEnabled
    Public Static list<laneresult> CalculateValues(string originVal,string DestinationVal, string recId)
        //Public Static list<AggregateResult> CalculateValues(string originVal,string DestinationVal, string recId)
    {
        System.debug('InsertedRecId--c--'+InsertedRecId);
        System.debug('entered to calculate----'+originVal+'---DestinationVal---'+DestinationVal+'---recId---'+recId);
        
        list<Segment__c> segmantrec = [SELECT Id, Destination__c, Origin__c, Stop__c,Sequence__c FROM Segment__c 
                                       WHERE Destination__c = :DestinationVal AND Origin__c = :originVal ORDER BY Sequence__c ASC];
        
        System.debug('segmantrec ----'+segmantrec.size() +'--'+segmantrec);
        
        set<string> originSet = new set<string>();
        set<string> DestinationSet = new set<string>();
        set<string> StopSet = new set<string>();
        for(Segment__c sec : segmantrec){
            originSet.add(sec.Origin__c);
            DestinationSet.add(sec.Destination__c);
            StopSet.add(sec.Stop__c);
        }
        
        System.debug('originSet----'+originSet.size() +'--'+originSet);
        System.debug('DestinationSet----'+DestinationSet.size() +'--'+DestinationSet);
        System.debug('StopSet----'+StopSet.size() +'--'+StopSet);
        
        List<AggregateResult> adsLaneRec = new List<AggregateResult>();
        if(segmantrec.size() == 0){
            adsLaneRec = [SELECT Opportunity__c,Opportunity__r.name oppname,Opportunity__r.Account.Name AccountName, 
                          sum(of_Trips_Per_Week__c) trps, count(of_Trips_Per_Week__c) cnt 
                          FROM ADS_Lane__c 
                          WHERE Opportunity__r.StageName != 'Closed Lost' AND Opportunity__r.Type = 'New Business' 
                          AND (Opportunity__r.RecordType.Name = 'ADS' OR Opportunity__r.RecordType.Name = 'ADS Detail Page') 
                          AND Opportunity__r.Opportunity_Type__c = 'ADS' 
                          AND ADS_Lane_Origin_Market__c =: originVal AND ADS_Lane_Destination_Market__c=: DestinationVal
                          group by Opportunity__c,Opportunity__r.name,Opportunity__r.Account.Name];
        }else{
            
            adsLaneRec = [SELECT Opportunity__c,Opportunity__r.name oppname,Opportunity__r.Account.Name AccountName, 
                          sum(of_Trips_Per_Week__c) trps, count(of_Trips_Per_Week__c) cnt 
                          FROM ADS_Lane__c 
                          WHERE Opportunity__r.StageName != 'Closed Lost' AND Opportunity__r.Type = 'New Business' 
                          AND (Opportunity__r.RecordType.Name = 'ADS' OR Opportunity__r.RecordType.Name = 'ADS Detail Page') 
                          AND Opportunity__r.Opportunity_Type__c = 'ADS' 
                          AND ( (ADS_Lane_Origin_Market__c =: originSet OR ADS_Lane_Origin_Market__c =: StopSet ) 
                               AND 
                               (ADS_Lane_Destination_Market__c =: DestinationSet OR ADS_Lane_Destination_Market__c=: StopSet ) 
                              )
                          group by Opportunity__c,Opportunity__r.name,Opportunity__r.Account.Name];
            
        }
        System.debug('adsLaneRec----'+adsLaneRec.size() +'--'+adsLaneRec);
        
        //-------concatinating the lane names-----------------
        set<Id> oppIdset = new set<Id>();
        for(AggregateResult opId :adsLaneRec){
            oppIdset.add((Id)opId.get('Opportunity__c'));
        }
        
        Map<Id,string> adsnamesMap = new Map<Id,string>();
        Map<string,string> stgNameMap = new Map<string,string>();
        for (Id key : oppIdset) {
            adsnamesMap.put(key,''); stgNameMap.put(key,'');
        }
        
        list<ADS_Lane__c> adsnames = [select id,name,Opportunity__c,Opportunity__r.StageName from ADS_Lane__c 
                                      where Opportunity__c=:oppIdset Order by Opportunity__c];
        System.debug('adsnames----'+adsnames);
        string str1 = '';
        string stgName = '';
        for(ADS_Lane__c adsmap: adsnames){
            System.debug('stage----'+adsmap.Opportunity__r.StageName);
            str1 = adsnamesMap.get(adsmap.Opportunity__c);stgName = stgNameMap.get(adsmap.Opportunity__r.StageName);
            
            if(String.isEmpty(str1)){
                str1 = adsmap.Name; stgName = adsmap.Opportunity__r.StageName;
            }else{
                str1 = str1 + ', ' + adsmap.Name;	stgName = adsmap.Opportunity__r.StageName;
            }
            adsnamesMap.put(adsmap.Opportunity__c,str1);	stgNameMap.put(adsmap.Opportunity__c,stgName); 
            //System.debug('stgName----'+stgName);
        }
        
        system.debug('adsnamesMap---'+adsnamesMap);
        System.debug('stgNameMap----'+stgNameMap);
        
        //------ using collectios allocating values--------
        List<laneresult> lr = new List<laneresult>();
        laneresult lr1 = new laneresult();
        for(AggregateResult lrvalues: adsLaneRec){
            lr1 = new laneresult();	lr1.oppId = (Id)lrvalues.get('Opportunity__c');	lr1.oppname = (string)lrvalues.get('oppname');
            lr1.oppstage = stgNameMap.get((Id)lrvalues.get('Opportunity__c'));	lr1.laneNames = adsnamesMap.get((Id)lrvalues.get('Opportunity__c'));
            lr1.AccountName = (string)lrvalues.get('AccountName'); lr1.trps = (Decimal)lrvalues.get('trps');
            lr1.cnt = (Decimal)lrvalues.get('cnt');	lr.add(lr1);
        }
        system.debug('lr--'+lr);
        
        // ----- creating the Commitment Opportunities records--------
        if(recId != null || InsertedRecId != null){
            System.debug('---inserting child----');
            
            List<Commitment_Opportunities__c> ComDelList = [select id,Commitment__c from Commitment_Opportunities__c where Commitment__c =: recId];
            System.debug('ComDelList----'+ ComDelList.size()+'--->'+ComDelList);
            delete ComDelList;
            
            List<Commitment_Opportunities__c> ComOpplist = new List<Commitment_Opportunities__c>();
            //set<string> commitIdset = new set<string>();
            Map<string,string > ComOppMap = new Map<string,string >();
            
            for(AggregateResult exp: adsLaneRec){
                //ComOppMap.put((string)exp.get('ADS_Lane__c'), (string)exp.get('trps')); 
                System.debug('exp----'+exp); System.debug('opp id----'+((Id)exp.get('Opportunity__c')));
                
                Commitment_Opportunities__c ComOpp = new Commitment_Opportunities__c();
                ComOpp.Lanes__c = ((Decimal)exp.get('cnt'));	ComOpp.Trips_Per_Week__c =((Decimal)exp.get('trps'));	if(recId != null){ ComOpp.Commitment__c =recId; }     
                if(InsertedRecId != null){ ComOpp.Commitment__c =InsertedRecId; }  ComOpp.OppLanes__c = adsnamesMap.get((Id)exp.get('Opportunity__c'));
                ComOpp.Opportunity__c =((Id)exp.get('Opportunity__c'));  ComOpplist.add(ComOpp);
                
            }
            
            insert ComOpplist;
            System.debug('ComOpplist----'+ComOpplist.size()+'--ComOpplist--'+ComOpplist);
        }
        // ----- creating the Commitment Opportunities records  ends--------
        
        
        
        //return adsLaneRec;
        return lr;
    }  
    
    public class laneresult{
        @AuraEnabled public id OppId {get;set;}
        @AuraEnabled public string oppname {get;set;}
        @AuraEnabled public string oppstage {get;set;}
        @AuraEnabled public string laneNames {get;set;}
        @AuraEnabled public string AccountName {get;set;}
        @AuraEnabled public Decimal trps {get;set;}
        @AuraEnabled public Decimal cnt {get;set;}
    }
    
    
}