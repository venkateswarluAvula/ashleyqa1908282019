public class ADS_SendEmail {
    /*
    @AuraEnabled
    public static list<EmailTemplate> getTemplates(){ 
        list<EmailTemplate> emailTemp = new list<EmailTemplate>();
        emailTemp = [SELECT Id,Name,Subject,TemplateType FROM EmailTemplate WHERE TemplateType IN ('custom','text','HTML')];
        return emailTemp;
    }
    @AuraEnabled 
    public static EmailTemplate getTemplateDetails(string templteId){
        
        EmailTemplate emailTemp = new EmailTemplate();
        list<EmailTemplate> emailTempLst = new list<EmailTemplate>();
        emailTempLst = [SELECT Id,Name,Subject,TemplateType,body FROM EmailTemplate WHERE ID=: templteId];
        
        emailTemp = emailTempLst.size()>0 ? emailTempLst[0] : emailTemp;
        return emailTemp;
        
    }
	*/

    @AuraEnabled
    public static String sendEmail(String Id, String TextVal) {

        //String toaddress =  UserInfo.getUserEmail(); //your email address
        //String toaddress =  'ADLcontractreview@ashleyfurniture.com';

        system.debug('id val----'+Id);
        Legal_Review_Request__c LRR = [select Id, Name, Account__c, Account__r.OwnerId, Account__r.Owner.Email, Account__r.Owner.ManagerId, Account__r.Owner.Manager.Email from Legal_Review_Request__c where Id =: Id];
        //String LRREmail = LRR.Account__r.OwnerId ;
        system.debug('account----'+LRR);
        system.debug('account----'+LRR.Account__r.Owner.Email);
        //system.debug('account----'+LRR.Account__r.Owner.ManagerId);
        system.debug('account----'+LRR.Account__r.Owner.Manager.Email);


        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //String[] toAddresses = new String[] {toaddress};

        string legalEmailAdr = system.label.ADS_Legal_Email_Address;
        List<String> legalEmailList = legalEmailAdr.split(';');

        list<string> toAddresses = new list<string>();
        for(string legalEmail:legalEmailList) {
            toAddresses.add(legalEmail);
        }
        if (LRR.Account__r.Owner.Email != null) {
            toAddresses.add(LRR.Account__r.Owner.Email);
        }
        if (LRR.Account__r.Owner.Manager.Email != null) {
            toAddresses.add(LRR.Account__r.Owner.Manager.Email);
        }

        String[] sfEmailSe = new String[] {system.label.ADS_Inbound_Email_Address};
        String replyToaddress = system.label.ADS_Inbound_Email_Address;

        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = :system.label.ADS_From_Email_Address];
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }

        //mail.setToAddresses(toAddresses);
        //mail.setReplyTo(toaddress);
        mail.setReplyTo(replyToaddress);
        //mail.setSenderDisplayName('ADS Salesforce Team');
        mail.setSubject('Email From Salesforce ADS Team'+' RecId:'+Id+' AccId:'+LRR.Account__c);
        mail.setToAddresses(toAddresses);
        mail.setBccAddresses(sfEmailSe);
        mail.setUseSignature(true);
        //mail.setPlainTextBody('This is test email body. This mail is being sent from apex code');
        //mail.setPlainTextBody(LRR.Name);
        
        //mail.setHtmlBody(TextVal+'<br/><br/>'+LRR.Name+',<br/><br/>'+'New Legal review Request is created, Click <a href='+URL.getSalesforceBaseUrl().toExternalForm()+'/lightning/r/Legal_Review_Request__c/'+LRR.Id+'/view>here</a> to view the request.');
        mail.setHtmlBody(TextVal+'<br/><br/>'+LRR.Name);
        system.debug('mail----'+mail);

        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        for (ContentDocumentLink a : [SELECT ContentDocumentId,Id,LinkedEntityId,ContentDocument.title, ContentDocument.FileExtension FROM ContentDocumentLink WHERE LinkedEntityId = :Id]){
            for (ContentVersion docVersion : [Select Id, VersionData from ContentVersion where ContentDocumentId =:a.ContentDocumentId ]) {
                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                string fileName = a.ContentDocument.title + '.' + a.ContentDocument.FileExtension;
                efa.setFileName(fileName);
                efa.setBody(docVersion.VersionData); fileAttachments.add(efa);
                mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            }
        }

        system.debug('fileAttachments--'+fileAttachments.size());
        mail.setFileAttachments(fileAttachments);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        return 'success';
    }
    
    @AuraEnabled
    public static Legal_Review_Request__c getrecords(String RecId){
        Legal_Review_Request__c lstOfRec = [select id, Name from Legal_Review_Request__c where id =: RecId];
        return lstOfRec;
    }
    
}