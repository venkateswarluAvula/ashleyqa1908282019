@isTest
public class ObjectPermissionsOverviewController_Test {
    @isTest static void testCase() {
        String username = 'testUser';
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User newuser = new User(Alias = username, Email='u1@testorg.com',
                                EmailEncodingKey='UTF-8',lastname='Testing',
                                LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                                ProfileId = p.Id,  Country='United States',
                                TimeZoneSidKey='America/Los_Angeles', UserName=username
                                + Datetime.now().hour() + Datetime.now().minute()
                                + Datetime.now().second() +'@testorg.com');
        insert newuser;
        system.debug('Creating user: ' + newuser.id);
        
        ObjectPermissions op = new ObjectPermissions();
        op.ParentId = newuser.Id;
        
        ObjectPermissionsOverviewController.opPfWrapper wp = new ObjectPermissionsOverviewController.opPfWrapper();
        wp.NumberOfCustomObject = 5;
        wp.ProfileName = 'System Administrator';
        
        ObjectPermissionsOverviewController opc = new ObjectPermissionsOverviewController();
        
    }
}