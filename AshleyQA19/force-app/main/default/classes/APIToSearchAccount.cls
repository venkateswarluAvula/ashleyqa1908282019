/*******************************************************************************************
* Description – Apex REST service with GET method
* APIToSearchAccount for Salesforce RSA SOS Commission Changes
Author: Sudeshna Saha
********************************************************************************************/
@RestResource(urlMapping='/v1/SearchAccounts/*')
global class APIToSearchAccount {
    
    @HttpGet
    global static AccountWrapper doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        AccountWrapper response = new AccountWrapper();
        System.debug('someval-->'+RestContext.request.requestUri);
        String someval = RestContext.request.requestUri.substringAfterLast('/');
        System.debug('someval-->'+someval);
        List<Account> myAcc = new List<Account>();
        ///services/apexrest/v1/SearchAccounts/firstname=Arron+lastname=Robbin+phone=1234556+email=abc@gmail.com
        //String accountId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        If(someval !=null && someval != ''){
            myAcc = searchAccounts(someval);
        }
        System.debug('myAcc-->'+myAcc);
        if(myAcc.size()>0){
            List<User> accountOwner = new List<User>();
            accountOwner = getOwner(myAcc);
            System.debug('accountOwner-->'+accountOwner);
            if(accountOwner.size() > 0){
                response.acctList = myAcc;
                response.accOwnerList = accountOwner;
                response.status = 'Success';
                response.message = 'Successful';
                res.statusCode = 200;
            }
            else{
                response.status = 'ERROR';
                response.message = 'Not Found';
                res.statusCode = 200;
            }
        }
        else{
            response.status = 'ERROR';
            response.message = 'Not Found';
            res.statusCode = 200;
        }
        return response;
    }
    
    //If the request came to /v1/accounts, then we want to execute a search
    private static List<Account> searchAccounts(String someval) {
        getParameter myVal = getvalue(someval); 
        System.debug('myVal--->'+myVal);
		List<Account> myaccount = new List<Account>();        
        try{
            if(myVal.fName != null && myVal.lName != null && (myVal.phone != null && myVal.phone != '') && (myVal.email !=null && myVal.email !='')){
                system.debug('1');
                myaccount = [SELECT Id, Name,PersonEmail,Phone,Account.FirstName,Account.LastName,Phone_2__pc,Phone_3__pc,
                             Account.OwnerId,Account.Owner.RSA_ID__c,Current_Owner_Email__c,Email_2__pc
                             FROM Account WHERE 
                             ((FirstName = :myVal.fName AND LastName = :myVal.lName AND (PersonEmail =:myVal.email OR Email_2__pc =:myVal.email) AND 
                               (Phone =:myVal.phone OR Phone_2__pc =:myVal.phone OR Phone_3__pc =:myVal.phone ))) 
                            ];
            }
            else if(myVal.fName != null && myVal.lName != null &&(myVal.phone != null && myVal.phone != '') && (myVal.email ==null || myVal.email == '')){
                system.debug('2');
                myaccount = [SELECT Id, Name,PersonEmail,Phone,Account.FirstName,Account.LastName,Phone_2__pc,Phone_3__pc,
                             Account.OwnerId,Account.Owner.RSA_ID__c,Current_Owner_Email__c,Email_2__pc
                             FROM Account WHERE 
                             (FirstName = :myVal.fName AND LastName = :myVal.lName AND (Phone =:myVal.phone OR Phone_2__pc =:myVal.phone OR Phone_3__pc =:myVal.phone ))
                            ];
            }
            else if(myVal.fName != null && myVal.lName != null && (myVal.phone == null || myVal.phone == '') && (myVal.email !=null && myVal.email !='')){
                system.debug('3');
                myaccount = [SELECT Id, Name,PersonEmail,Phone,Account.FirstName,Account.LastName,Phone_2__pc,Phone_3__pc,
                             Account.OwnerId,Account.Owner.RSA_ID__c,Current_Owner_Email__c,Email_2__pc
                             FROM Account WHERE 
                             (FirstName = :myVal.fName AND LastName = :myVal.lName AND (PersonEmail =:myVal.email OR Email_2__pc =:myVal.email))
                            ];
            }
            else if(myVal.fName != null && myVal.lName != null && (myVal.phone == null || myVal.phone == '') && (myVal.email ==null || myVal.email == '')){
                system.debug('4');
                myaccount = [SELECT Id, Name,PersonEmail,Phone,Account.FirstName,Account.LastName,Phone_2__pc,Phone_3__pc,
                             Account.OwnerId,Account.Owner.RSA_ID__c,Current_Owner_Email__c,Email_2__pc
                             FROM Account WHERE 
                             (FirstName = :myVal.fName AND LastName = :myVal.lName)
                            ];
            }
        }
        catch(Exception e){
             System.debug('No DATA'); 
        }
        return myaccount; 
    }
    private static List<User> getOwner(List<Account> accList){
        set<Id> RSAIdSet = new set<Id>();
        System.debug('accList-->'+accList);
        for(Account acc: accList){
            RSAIdSet.add(acc.OwnerId);
        }
        System.debug('RSAIdSet-->'+RSAIdSet);
        List<User> accountOwner = new List<User>();
        try{
        accountOwner = [SELECT Id,Email,FirstName,LastName,RSA_ID__c,Username FROM User WHERE Id IN :RSAIdSet];
        }
        catch(Exception e){
             System.debug('No DATA'); 
        }
        return accountOwner;
    }
    private static getParameter getvalue(String someval){
        getParameter paramList = new getParameter();
        paramList.fName = someval.substringBefore('+lastname=');
        paramList.fName = paramList.fName.substringAfter('firstname=');
        String firstName = paramList.fName;
        if (firstName != null)   { 
            firstName= firstName.replaceAll( '%20', ' ');
            paramList.fName = firstName;
        } 
        System.debug('fName-->'+paramList.fName);
        paramList.lName = someval.substringBefore('+phone=');
        paramList.lName = paramList.lName.substringAfter('+lastname=');
        paramList.lName = paramList.lName.substringBefore('+');
        String lastName = paramList.lName;
        if (lastName != null)   { 
            lastName= lastName.replaceAll( '%20', ' ');
            paramList.lName = lastName;
        } 
        System.debug('lName-->'+paramList.lName);
        paramList.phone = someval.substringBefore('+email=');
        paramList.phone = someval.substringAfter('+phone=');
        system.debug('paramList.phone..' + paramList.phone);
        String myphone = paramList.phone;
        String cphone;
        if (myphone != null)   { 
            cphone= myphone.replaceAll( '%20', '-');
            cphone = cphone.replaceAll('\\D','');
        }  
        system.debug('myphoneval..' + cphone);
        if(cphone.length() < 10){
            cphone = '';
        }
        paramList.phone = cphone;
        System.debug('phone-->'+paramList.phone);
        paramList.email = someval.substringAfter('+email=');
        System.debug('email-->'+paramList.email);
		return paramList;
    }
    global class AccountWrapper {
        public List<Account> acctList;
        public List<User> accOwnerList;
        public String status;
        public String message;
        public AccountWrapper(){
            acctList = new List<Account>();
            accOwnerList = new List<User>();
        }
    }
    global class getParameter {
        public List<String> paramList;
        public String lName;
        public String fName;
        public String phone;
        public String email;
        public getParameter(){
            paramList = new List<String>();
        }
    }
}