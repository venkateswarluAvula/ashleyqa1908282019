@isTest
public class SalesOrderReportDataController_Test {

    @isTest
    public static void Usecase(){
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
        mock.setStaticResource('soRoutingDatamockResponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        HotOrders__c opt = new HotOrders__c();
        opt.Market_Id__c = '8888300-164';
        opt.Name = 'Kingswere - Atlanta - 8888300';
        insert opt;
        Test.startTest();
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        SalesOrderReportDataController.SalesOrderHotStatus('01/31/2019', '01/31/2019', '8888300-164');
        //SalesOrderControl.searchAccountsSOQL('200446970:001q000000raI3CAAU');
        SalesOrderReportDataController.marketvalues();
        Test.stopTest();
    }
     @isTest
    public static void SalesOrderASAPStatustest(){
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
        mock.setStaticResource('soRoutingDatamockResponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        HotOrders__c opt = new HotOrders__c();
        opt.Market_Id__c = '8888300-164';
        opt.Name = 'Kingswere - Atlanta - 8888300';
        insert opt;
        String market='8888300-164';
        String type='ASAP';    
        Test.startTest();
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        SalesOrderReportDataController.SalesOrderHotStatus('01/31/2019', '01/31/2019', '8888300-164');
        //SalesOrderControl.searchAccountsSOQL('200446970:001q000000raI3CAAU');
        SalesOrderReportDataController.SalesOrderASAPStatus(market , type);
        Test.stopTest();
    }
    
    @isTest
    public static void SalesOrderDeliveryConfirmationStatustest(){
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
        mock.setStaticResource('soRoutingDatamockResponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        HotOrders__c opt = new HotOrders__c();
        opt.Market_Id__c = '8888300-164';
        opt.Name = 'Kingswere - Atlanta - 8888300';
        insert opt;
        String market='8888300-164';
        String type='ASAP';    
        Test.startTest();
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        SalesOrderReportDataController.SalesOrderHotStatus('01/31/2019', '01/31/2019', '8888300-164');
        //SalesOrderControl.searchAccountsSOQL('200446970:001q000000raI3CAAU');
        SalesOrderReportDataController.SalesOrderDeliveryConfirmationStatus('2018-09-29', '2018-09-29', 'Kingswere Georgia - 8888300');
        Test.stopTest();
    }
    
    @isTest
    public static void soConfirmDatatest(){
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
        mock.setStaticResource('soRoutingDatamockResponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        HotOrders__c opt = new HotOrders__c();
        opt.Market_Id__c = '8888300-164';
        opt.Name = 'Kingswere - Atlanta - 8888300';
        insert opt;
        String market='8888300-164';
        String type='ASAP';    
        Test.startTest();
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        SalesOrderReportDataController.SalesOrderHotStatus('01/31/2019', '01/31/2019', '8888300-164');
        //SalesOrderControl.searchAccountsSOQL('200446970:001q000000raI3CAAU');
        SalesOrderReportDataController.soConfirmData('8888300-164','200466320',1,'23','2019-03-27', 1, '2018-09-29', '2018-09-29');
        Test.stopTest();
    }
      
    @isTest
    public static void SalesOrderVIPStatusTest() {
        List<Account> accList = new List<Account>();
        accList = TestDataFactory.initializeAccounts(1);
        insert accList;

        List<Contact> contList = new List<Contact>();
        contList = TestDataFactory.initializeContacts(accList[0].Id, 1);
        insert contList;

		Case caseObj = new Case(Sales_Order__c = '17331400:001q000000raDkvAAE', Status = 'New', Origin = 'Phone',
                            Type = 'Open Order Inquiry', Subject = 'Test',
                            Description = 'Test', AccountId = accList[0].Id, ContactId = contList[0].Id );
        insert caseObj;

		Case caseObj1 = new Case(Sales_Order__c = '17331401:001q000000raDkvAAE', Status = 'Closed in Salesforce', Origin = 'Phone',
                            Type = 'Open Order Inquiry', Subject = 'Test', Type_of_Resolution__c = 'Test', Resolution_Notes__c = 'Test',
                            Description = 'Test', AccountId = accList[0].Id, ContactId = contList[0].Id );
        insert caseObj1;

		string caraEndpoint = 'https://login.microsoftonline.com/5a9d9cfd-c32e-4ac1-a9ed-fe83df4f9e4d/oauth2/token';
        string caraTokenRespBody = '{'
                +'"token_type": "Bearer",'
                +'"scope": "user_impersonation",'
                +'"expires_in": "3600",'
                +'"ext_expires_in": "3600",'
                +'"expires_on": "1557751385",'
                +'"not_before": "1557747485",'
                +'"resource": "https://5a9d9cfd-c32e-4ac1-a9ed-fe83df4f9e4d/cara-api-qa",'
                +'"access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IkhCeGw5bUFlNmd4YXZDa2NvT1UyVEhzRE5hMCIsImtpZCI6IkhCeGw5bUFlNmd4YXZDa2NvT1UyVEhzRE5hMCJ9.eyJhdWQiOiJodHRwczovLzVhOWQ5Y2ZkLWMzMmUtNGFjMS1hOWVkLWZlODNkZjRmOWU0ZC9jYXJhLWFwaS1xYSIsImlzcyI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0LzVhOWQ5Y2ZkLWMzMmUtNGFjMS1hOWVkLWZlODNkZjRmOWU0ZC8iLCJpYXQiOjE1NTc3NDc0ODUsIm5iZiI6MTU1Nzc0NzQ4NSwiZXhwIjoxNTU3NzUxMzg1LCJhY3IiOiIxIiwiYWlvIjoiNDJaZ1lORHRjcjY2NGx4eitMWmpudE1GRG9YSlcrVGZNMzV5U0NoOHcrb21tV2t0aVI4QSIsImFtciI6WyJwd2QiXSwiYXBwaWQiOiJhYTQ4ZmUxMS00ZTQyLTRkMGUtYmQ0My01YTZiNmRiYThkZTkiLCJhcHBpZGFjciI6IjEiLCJmYW1pbHlfbmFtZSI6InNfQ1JNUUFDYXJhIiwiaXBhZGRyIjoiMTIzLjE3Ni4zNC4xMjMiLCJuYW1lIjoic19DUk1RQUNhcmEiLCJvaWQiOiJlZDk0MGM3Zi0wMjEwLTQ0ZGUtYjI5OC01YWIwODI2ZGM3MTciLCJvbnByZW1fc2lkIjoiUy0xLTUtMjEtMzM5MjIwNjA3OC03MTg0MDUxMzEtMzAxMzI4MTI0NC0yNDMzMzAiLCJzY3AiOiJ1c2VyX2ltcGVyc29uYXRpb24iLCJzdWIiOiJGYmFnNWJWTUg2clhTWnJsVjd6NzNEQl90UDNPbm5pLW5oY3hjM1ptdjNnIiwidGlkIjoiNWE5ZDljZmQtYzMyZS00YWMxLWE5ZWQtZmU4M2RmNGY5ZTRkIiwidW5pcXVlX25hbWUiOiJzX0NSTVFBQ2FyYUBhc2hsZXlmdXJuaXR1cmUuY29tIiwidXBuIjoic19DUk1RQUNhcmFAYXNobGV5ZnVybml0dXJlLmNvbSIsInV0aSI6Il9IS3lva0h4d2ttTnhQdkRqR1JFQUEiLCJ2ZXIiOiIxLjAifQ.pkDwTJ4kk6uYF-D2ftVjROOVCjSFBJzZNuAm3DdxcbntTuFKAgrQiFo4UmFYz4RTE8gGdds02NC5CTfNzy3r3vBuCfeq4FTWLvO_3X2HoBpHUHV6SEQICWuuoslpRtZ0vv96a2LBiQ1aaE3WfZkm1mXh9pKMJS7vWLwzdv6jbJyhe4HZa5oiaeVeEFx9ImfSfePNqwBZj3Z3xRr3wFfoLh1tYji_brTb2Y31BCENWbJLBQCfMJoaKA9sCNhuA6-Bga6KobAihwaU-Xc1THX4pmtXQvsFBZIpt7fvW8xQgLSUWy3muWHoaRPLL6l5m_qv-ErdsglCPQYlRV528ZJjhg",'
                +'"refresh_token": "AQABAAAAAADCoMpjJXrxTq9VG9te-7FXYmzBkOeZcO3R6o277GSbMvKilJrVIJgtNNGY2GR7FuycYbyn8bmlttinuXM3Mr5i6HAHYpuTyPJgQoF9dW3rmldZiUB7ULNgQdmt4Yya4FlCl9cZC3Q-cm6e1TKWBY_Rtp_MWley0x_RYqoSkXW81okqtVgVhgpKhhk0pZDryIrkEL2BSQB9qxuf-Mz_Zohg35KDDAQeap0b3aSX9UGwguxfny8sR3HyUYaia545F29M1nVbhJl_uwNibNBNavDZKUqG3_SpkPdcwjbAQ8QvLiMkEwfeUSFEnwNQv1xb1uFGSh9p0VvZev46uxgqYjfrdbx5SO2fkhr5l9n_zrxz2x_k6WPs_gmfqh8xHQc8j102_6WFPQ4s9oIjTpW3GYXaZ8ke9Ll2naht26tmGZIdUa0czMQD6KQA5HXREnfHBMowgCaiiDTYJT9aXQzdOdQmS3aAoUg8R8Ji0WpSDusiDFr1uwD4gukI-Hyb9ncjf4IfcwNq9FNH7sjDSiYVNm0r_wHbzTbKKcdXwlOu8H4dck9-07xKJJfQix-nR7TcNmApdoOKhGSleJgijMzOBAjsKVdMbTA3VI7vSsWChCVrB3Qb7CaW8bkc7H5yDw8W7c7MeumYEah-PgICaoD_cVMFQ4NUAuF-Ueef0X-jhVjqJDWeEMfxs2M7leP1tJMRmsV43aBABTPaEJadWVakiqqFPNBI3dfIWQY4sE_5OwAU-LzfbwZhiBtbSPlpRh1mUpU-rJQAkpoDIrP_Hsppw7FvIAA",'
                +'"id_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJhdWQiOiJhYTQ4ZmUxMS00ZTQyLTRkMGUtYmQ0My01YTZiNmRiYThkZTkiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC81YTlkOWNmZC1jMzJlLTRhYzEtYTllZC1mZTgzZGY0ZjllNGQvIiwiaWF0IjoxNTU3NzQ3NDg1LCJuYmYiOjE1NTc3NDc0ODUsImV4cCI6MTU1Nzc1MTM4NSwiYW1yIjpbInB3ZCJdLCJmYW1pbHlfbmFtZSI6InNfQ1JNUUFDYXJhIiwiaXBhZGRyIjoiMTIzLjE3Ni4zNC4xMjMiLCJuYW1lIjoic19DUk1RQUNhcmEiLCJvaWQiOiJlZDk0MGM3Zi0wMjEwLTQ0ZGUtYjI5OC01YWIwODI2ZGM3MTciLCJvbnByZW1fc2lkIjoiUy0xLTUtMjEtMzM5MjIwNjA3OC03MTg0MDUxMzEtMzAxMzI4MTI0NC0yNDMzMzAiLCJzdWIiOiJhVURkWDBOTWRzVFJpSkRGLTY1OWNfZklpOWZLUi1TVHlpdzFsYnVWRTNVIiwidGlkIjoiNWE5ZDljZmQtYzMyZS00YWMxLWE5ZWQtZmU4M2RmNGY5ZTRkIiwidW5pcXVlX25hbWUiOiJzX0NSTVFBQ2FyYUBhc2hsZXlmdXJuaXR1cmUuY29tIiwidXBuIjoic19DUk1RQUNhcmFAYXNobGV5ZnVybml0dXJlLmNvbSIsInZlciI6IjEuMCJ9."'
            +'}';

        String vipEndpoint = System.label.VIPOrdersAPI + 'marketID=Kingswere Georgia - 8888300&startDate=2018-09-29&endDate=2018-09-29&type=0';
		string vipRespBody = '['
			+'  {'
			+'    "SFContactID":"001q000000raDkvAAE", "OrderNumber":"17331400", "Market":"Kingswere Georgia - 8888300", "AccountShipTo": "8888300-164", "DeliveryDate":"2018-09-29", "Type": "PDI", "VIP": "True", "LIB":"True", "SKU":"B213-84"'
			+'  },'
			+'  {'
			+'    "SFContactID":"001q000000raDkvAAE", "OrderNumber":"17331401", "Market":"Kingswere Georgia - 8888300", "AccountShipTo": "8888300-164", "DeliveryDate":"2018-09-29", "Type": "PDI", "VIP": "True", "LIB":"True", "SKU":"B213-84"'
			+'  },'
			+'  {'
			+'    "SFContactID":"001q000000raDkvAAE", "OrderNumber":"17331402", "Market":"Kingswere Georgia - 8888300", "AccountShipTo": "8888300-164", "DeliveryDate":"2018-09-29", "Type": "PDI", "VIP": "True", "LIB":"True", "SKU":"B213-84"'
			+'  }'
			+']';

        SingleRequestMock caraTokenResp = new SingleRequestMock(200, 'OK', caraTokenRespBody, null);
        SingleRequestMock vipResp = new SingleRequestMock(200, 'OK', vipRespBody, null);

        Map<String, HttpCalloutMock> endpoint2TestResp = new Map<String,HttpCalloutMock>();
        endpoint2TestResp.put(caraEndpoint, caraTokenResp);
        endpoint2TestResp.put(vipEndpoint, vipResp);

        HttpCalloutMock multiCalloutMock = new MultiRequestMock(endpoint2TestResp);

        Test.startTest();
		Test.setMock(HttpCalloutMock.class, multiCalloutMock);
        list<SalesOrderVIPControlWrapper> responseList = SalesOrderReportDataController.SalesOrderVIPStatus('2018-09-29', '2018-09-29', 'Kingswere Georgia - 8888300', '0');
        Test.stopTest();
  
}

}