@isTest
public class caseAttachmentBatch_test {	
    static testmethod void caseUpdateattachment(){
        Account ac = new Account();
        ac.name = 'test account';
        insert ac;
        case ca = new case();
        ca.AccountId = ac.id;
        insert ca;
        Contact con = new Contact(AccountId = ac.id,LastName = 'testaccount');
        insert con;        
        Task tk = new task();
        tk.Description = 'Sms replay from customer 13176664444';
        tk.WhoId = con.id;
        tk.WhatId = ca.id;
        tk.Twilio_Sent_To_Phone_Number__c = '+13176664444';
        tk.Status = 'New Customer Reply';
        insert tk;
        String myString = 'StringToBlob';
		Blob myBlob = Blob.valueof(myString);
        
		Attachment att = new Attachment();
        att.ParentId = tk.Id;
		att.Body = myBlob;
        att.Name = 'Testimage.pdf';
        att.ContentType = 'application/pdf';
        insert att;
        
        caseAttachmentBatch caseAtt = new caseAttachmentBatch();
        //caseAtt.query = 'select id, Body, Name, ParentId, OwnerId from Attachment where ParentId = ' + tk.id + ']';
        DataBase.executeBatch(caseAtt);        
    }
    
    static testmethod void caseUpdateattachmentquery(){
        Account ac = new Account();
        ac.name = 'test account';
        insert ac;
        case ca = new case();
        ca.AccountId = ac.id;
        insert ca;
        Contact con = new Contact(AccountId = ac.id,LastName = 'testaccount');
        insert con;        
        Task tk = new task();
        tk.Description = 'Sms replay from customer 13176664444';
        tk.WhoId = con.id;
        tk.WhatId = ca.id;
        tk.Twilio_Sent_To_Phone_Number__c = '+13176664444';
        tk.Status = 'New Customer Reply';
        insert tk;
        String myString = 'StringToBlob';
		Blob myBlob = Blob.valueof(myString);
        
		Attachment att = new Attachment();
        att.ParentId = tk.Id;
		att.Body = myBlob;
        att.Name = 'Testimage.pdf';
        att.ContentType = 'application/pdf';
        insert att;
        
        caseAttachmentBatch caseAtt = new caseAttachmentBatch();
        caseAtt.query = 'select id, Body, Name, ParentId, OwnerId from Attachment where ParentId != null';
        DataBase.executeBatch(caseAtt);        
    }
}