@isTest
public class Case_AddressDetailsControolerTest {
    private static testMethod void test1(){
        //Creating Account
        TestDataFactory.initializeAccounts(1);
        
        //Creating Account
        List<Account> testAccount = TestDataFactory.initializeAccounts(1);
        insert testAccount;
        
        //Creating Contact
        List<Contact> testContact = TestDataFactory.initializeContacts(testAccount[0].Id,1);
        insert testContact;
        
        //Creating Case
        List<Case> testCase = TestDataFactory.initializeCases(testAccount[0].Id,testContact[0].Id,1);  
        insert testCase;
        
        //Calling the Controller Method
        Case result = Case_AddressDetailsController.getDetails(testCase[0].Id); 
        Contact result1 = Case_AddressDetailsController.getContactDetails(testCase[0].Id);
        Contact result2 = Case_AddressDetailsController.getContactDetails(null);
        Case result3 = Case_AddressDetailsController.getDetails(null); 
        //As it is not associated to SalesOrder the result will be null
    }
}