@isTest
public class ShoppingCartMultipleFppCtrl_Test {
    
    public static testMethod void multipleFppUpdateCartTest(){
        string fpp = 'Test';
        List<string> selecteditems = new List<string>();
        
        Account acc =  TestDataFactory.prepareCustomerInfo();
        Opportunity opp =  TestDataFactory.prepareShoppingCart(acc);
        List<Shopping_cart_line_item__c> lineItems =   TestDataFactory.prepareShoppingCartLineItems(opp);
        selecteditems.add(lineItems[0].id);
        selecteditems.add(lineItems[1].id);
        
        string result = ShoppingCartMultipleFppCtrl.multipleFppUpdateCart(fpp, selecteditems);
        system.assert(result == 'Success');
    }
    public static testMethod void multipleFppUpdateCartexceptiontest(){
        try{
       string result = ShoppingCartMultipleFppCtrl.multipleFppUpdateCart(null, null); 
        }catch(Exception ex){  
        }
    }
    
}