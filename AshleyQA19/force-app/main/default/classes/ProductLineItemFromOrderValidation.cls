public class ProductLineItemFromOrderValidation implements PLIInstance{
    Static String var1;
    Final integer pi;
    @AuraEnabled
    public static boolean Validation(String src, ID PLIID){ 
        system.debug('src'+src);
        system.debug('PLIID'+PLIID);
        Boolean  isValidNum;
        Util__c PLIStatusUpdate = new Util__c();
        try{
            PLIStatusUpdate = [SELECT ID,Callout_Status__c,PLI_ID__c,SetupOwnerId from Util__c 
                               where PLI_ID__c =: PLIID];
            if (src == 'PLIH'){
                PLIStatusUpdate.Callout_Status__c = true;
                PLIStatusUpdate.PLI_ID__c = PLIID;
                Update PLIStatusUpdate;
                isValidNum = PLIStatusUpdate.Callout_Status__c;
                system.debug('isValidNum-->'+isValidNum);
                return isValidNum;
            }
        }
        catch(Exception ex){
            if (src == 'PLIH'){
                try{
                    PLIStatusUpdate = [SELECT ID,Callout_Status__c,PLI_ID__c,SetupOwnerId from Util__c];
                    PLIStatusUpdate.Callout_Status__c = true;
                    PLIStatusUpdate.PLI_ID__c = PLIID;
                    Upsert PLIStatusUpdate;
                    isValidNum = PLIStatusUpdate.Callout_Status__c;
                    system.debug('isValidNum-->'+isValidNum);
                    return isValidNum;
                }
                catch(Exception e){
                    PLIStatusUpdate.Callout_Status__c = true;
                    PLIStatusUpdate.PLI_ID__c = PLIID;
                    Insert PLIStatusUpdate;
                    isValidNum = PLIStatusUpdate.Callout_Status__c;
                    system.debug('isValidNum-->'+isValidNum);
                    return isValidNum;
                }
            }
        }
        return false;
    }
    
    
    @AuraEnabled
    public static boolean UpdateValidation(String src, ID PLIID){ 
        system.debug('src'+src);
        system.debug('PLIID'+PLIID);
        Boolean  isValidNum;
        Util__c PLIStatusUpdate = new Util__c();
        try{
            PLIStatusUpdate = [SELECT ID,Callout_Status__c,PLI_ID__c from Util__c where PLI_ID__c =: PLIID];
            PLIStatusUpdate.Callout_Status__c = false;
            PLIStatusUpdate.PLI_ID__c = PLIID;
            Update PLIStatusUpdate;
            isValidNum = PLIStatusUpdate.Callout_Status__c;
            system.debug('isValidNum-->'+isValidNum);
            return isValidNum;
        }
        catch(Exception ex){
            if (src == 'PLIH'){
                PLIStatusUpdate.Callout_Status__c = false;
                PLIStatusUpdate.PLI_ID__c = PLIID;
                Upsert PLIStatusUpdate;
                isValidNum = PLIStatusUpdate.Callout_Status__c;
                system.debug('isValidNum-->'+isValidNum);
                return isValidNum;
            }
        }
        return false;
    }
    
    @AuraEnabled
    public static boolean updateTrackingNumber(ID PLIID, POJSON data){ 
        system.debug('PLIID'+PLIID);
        system.debug('data-->'+data);
        ProductLineItem__c PLITrackingNumUpdate = new ProductLineItem__c();
        try{
            PLITrackingNumUpdate = [SELECT id,Part_Order_Number__c,Replacement_Part_Item_Number__c, Part_Order_Tracking_Number__c,Defect_Location__c,Item_Defect__c,Ashley_Direct_Link_ID__c, Part_Order_Shipping_Date__c,Part_Order_Num__c FROM ProductLineItem__c WHERE Id =: PLIID];
            if(data.ExtDetail.UPSTracking!='' && data.ExtDetail.UPSTracking != null){
                PLITrackingNumUpdate.Part_Order_Tracking_Number__c = data.ExtDetail.UPSTracking;
            }
            if(data.ExtDetail.DefectLocation!='' && data.ExtDetail.DefectLocation != null){
                PLITrackingNumUpdate.Defect_Location__c = data.ExtDetail.DefectLocation;
            }
            if(data.ExtDetail.DefectCode!='' && data.ExtDetail.DefectCode != null){
                PLITrackingNumUpdate.Item_Defect__c = data.ExtDetail.Defect;
            }
            if(data.ExtDetail.RPKey!='' && data.ExtDetail.RPKey != null){
                PLITrackingNumUpdate.Part_Order_Number__c = data.ExtDetail.RPKey;
            }
            if(data.ExtDetail.Model!='' && data.ExtDetail.Model != null){
                PLITrackingNumUpdate.Replacement_Part_Item_Number__c = data.ExtDetail.Model;
            }
            PLITrackingNumUpdate.isReload__c = true;
            system.debug('ShipDate-->'+data.ExtDetail.UPSTracking);
            if(data.ExtDetail.ShipDate!='' && data.ExtDetail.ShipDate != null && data.ExtDetail.ShipDate.length() > 7){
                String String1 = data.ExtDetail.ShipDate;
                String String2 = '-';
                String newString = String1.substring(0, 4) + '-';
                newString = newString.substring(0,newString.length()) + 
                    String1.substring(4,6);
                newString = newString.substring(0,newString.length()) + '-';
                newString = newString.substring(0,newString.length()) +
                    String1.substring(6,String1.length());
                System.debug('newString-->'+newString);
                PLITrackingNumUpdate.Part_Order_Shipping_Date__c = Date.valueOf(newString);
            }
           // PLITrackingNumUpdate.Part_Order_Shipping_Date__c = Date.valueOf(data.ExtDetail.ShipDate);
            system.debug('PLITrackingNumUpdate-->'+PLITrackingNumUpdate);
            Update PLITrackingNumUpdate;
            system.debug('status-->'+true);
            return true;
        }
        catch(Exception ex){
            
            return false;
        }
    }
}