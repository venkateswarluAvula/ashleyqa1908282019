@isTest
public class DispatchTrackOnCaseCtrl_Test {
    
    @isTest
    public static void dispatchTrackTest() 
    {
        List<Case> caselist = new List<Case>();
        Case Casee = new Case();
        Casee.Subject='Test record';
        Casee.Type = 'General Inquiry';
        Casee.Description = 'test record';
        Casee.Origin = 'Phone';
        Casee.Status = 'New';
        Casee.Priority='Medium';
        Casee.Legacy_Service_Request_ID__c = '88804';
        Casee.Technician_ServiceReqId__c = '789451';
        Casee.Profit_Center__c = 'SOUTHLAKE - 133 - 23';
        Casee.Estimated_time_for_stop__c = '2';
        Casee.Request_Status__c = 'New';
        Casee.Legacy_Assignee__c = '1';
        Casee.Legacy_Account_Ship_To__c = '8888300-164';
        Casee.Reason = 'Delivery Issue';
        Casee.Category_Reason_Codes__c = 'Delivery Issue';
        Casee.Tech_Scheduled_Date__c =date.parse('12/27/2018');
        Casee.TechnicianNameScheduled__c = null;
        Casee.Technician_Schedule_Date__c = null;
        Casee.Follow_up_Date__c = date.parse('12/25/2018');
        Casee.CreatedDate = date.parse('5/31/2018');
        Insert Casee;
        caselist.add(Casee);
        System.debug('Insertion case'+Casee);
        String str = caselist[0].Profit_Center__c;
        String str2 = str.substringAfter('- ');
        String RequestId = caselist[0].Technician_ServiceReqId__c;
        String ProfitCenterCode = str2.substringAfter('- ');
        String StoreId = str2.substringBefore(' -');
        String errReqId = 'RequestId not available.';
        
        // Create custom setting
        Integration_Settings__c integSetting = new Integration_Settings__c(
        	Name = 'Dispatch_Track',
        	End_Point_URL__c = 'https://ashleyhomestore.dispatchtrack.com/track_order/'+ StoreId + 'S-' + ProfitCenterCode +'-' + RequestId);
        insert integSetting;
                
        DispatchTrackOnCaseCtrl.formDispatchTrackUrl(Casee.Id);
    }

     @isTest
    public static void dispatchTrackTest1() 
    {
        List<Case> caselist = new List<Case>();
        Case Casee = new Case();
        Casee.Subject='Test record';
        Casee.Type = 'General Inquiry';
        Casee.Description = 'test record';
        Casee.Origin = 'Phone';
        Casee.Status = 'New';
        Casee.Priority='Medium';
        Casee.Legacy_Service_Request_ID__c = '88804';
        //Casee.Technician_ServiceReqId__c = '789451';
        Casee.Profit_Center__c = 'SOUTHLAKE - 133 - 23';
        Casee.Estimated_time_for_stop__c = '2';
        Casee.Request_Status__c = 'New';
        Casee.Legacy_Assignee__c = '1';
        Casee.Legacy_Account_Ship_To__c = '8888300-164';
        Casee.Reason = 'Delivery Issue';
        Casee.Category_Reason_Codes__c = 'Delivery Issue';
        Casee.Tech_Scheduled_Date__c =date.parse('12/27/2018');
        Casee.TechnicianNameScheduled__c = null;
        Casee.Technician_Schedule_Date__c = null;
        Casee.Follow_up_Date__c = date.parse('12/25/2018');
        Casee.CreatedDate = date.parse('5/31/2018');
        Insert Casee;
        caselist.add(Casee);
        System.debug('Insertion case'+Casee);
        String str = caselist[0].Profit_Center__c;
        String str2 = str.substringAfter('- ');
        String RequestId = caselist[0].Legacy_Service_Request_ID__c;
        String ProfitCenterCode = str2.substringAfter('- ');
        String StoreId = str2.substringBefore(' -');
        String errReqId = 'RequestId not available.';
                
        DispatchTrackOnCaseCtrl.formDispatchTrackUrl(Casee.Id);
    }
    
}