@isTest
public class CreateNoteRecordTest {
    static testMethod void TestData() {
        List<Account> accList = new List<Account>();
        accList = TestDataFactory.initializeAccounts(1);
        insert accList;
        
        List<Contact> contList = new List<Contact>();
        contList = TestDataFactory.initializeContacts(accList[0].Id, 1);
        insert contList;
        
        // Insert Case
        Case caseObj = new Case(Sales_Order__c = '17331400:500q000000JjXzTAAV', Status = 'New', Origin = 'Phone',
                                Type = 'Open Order Inquiry', Subject = 'Test',
                                Description = 'Test', AccountId = accList[0].Id, ContactId = contList[0].Id);
        insert caseObj;
        system.debug(caseObj);
        
        ContentNote nt = new ContentNote();
        nt.Title = 'Test Note';
        insert nt;
        
        ContentDocument cd=[select id from ContentDocument where id=:nt.Id];
        ContentDocumentLink cdl=new ContentDocumentLink();
        cdl.ContentDocumentId=cd.id;
        cdl.LinkedEntityId=caseObj.Id;
        cdl.ShareType='V';
        cdl.Visibility='AllUsers';
        insert cdl;
        
        Test.startTest();
        CreateNoteRecord.createRecord(nt,caseObj.id);
        CreateNoteRecord.searchNotes(caseObj.id);
        Test.stopTest();
    }
}