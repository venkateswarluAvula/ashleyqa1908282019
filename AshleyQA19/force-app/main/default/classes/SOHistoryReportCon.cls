public class SOHistoryReportCon {
    public Account AccVal{get; set;}
    public List<SalesOrder__x> lstselectedSo{get; set;}
    public List<SoHistoryReportWrapper> soHis{get; set;}
    public Map<string,List<inneritemswrap>> mapinneritemsval {get; set;}
    public List<outeritemswrap> SetsoOrders {get; set;}
    public Boolean flag {get;set;}
    public set<string> sonum{get;set;}
    public string fromdate {get;set;}
    public decimal InvCurrentBal {get;set;}
    
    private integer lngQty = 0;
    private decimal price = 0;
    private decimal extcst = 0;
    private decimal tax = 0;
    private decimal taxcount = 0;
    private decimal gross = 0;
    private decimal invoicettl = 0;
    public Map<string,integer > mapItemsQty {get; set;}
    public Map<string,decimal > totalprice {get; set;}
    public Map<string,decimal > extcost {get; set;}
    public Map<string,decimal > taxamt {get; set;}
    public Map<string,decimal > grosspercent {get; set;}
    public Map<string,decimal > invoicetotal {get; set;}
    
    public SOHistoryReportCon(){
        pdfgeneration();
    }
    
    Public void pdfgeneration(){
        //---- getting Account Id--------
        string AccId = ApexPages.currentPage().getParameters().get('id');
        system.debug('AccId----'+AccId);
        AccVal = [select id,Name from Account where id =: AccId];
        
        //----- getting Salesorder numbers------
        string SOVal = ApexPages.currentPage().getParameters().get('SO');
        system.debug('SOVal----'+SOVal);
        
        //List<String> SOVallst = SOVal.split(',');
        //system.debug('SOVallst--'+SOVallst.size()+'--'+SOVallst);
        
        string dwlfile = ApexPages.currentPage().getParameters().get('dwl');
        system.debug('dwlfile----'+dwlfile);
        if(dwlfile == 'download'){
            String myGeneratedFileName = 'PurchaseHistory.pdf';
            Apexpages.currentPage().getHeaders().put('content-disposition', 'attachment; filename='+myGeneratedFilename);
        }
        
        //SOVal----(300522790, 21111310)
        //{ "SalesOrders": ["200462400", "200462530"] } 
        
        SORoutingAuthorization sora = new SORoutingAuthorization();
        String accessTkn = sora.accessTokenData();   
        system.debug('accessTkn-----------'+ accessTkn);
        
        String response;
        String EPoint = system.label.ReportHistoryAPI+AccId;
        //String EPoint = 'https://test.cara.ashleyretail.com/cara/customer/PurchaseHistoryReport?SFID=001q0000014SzryAAC';
        
        //string selectedOrders1 = '{"SalesOrders": ["200462400", "200462530"]} ';
        
        string selectedOrders = '';
        If(Test.isRunningTest()){
            selectedOrders = '{ "SalesOrders": [{"200462400"}, {"200462530"}] }' ;
        }else{
            selectedOrders = SOVal.replace('(','["').replace(')','"]').replace(', ','","');
            selectedOrders = '{"SalesOrders": '+selectedOrders+'}';
            system.debug('selectedOrders----'+selectedOrders);
        }
        //-----post method-----------
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse httpResp = new HttpResponse();
        req.setHeader('Authorization', 'Bearer '+accessTkn);
        req.setHeader('Content-Type' ,'application/json');
        req.setEndpoint(EPoint);
        req.setBody(selectedOrders);
        req.setMethod('POST');
        httpResp = http.send(req); 
        
        system.debug('resp-------'+httpResp);
        system.debug('resp body-------'+httpResp.getBody());
        
        
        if (httpResp.getStatusCode() == 200 && httpResp.getBody() != '[]' ){
            flag = true;
            system.debug('if-------');
            response = httpResp.getBody(); 
            system.debug('response--RD---'+response);
            List<SoHistoryReportWrapper> soHis = new List<SoHistoryReportWrapper>();
            If(Test.isRunningTest()){
                  StaticResource srObject = [select id,body from StaticResource Where Name = 'SOHistoryRepBody'];
                  String contents = srObject.body.toString();
                  soHis = (List<SoHistoryReportWrapper>)JSON.deserialize(contents, List<SoHistoryReportWrapper>.class);
                system.debug('soHis--in--'+soHis);
            }else{
                soHis = (List<SoHistoryReportWrapper>)JSON.deserialize(response, List<SoHistoryReportWrapper>.class);
                system.debug('soHis---'+soHis.size()+'--'+soHis);
            }
            
            mapinneritemsval = new Map<string,List<inneritemswrap>>();
            inneritemswrap innVal = new inneritemswrap();
            outeritemswrap outVal = new outeritemswrap();
            sonum = new set<string>();
            SetsoOrders = new List<outeritemswrap>();
            
            for(SoHistoryReportWrapper sohr : soHis){
                sonum.add(sohr.SaleOrder);
            }
            system.debug('sonum---'+sonum.size()+'--'+sonum);
            
            map<string,outeritemswrap> mapsoOrders = new map<string,outeritemswrap>();
            map<string,inneritemswrap> mapsoOrderitems = new map<string,inneritemswrap>();
            List<inneritemswrap> innerlist = new List<inneritemswrap>();
            List<Date> Deliverydtlist = new List<Date>();
            Decimal InvoiceCurrentBalance = 0.00;
            
            for(SoHistoryReportWrapper sohr : soHis){
                if(mapsoOrders.get(sohr.SaleOrder) == null){
                    outVal = new outeritemswrap();
                    outVal.sonumber = sohr.SaleOrder;
                    outVal.soinvoice = sohr.SaleOrder;
                    outVal.CustomerId = sohr.CustomerId;
                    if(sohr.CurrentBalance == ''){
                        outVal.CurrentBalance = '0.00';
                    }else{
                        outVal.CurrentBalance = sohr.CurrentBalance;
                    }
                    outVal.SalesComments1 = sohr.SalesComments1;
                    outVal.SalesComments2 = sohr.SalesComments2;
                    outVal.Phone1 = sohr.Phone1;
                    outVal.Phone2 = sohr.Phone2;
                    outVal.Phone3 = sohr.Phone3;
                    outVal.Email = sohr.Email;
                    outVal.CustomerType = sohr.CustomerType;
                    outVal.MostRecentPurchaseDate = (sohr.MostRecentPurchaseDate).substring(0,sohr.MostRecentPurchaseDate.length()-12);
                    //outVal.MostRecentPurchaseDate = outVal.MostRecentPurchaseDate.format('mm-dd-yy');
                    outVal.Address1 = sohr.Address1;
                    outVal.Address2 = sohr.Address2;
                    outVal.City = sohr.City;
                    outVal.State = sohr.State;
                    outVal.Zip = sohr.Zip;
                    outVal.sls_his_slm_1 = sohr.sls_his_slm_1;
                    outVal.sls_his_splt_1 = sohr.sls_his_splt_1.substring(0,sohr.sls_his_splt_1.length()-5);
                    outVal.sls_his_slm_2 = sohr.sls_his_slm_2.trim();
                    outVal.sls_his_splt_2 = sohr.sls_his_splt_2.substring(0,sohr.sls_his_splt_1.length()-5);
                    outVal.sls_his_slm_3 = sohr.sls_his_slm_3;
                    outVal.sls_his_splt_3 = sohr.sls_his_splt_3.substring(0,sohr.sls_his_splt_1.length()-5);
                    outVal.sls_his_slm_4 = sohr.sls_his_slm_4;
                    outVal.sls_his_splt_4 = sohr.sls_his_splt_4.substring(0,sohr.sls_his_splt_1.length()-5);
                    outVal.InvoiceDate = sohr.InvoiceDate;
                    string deliverydt = sohr.DeliveredDate.substring(0,sohr.DeliveredDate.length()-12);
                    outVal.DeliveredDate = Date.parse(deliverydt);
                    
                    Deliverydtlist.add(outVal.DeliveredDate);
                    Deliverydtlist.sort();
                    mapsoOrders.put(sohr.SaleOrder,outVal);
                    SetsoOrders.add(outVal);
                    
                    system.debug('DeliveredDate---'+outVal.DeliveredDate);
                    
                    //system.debug('SetsoOrders--1-'+SetsoOrders.size()+'---'+SetsoOrders);
                } 
                
            }
            
            system.debug('Deliverydtlist size---'+Deliverydtlist.size());
            if(Deliverydtlist.size() > 0){
                system.debug('Deliverydtlist---'+Deliverydtlist);
                String dateFormatString = 'M/dd/YYYY';
                Date d = Deliverydtlist[0];
                Datetime dt = Datetime.newInstance(d.year(), d.month(),d.day());
                fromdate = dt.format(dateFormatString);
                //fromdate = String.ValueOf(Deliverydtlist[0]);
                system.debug('fromdate---'+fromdate);
            }
            
            
            mapItemsQty = new Map<string,integer>();
            totalprice = new Map<string,decimal>();
            extcost = new Map<string,decimal>();
            taxamt = new Map<string,decimal>();
            grosspercent = new Map<string,decimal>();
            invoicetotal = new Map<string,decimal>();
            decimal invoicesum;
            //decimal totalInvoice;
            
            for(string sonumx : sonum){
                innerlist = new List<inneritemswrap>();
                lngQty = 0;
                price = 0;
                extcst = 0;
                tax = 0;
                taxcount = 0;
                gross = 0;
                invoicettl = 0;
                string xinvoicenum = '';
                //totalInvoice = 0.00;
                for(SoHistoryReportWrapper sohr : soHis){
                    if(sohr.SaleOrder == sonumx){
                        innVal = new inneritemswrap();
                        
                        innVal.InvoiceNum = sohr.InvoiceNo;
                        innVal.ItemId = sohr.ItemId;
                        innVal.Quantity = decimal.valueOf(sohr.QuantitySold);
                        innVal.RetailPrice = decimal.valueOf(sohr.RetailPrice);
                        if(decimal.valueOf(sohr.QuantitySold) == -1 ){
                            system.debug('qty--:'+decimal.valueOf(sohr.RetailPrice));
                            
                            innVal.ExtendedPrice = decimal.valueOf(sohr.RetailPrice);
                            if(sohr.ExtendedCost == ''){
                                innVal.ExtendedCost = 0.00;
                            }else{
                                innVal.ExtendedCost = decimal.valueOf(sohr.ExtendedCost);
                            }
                            price = price + innVal.ExtendedPrice; 
                            innVal.grossMargin = 0;
                            
                        }else{
                            system.debug('qty2--:'+decimal.valueOf(sohr.RetailPrice));
                            if(decimal.valueOf(sohr.QuantitySold) == 0){
                                innVal.ExtendedPrice = decimal.valueOf(sohr.RetailPrice);
                            }else{
                                innVal.ExtendedPrice = decimal.valueOf(sohr.RetailPrice) * integer.valueOf(sohr.QuantitySold);
                            }
                            
                            if(sohr.ExtendedCost == ''){
                                innVal.ExtendedCost = 0.00;
                            }else{
                                innVal.ExtendedCost = decimal.valueOf(sohr.ExtendedCost);
                            }
                            price = price + innVal.ExtendedPrice;
                            innVal.grossMargin = innVal.ExtendedPrice - innVal.ExtendedCost;
                            system.debug('price--1-:'+price);
                        }
                        system.debug('price---:'+price);
                        
                        innVal.Decription1 = sohr.Decription1;
                        innVal.Decription2 = sohr.Decription2;
                        
                        if(innVal.grossMargin == 0 || innVal.ExtendedPrice == 0){
                            innVal.grossMargin = 0.00;
                        }else{
                            innVal.grossMargin = (innVal.grossMargin/innVal.ExtendedPrice)*100;
                            innVal.grossMargin = innVal.grossMargin.setScale(2);
                        }
                        system.debug('grossMargin---:'+innVal.grossMargin);
                        innerlist.add(innVal);
                        
                        lngQty = lngQty + integer.valueOf(sohr.QuantitySold);
                        extcst = extcst + innVal.ExtendedCost;
                        system.debug('extcst---:'+extcst);
                        
                        if( xinvoicenum != sohr.InvoiceNo && innVal.Quantity > 0 ){
                            if(sohr.sls_his_tax_amt_1 == '' && sohr.sls_his_tax_amt_2 == '' && sohr.sls_his_tax_amt_3 == ''){
                                tax = tax + 0.00;
                            }else{
                                tax = tax + decimal.valueOf(sohr.sls_his_tax_amt_1) + decimal.valueOf(sohr.sls_his_tax_amt_2) + decimal.valueOf(sohr.sls_his_tax_amt_3);
                            }  
                            xinvoicenum = sohr.InvoiceNo;
                        }
                        
                        /*
                        if(sohr.sls_his_tax_amt_1 == '' && sohr.sls_his_tax_amt_2 == '' && sohr.sls_his_tax_amt_3 == ''){
                            tax = 0.00;
                            taxcount = taxcount +1;
                        }else{
                            tax = decimal.valueOf(sohr.sls_his_tax_amt_1) + decimal.valueOf(sohr.sls_his_tax_amt_2) + decimal.valueOf(sohr.sls_his_tax_amt_3);
                        	taxcount = taxcount + 1;
                        }
						*/
                        system.debug('tax---@'+tax);
                        system.debug('lngQty---'+lngQty);
                        
                    }
                }
                
                    invoicesum = price+tax; 
                    InvoiceCurrentBalance = InvoiceCurrentBalance+invoicesum;
                    InvCurrentBal = InvoiceCurrentBalance;
                    system.debug('invoicesum---'+invoicesum);
                    system.debug('InvoiceCurrentBalance---'+InvoiceCurrentBalance);
                mapinneritemsval.put(sonumx, innerlist);
                
                //-----------calculation values----------
                mapItemsQty.put(sonumx, lngQty);
                totalprice.put(sonumx, price);
                system.debug('totalprice---'+totalprice);
                extcost.put(sonumx, extcst);
                taxamt.put(sonumx, tax);
                if(price != 0 ){
                    decimal totalgrosspercent = (price-extcst)/price;
                    if(totalgrosspercent == 1){
                        totalgrosspercent = totalgrosspercent - 1;
                    }else{
                        totalgrosspercent = totalgrosspercent;
                    }
                    system.debug('totalgrosspercent---:'+totalgrosspercent*100);
                grosspercent.put(sonumx, ((totalgrosspercent)*100).setScale(2));
                }else{
                    grosspercent.put(sonumx,0.00);
                }
                invoicetotal.put(sonumx, price+tax);
                system.debug('mapItemsQty---'+mapItemsQty);
                
            }
            //system.debug('invoicesum--1-'+invoicesum);
                //totalInvoice = totalInvoice+invoicesum;
                //system.debug('totalInvoice---'+totalInvoice);
        }else{
            system.debug('else-------');
            flag = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'There are no records for the selected sales order'));
        }
        System.debug('========== Heap Size :: ' + Limits.getHeapSize());
    }
    
    
    public class inneritemswrap{
        public string InvoiceNum{get;set;}
        public string InvoiceDate{get;set;}
        public string ItemId{get;set;}
        public string ItemSequence{get;set;}
        public string Decription1{get;set;}
        public string Decription2{get;set;}
        public decimal RetailPrice{get;set;}
        public decimal ExtendedPrice{get;set;}
        public decimal Quantity{get;set;}
        public string Quantitycount{get;set;}
        public decimal ExtendedCost{get;set;}
        public decimal grossMargin{get;set;}
    }
    
    public class outeritemswrap{
        public string sonumber{get;set;}
        public string soinvoice{get;set;}
        public string CustomerId{get;set;}
        public string CurrentBalance{get;set;}
        public string SalesComments1{get;set;}
        public string SalesComments2{get;set;}
        public string Phone1{get;set;}
        public string Phone2{get;set;}
        public string Phone3{get;set;}
        public string Email{get;set;}
        public string CustomerType{get;set;}
        public string MostRecentPurchaseDate{get;set;}
        public string Address1{get;set;}
        public string Address2{get;set;}
        public string City{get;set;}
        public string State{get;set;}
        public string Zip{get;set;}
        public string sls_his_slm_1{get;set;}
        public string sls_his_splt_1{get;set;}
        public string sls_his_slm_2{get;set;}
        public string sls_his_splt_2{get;set;}
        public string sls_his_slm_3{get;set;}
        public string sls_his_splt_3{get;set;}
        public string sls_his_slm_4{get;set;}
        public string sls_his_splt_4{get;set;}
        public string InvoiceDate{get;set;}
        public date DeliveredDate{get;set;}
    }
    
}