global class UpdAdrStateBatch implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id, State__c, StatePL__c, StateList__c FROM Address__c WHERE StateList__c = null and Address_Line_1__c != null and (State__c != null OR StatePL__c != null)]); 
    }

    global void execute(Database.BatchableContext bc, List<Address__c> adrList) {
        for(Address__c adr : adrList) {
            if(!string.isBlank(adr.State__c)) {
                if ((adr.State__c != '**') && (adr.State__c != 'Alderta') && (adr.State__c != 'Canada') && (adr.State__c != 'NA') && (adr.State__c != 'N/A') && (adr.State__c != 'Ontario') && (adr.State__c != 'ontaro canada') && (adr.State__c != 'Quebec') && (adr.State__c != 'S') && (adr.State__c != 'Woonsocket') && (adr.State__c != 'WFl')){
                    adr.StateList__c = (adr.State__c).toUpperCase();
                } else {
                    adr.StateList__c = null;
                }
            } else {
                adr.StatePL__c = null;
            }
        }

        Database.SaveResult[] srList = Database.update(adrList, false);
        List<string> errMsgList = new List<string>();
        for(Database.SaveResult sr : srList) {
            if(!sr.isSuccess()) {
                for(Database.Error err : sr.getErrors()) {
                    system.debug(err.getStatusCode() + ': ' + err.getMessage());
                    errMsgList.add(err.getStatusCode() + ': ' + err.getMessage());
                }
            }
        }

        if(errMsgList.size() > 0) {
            //insert in ErrorLog__c object
            ErrorLog__c errLog = new ErrorLog__c(Name = 'State PL Update Batch Error', ApexClass__c = 'UpdAdrStateBatch', Method__c = 'execute', Message__c = string.join(errMsgList,' | '));
            insert errLog;
        }
    }

    global void finish(Database.BatchableContext bc) {}
}