@isTest
public class ImportLanesInOpportunitiesTest {
    //For direct delete
    @isTest static void testGetMethod(){
        //Create Account
        Account delacct = new Account();
       // delacct.recordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CUSTOMER_PERSON_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
        delacct.LastName = 'last' + Math.random();
        delacct.FirstName = 'first' + Math.random();
        delacct.PersonEmail = delacct.FirstName + '.' + delacct.LastName + '@test.com';
        delacct.Primary_Language__pc = 'English';
        string phone = '888' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        delacct.phone = phone;
        Insert delacct;
        //Create Opportunity
        Opportunity op = new Opportunity();
        op.Name = 'TestOp';
        op.CloseDate = Date.today();
        op.AccountId = delacct.Id;
        op.StageName = 'Closed Won';
        Insert op;
        
        ImportLanesInOpportunities.fieldWrapper val = new ImportLanesInOpportunities.fieldWrapper();
        val.of_Trips_Per_Week = '10';
        val.Awarded = 'true';
        val.ADS_Lane_Destination_City = '100 PALMS';
        val.Destination_Drop_Type = 'Drop & Hook';
        val.ADS_Lane_Destination_Market = 'Redlands Mkt';
        val.Destination_Region = 'WEST';
        val.ADS_Lane_Destination_State = 'CA';
        val.ADS_Lane_Destination_Zip = '22225';
        val.Minimum_Charge = '10';
        val.Opportunity = op.Id;
        val.ADS_Lane_Origin_City = '100 PALMS';
        val.Origin_Drop_Type = 'Drop & Hook';
        val.ADS_Lane_Origin_Market = 'Redlands Mkt';
        val.Origin_Region = 'WEST';
        val.ADS_Lane_Origin_State = 'CA';
        val.ADS_Lane_Origin_Zip = '22225';
        String JsonMsg=JSON.serialize(val);
        List<ImportLanesInOpportunities.fieldWrapper> JSONm = new List<ImportLanesInOpportunities.fieldWrapper>();
        JSONm.add(val);
        JsonMsg=JSON.serialize(JSONm);
        Test.startTest();
        String resmsg = ImportLanesInOpportunities.insertData(JsonMsg,op.Id);
        list<ADS_Lane__c> resList = ImportLanesInOpportunities.fetchLane(op.Id);
        System.debug(resmsg);
   		Test.stopTest();
    }
}