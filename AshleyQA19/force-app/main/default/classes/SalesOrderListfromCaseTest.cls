@isTest
private class SalesOrderListfromCaseTest {

    @testSetup
    static void setup() {
        List<Account> accList = new List<Account>();
        accList = TestDataFactory.initializeAccounts(1);
        insert accList;

        List<Contact> contList = new List<Contact>();
        contList = TestDataFactory.initializeContacts(accList[0].Id, 1);
        insert contList;

        // Insert Case
        Case caseObj = new Case(Sales_Order__c = '17331400:001q000000raDkvAAE', Status = 'New', Origin = 'Phone',
                            Type = 'Open Order Inquiry', Subject = 'Test',Type_of_Resolution__c = 'yes',
                            Description = 'Test', AccountId = accList[0].Id, ContactId = contList[0].Id);
        insert caseObj;
        system.debug(caseObj);
    }

    @isTest static void getCaseinfoTest() {
        Case caseObj = [Select Id from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];
        Test.startTest();
        SalesOrderListfromCase.getCaseinfo(caseObj.Id);
        Test.stopTest();
    }
    
    @isTest static void getSalesOrdersTest() {
        Case caseObj = [Select Id,Type_of_Resolution__c from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];
        system.debug(caseObj);
        Test.startTest();
        SalesOrderListfromCase.getSalesOrders(caseObj.Id);
        Test.stopTest();
    }

    @isTest static void updateSalesOrderRecordTest() {
        Case caseObj = [Select Id from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];
        Test.startTest();
        SalesOrderListfromCase.updateSalesOrderRecord(caseObj.Id, '17331400:001q000000raDkvAAE', true, null,'8888300-164');
        Test.stopTest();
    }
}