public class Case_AddressDetailsController {
    @AuraEnabled
    public static Case getDetails(Id objectID){
        
        if(objectID!=null){
            List<Case> testAddressRecords = [SELECT ID, Address_Line_1__c,Address_Line_2__c,Address__c,City__c,State__c,ZIP__c,Address__r.Name  
                                                   FROM Case WHERE Id =:objectID LIMIT 1];
            system.debug('here--->' + testAddressRecords);
            if(testAddressRecords.size()!=0){
                return testAddressRecords[0];   
            }   
            //END OF CHANGED FIELD NAME          	
        }
        return null;
    }
    @AuraEnabled
    public static Contact getContactDetails(Id objectID){
        
        if(objectID!=null){
            Case casedet = [SELECT ID, AccountId FROM Case WHERE Id =:objectID];
            List<Contact> testContactRecords = [SELECT ID, AccountId,Email,Name,Phone,Phone_2__c,Phone_3__c,Title 
                                                   FROM Contact WHERE AccountId =:casedet.AccountId LIMIT 1];
            system.debug('here contact--->' + testContactRecords);
       /*     if(testContactRecords[0].Phone == '' || testContactRecords[0].Phone == null){
                if(testContactRecords[0].Phone_2__c == '' || testContactRecords[0].Phone_2__c == null){
                    testContactRecords[0].Phone = testContactRecords[0].Phone_3__c;
                }
                else{
                    testContactRecords[0].Phone = testContactRecords[0].Phone_2__c;
                }
            } */
            if(testContactRecords.size()!=0){
                return testContactRecords[0];   
            }   
            //END OF CHANGED FIELD NAME          	
        }
        return null;
    }
}