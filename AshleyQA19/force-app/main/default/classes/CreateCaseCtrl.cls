public class CreateCaseCtrl {
    
    @AuraEnabled
    public static SalesOrder__x getSalesOrderInfo(Id recordId){
        SalesOrder__x orderObj = new SalesOrder__x();
        if(Test.isRunningTest()){
            orderObj = new SalesOrder__x(id ='x010n000000CuZuAAK',
                                         ExternalId = '17331400:001q000000raDkvAAE',
                                         phhHot__c = true,
                                         phhOrder_Notes__c = 'Test Type',
                                         phhDesiredDate__c = system.today(),
                                         fulfillerID__c = '1-',
                                         phhStoreID__c = '23-12345',
                                         phhDeliveryType__c = 'TW',
                                         phhProfitcenter__c  = 23,
                                         phhSaleType__c = 'POS',
                                         phhSalesOrder__c = '200460320',
                                         phhSOSource__c = 'POS9',
                                         phhCustomerName__c = 'DAVID HESSE',
                                         phhStoreLocation__c = 'TAMPA',
                                         phhReasonCode__c = 'Invalid delivery date',
                                         phhGuestID__c = '001e000001FinfrAAB',
                                         phhDatePromised__c = system.today(),
                                         phhWindowBegin__c = '07:00',
                                         phhWindowEnd__c = '19:00',
                                         phhASAP__c = false,
                                         phhServiceLevel__c = 'THD',
                                         VIPFlag__c = true,
                                         phhBackOrder__c = 'test',
                                         phhBalanceDue__c = 100,
                                         phhContactStatus__c = 'QA',
                                         phhErpNumber__c = '8886000',
                                         Estimated_Arrival__c = '09-09-2019',
                                         phhHighDollarSale__c = false,
                                         IsMarketActive__c = true,
                                         phhDeliveryAttempts__c = 1, 
                                         phhOrderSubType__c = 'Home',
                                         LIBFlag__c = true);
        } else {
            orderObj = [SELECT ExternalId, fulfillerID__c, phhERPAccounShipTo__c, phhERPCustomerID__c, Id, phhProfitcenter__c, phhMarketAccount__c, phhStoreNameStoreNumberPC__c,
                        phhCustomerID__c, phhSalesOrder__c, phhSaleType__c, phhCustomerType__c, phhShipToName__c, phhGuestID__c,Text_consent__c
                        FROM SalesOrder__x WHERE Id =: recordId];
        }
        return orderObj;
    }
    
    //Added to map market account for Ashcomm orders
    @AuraEnabled
    public static string getMarketConfig(string legacy){
        list<Market_Configuration__mdt> marketConfigList = [SELECT MasterLabel FROM Market_Configuration__mdt where Fulfiller_Id__c =:legacy];
        system.debug('market--'+marketConfigList[0].MasterLabel);
        return marketConfigList[0].MasterLabel;
    }
    
    @AuraEnabled
    public static Account getAccDetail (Id accId) {
        Account Acc = new Account();
        List<Account> AccList = [SELECT Id,Name FROM Account WHERE Id = :accId LIMIT 1];
        if (AccList.size() > 0) {
            Acc = AccList[0];
        }
        return Acc;
    }
    
    @TestVisible private static list<SalesOrderItem__x> mockedcustlist1 = new List<SalesOrderItem__x>();
    @AuraEnabled
    //Get Product line item by sales order external ID
    public static List<SalesOrderItem__x> getOrderLineItemsByOrderExternalId(string extId) {
        if(Test.isRunningTest()){
            List<SalesOrderItem__x> lineItem2 = new List<SalesOrderItem__x>();
            lineItem2.add(new SalesOrderItem__x(Id = 'x00q00000000DMoAAM',
                                                ExternalId = '21111310:001q000000raI3DAAU',
                                                phdItemSKU__c = 'B502-87',
                                                phdItemDesc__c = 'Full Panel Headboard / Kaslyn / White',
                                                phdItemDesc2__c = 'Full Panel Headboard / Kaslyn / White',
                                                phdQuantity__c = 1,
                                                phdSalesOrder__c = '21111310',
                                                phdIsFPP__c = false,
                                                phdShipAddress1__c = '250 AMAL DR',
                                                phdShipAddress2__c = 'apt 102',
                                                phdShipCity__c = 'ATLANTA',
                                                phdShipState__c = 'GA',
                                                phdShipZip__c = '30315',
                                                phdInStoreQty__c = 0,
                                                phdInWarehouseQty__c = 0,
                                                phdWarrantyDaysLeft__c = 365,
                                                phdDeliveryType__c = 'G02',
                                                phdDeliveryTypeDesc__c = 'GA TRUCK 02',
                                                phdSaleType__c = 'POS',
                                                phdItemSeq__c = 10,
                                                phdOrderType__c = 'ORD(Exch)',
                                                phdPaymentType__c = 'Payment Test',
                                                phdRSA__c = 'HOU',
                                                phdAmount__c = 199.99,
                                                Ship_Customer_Name__c = 'HESSE',
                                                phdWholeSalePrice__c = 99.01,
                                                phdVendorName__c='vendor'));
            return lineItem2;
        }
        else{
            return [SELECT Id, ExternalId, phdItemSKU__c, phdItemDesc__c, phdItemDesc2__c, phdQuantity__c, phdReturnedReason__c, 
                    phdDeliveryDueDate__c, phdSalesOrder__c, phdIsFPP__c, phdItemStatus__c, phdWarrantyExpiredOn__c, 
                    phdShipAddress1__c, phdShipAddress2__c, phdShipCity__c, phdShipState__c, phdShipZip__c,
                    phdLOC_PO__c, phdInStoreQty__c, phdInWarehouseQty__c, phdPurchaseDate__c, phdSaleType__c, phdWarrantyDaysLeft__c, phdDeliveryType__c
                    FROM SalesOrderItem__x 
                    WHERE phdSalesOrder__r.ExternalId =: extId];
        }
    }
    
    @AuraEnabled
    public static List<SalesOrderItem__x> getSoLineItemsBySoExternalId(string extId) {
        List<SalesOrderItem__x> returnlineItemList = new List<SalesOrderItem__x>();
        List<SalesOrderItem__x> lineItemList = getOrderLineItemsByOrderExternalId(extId);
        system.debug('****** extId: ' + extId + ' :lineItemList: ' + lineItemList);
        if(lineItemList.size() > 0){
            for(SalesOrderItem__x lineItem : lineItemList){
                if((lineItem.phdQuantity__c >= 0) && (!(lineItem.phdSaleType__c).equalsIgnoreCase('Canceled'))){
                    returnlineItemList.add(lineItem);
                }
            }
        }
        return returnlineItemList;
    }
    
    @AuraEnabled
    public static string create_case(case caseis, String salesorder, boolean NPS, string resolved,Id accountId, String casetype, String casesubtype, string subject, string marketAccount, string serviceProvider,string storeNameStoreNumberPC, string shipAddress1, string shipAddress2, string shipCity, string shipState, string shipZip, String preferred , string caseorigin, string caserefusal, string casestatus, string accShipto) {
        string newCaseId;
        system.debug('*** case: '+ caseis);
        system.debug('*** salesorder: '+ salesorder);
        
        List<Contact> contactList = [SELECT Id, AccountId FROM Contact WHERE AccountId =:accountId];
        system.debug('*** contactList: '+ contactList);
        List<Account> accountList = [SELECT Id, PersonEmail, Phone FROM Account WHERE Id =:accountId];
        system.debug('*** accountList: '+ accountList);
        
        //caseis.Address__r = null;
        //if (string.isBlank(caseis.Address__c)) {
        //if the address is blank then select an address for the customer
        id adrId;
        //adrId = caseis.Address__r.Id;
        system.debug('*** preferred: '+ preferred);
        //if(adrId == null) {
        //if the customer address is null then create sales order line item ship to address for the customer and set that address to the case.
        Map<string, id> addressMap = new Map<string, id>();
        addressMap = AddressHelper.getCustomAddress(accountId);
        adrId = AddressHelper.getAddressId(shipAddress1, shipAddress2, shipCity, shipState, shipZip, accountId, addressMap, 'CreateCaseCtrl.create_case');
        system.debug('*** adrId: '+ adrId);
        
        List<Address__c> upadd = [SELECT Id, AccountId__c, Address_Line_1__c, Address_Line_2__c, City__c, StateList__c, Zip_Code__c, Country__c, Address_Type__c,Preferred__c 
                                  FROM Address__c WHERE  Id =:adrId];
        if(upadd.size() > 0) {
            if(preferred == 'Yes'){
                upadd[0].Preferred__c = true;
                update upadd;
                AddressHelper.setCustomerPreferredAddress(accountId,adrId);
            }
            adrId = upadd[0].Id;
        }
        //AddressHelper.setCustomerPreferredAddress(accountId,adrId);
        if (adrId != null) {
            caseis.Address__c = adrId;
        }
        //}
        
        if (contactList.size() > 0) {
            caseis.ContactId = contactList[0].Id;
        } else {
            caseis.ContactId = null;
        }
        
        if (string.isNotBlank(caserefusal)) {
            caseis.Refusal_Reason__c = caserefusal;
        }
        if (string.isNotBlank(casestatus)) {
            caseis.Status = casestatus;
        }
        if (string.isNotBlank(serviceProvider)) {
            caseis.Home_Delivery_Service_Providers__c = serviceProvider;
        }
        
        if (string.isNotBlank(caseorigin)) {
            caseis.Origin = caseorigin;
        } else {
            caseis.Origin = 'Phone';
        }
        if (accountList.size() > 0) {
            caseis.Case_Email__c = accountList[0].PersonEmail;
            caseis.Case_Phone_Number__c = accountList[0].Phone;
        }
        caseis.Survey_Opt_In__c = NPS;
        caseis.Sales_Order__c = salesorder;
        caseis.Type = casetype;
        caseis.Sub_Type__c = casesubtype;
        caseis.Market__c = marketAccount;
        caseis.Profit_Center__c = storeNameStoreNumberPC;
        caseis.Legacy_Account_Ship_To__c = accShipto;
        system.debug('caseis insert values-->'+ caseis);
        if ((marketAccount == 'ASHCOMM') && (casetype == 'Post Delivery') && (casesubtype == 'Changed Mind') && (resolved == 'Yes')){
            throw new AuraHandledException('Ashcomm orders set to Type: Post Delivery and Sub-Type: Changed Mind cannot be resolved immediately. "Resolved?" must be set to "No" ');
        }
        
        try {
            Database.SaveResult sr = Database.insert(caseis);
            if (sr.isSuccess()) {
                newCaseId = sr.getId();
            } else {
                CaseHelper.databaseErrorLog(sr, 'Case insert try Error', 'CreateCaseCtrl', 'create_case', JSON.serialize(caseis));
            }
        } catch (DmlException de) {
            CaseHelper.dmlExceptionError(de, 'Case insert DML catch Error', 'CreateCaseCtrl', 'create_case', JSON.serialize(caseis));
        }
        
        return newCaseId;
    }
    
    //Creating new line item and a respective case
    @AuraEnabled
    public static void newProductLineItemRecord(List<String> ProductId, Id caseId, String salesorder){
        system.debug('my new Case Id' + caseId);
        createLineItem CLI = new createLineItem(caseId);
        createLineItem.newProductLineItemRecordwithCaseId(ProductId,caseId,salesorder);
    }
    // Get the StateList
    @AuraEnabled
    public static List <string> getStatePl() {
        List<string> statePlList = new List<string>();
        Schema.DescribeFieldResult fieldResult = Address__c.StatePL__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        system.debug('ple--'+ple);
        for (Schema.PicklistEntry f: ple) {
            if(f.isActive()){
                statePlList.add(f.getValue());
            }
        }
        system.debug('statePlList--'+statePlList);
        return statePlList;
        
    }
    
    // EDQ added
    @AuraEnabled
    public static String SearchAddress(string searchTerm, string country, Integer take) {
        return EDQService.SearchAddress(searchTerm, country, take);
    }
    
    // EDQ added
    @AuraEnabled
    public static String FormatAddress(string formatUrl) {
        return EDQService.FormatAddress(formatUrl);
    }
    
    public class AddressValidationWrap {
        @AuraEnabled
        public string vMessage { get; set; }
        @AuraEnabled
        public List<case> vCaseList { get; set; }
        
        public AddressValidationWrap(string validMessage, List<case> csList) {
            this.vMessage = validMessage;
            this.vCaseList = csList;
        }
    }  
}