@isTest
public class salesOrderRouting_Test{
    static testMethod void testsoRoutingData() {
        SalesOrder__x salesOrder = new SalesOrder__x(fulfillerID__c = '8888300-164',
                                                     ExternalId = '17331400:001q000000raDkvAAE',  
                                                     phhProfitcenter__c = 1234567,
                                                     Phhcustomerid__c = '784584585',
                                                     phhSalesOrder__c = '88845758',
                                                     phhStoreID__c = '133'
                                                    );
        Account acct = new Account(Id = '001q000000raI3DAAU');
        system.debug('order------' + salesOrder);
        SalesOrderDAO.mockedSalesOrders.add(salesOrder);
        system.debug('order--2----' + salesOrder.Id);
        SalesOrder__x salesOrderObj = SalesOrderDAO.getOrderById(salesOrder.Id);
        SalesOrderItem__x salesOrderItem = new SalesOrderItem__x(ExternalId = '17331400:001q000000raDkvAAE', phdShipZip__c = '30548');
        SalesOrderDAO.mockedSalesOrderLineItems.add(salesOrderItem);
        SalesOrderItem__x salesOrderItemObj = SalesOrderDAO.getOrderLineItemByExternalId(salesOrderItem.ExternalId);
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
        mock.setStaticResource('soRoutingDatamockResponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        
        Test.startTest();
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        SORoutingWrapper.value  soValue = new SORoutingWrapper.value();
        salesOrderRouting.soRoutingData(salesOrder.ExternalId );
        salesOrderRouting.soRoutingHistoryData(salesOrder.ExternalId );
        salesOrderRouting.soLineItemsData(salesOrder.ExternalId,23 );
        salesOrderRouting.soLineItemHistoryData(salesOrder.ExternalId,23);
        Salesorderstatusdetail.sodetailData(salesOrder.ExternalId);
        AccountDeliveryConfirmation.accDelCnf(acct.Id);
        AccountDeliveryConfirmation.soConfirmData('8888300-164','200462350',23,'G03','2017-05-26 00:00:00.000',1,'2017-05-26 08:45:00.000','2017-05-26 12:45:00.000');
        String jSRout = '[{"StoreID": 133,"SalesOrderNumber": "200442750","TransportationOrderID": "01330000862365","IsConfirmed": false,"BegunTime": "09:55","CompletedTime": "10:18","UserName": "ASHLEY","RoutingPass": 1,"TimeChanged": 0,"ProfitCenter": 23,"DeliverDate": "2018-06-26","CustomerWindowOpen": "09:45","CustomerWindowClose": "13:45","TruckID": "G01","SFPersonAccountID": "001q000000raIJLAA2","UniqueID": "17331388","ConfirmationDateTime": null,"AccountShipTo": "8888300-164","SOLineItems": [{"SKU": "*090949C","ItemDescription": "Must Take Delivery Within 90 Days.","Quantity": 0},{"SKU": "*ANNIVERSARY","ItemDescription": "1 year anniversary","Quantity": 0},{"SKU": "*AS IS","ItemDescription": "No Refunds, No Svc, No Exchanges","Quantity": 0},{"SKU": "*DELIV-TAX","ItemDescription": "Delivery Fees","Quantity": 0},{"SKU": "B213-21","ItemDescription": "Dresser","Quantity": 1}]}]';
        
        salesOrderRouting.soConfirmData(jSRout,1);
        salesOrderRouting.convertDatetimeToLocal(System.now());
        AccountDeliveryConfirmation.getSalesOrderLineItem(acct.Id,'200462350');
        
        string soroutingstr = '{'
            +'"@odata.context": "https://test.cara.ashleyretail.com/odata/$metadata#DTRoutingHistory",'
            +'"value": ['
            +'{'
            +'"SFPersonAcccountID": null,'
            +'"StoreID": 133,'
            +'"SalesOrderNumber": "200460210",'
            +'"TransportationOrderID": "01330000865578",'
            +'"IsConfirmed": true,'
            +'"BegunTime": "2019-01-26T09:12:00Z",'
            +'"CompletedTime": "2019-01-26T09:35:00Z",'
            +'"UserName": "ASHLEY",'
            +'"RoutingPass": 1,'
            +'"TimeChanged": 0,'
            +'"ProfitCenter": 23,'
            +'"DeliverDate": "2019-01-26",'
            +'"CustomerWindowOpen": "09:00 AM",'
            +'"CustomerWindowClose": "01:00 PM",'
            +'"TruckID": "G01",'
            +'"ConfirmationDateTime": "2018-12-18 04:48:55 AM",'
            +'"AccountShipTo": "8888300-164",'
            +'"DriverName": "GXPO - Ohemeng Mensah-Bonsu (Prestige transportation): 770.864.7625",'
            +'"DeliveryTime": "2017-05-18 18:08:43 -0400",'
            +'"SOLineItems": ['
            +'{'
            +'"SKU": "B213-21",'
            +'"ItemDescription": "ASHLEY B213-21",'
            +'"Quantity": 1,'
            +'"RoutingPass": 1'
            +'},'
            +'{'
            +'"SKU": "*090909A",'
            +'"ItemDescription": "Reserves Price Only.",'
            +'"Quantity": 1,'
            +'"RoutingPass": 1'
            +'}'
            +']'
            +'}'
            +']'
            +'}';
        
        SORoutingWrapper soRouting = new SORoutingWrapper();
        soRouting = (SORoutingWrapper) JSON.deserialize(soroutingstr, SORoutingWrapper.class);
        
        Test.stopTest(); 
    }
    static testMethod void testsoRoutingData1() {
        
        
        SalesOrder__x salesOrder = new SalesOrder__x(fulfillerID__c = '8888300-164',
                                                     ExternalId = '17331400:001q000000raDkvAAE',  
                                                     phhProfitcenter__c = 1234567,
                                                     Phhcustomerid__c = '784584585',
                                                     phhSalesOrder__c = '88845758',
                                                     phhStoreID__c = '133'
                                                    );
        Account acct = new Account(Id = '001q000000raI3DAAU');
        system.debug('order------' + salesOrder);
        SalesOrderDAO.mockedSalesOrders.add(salesOrder);
        system.debug('order--2----' + salesOrder.Id);
        SalesOrder__x salesOrderObj = SalesOrderDAO.getOrderById(salesOrder.Id);
        SalesOrderItem__x salesOrderItem = new SalesOrderItem__x(ExternalId = '17331400:001q000000raDkvAAE', 
                                                                 phdShipZip__c = '30548');
        SalesOrderDAO.mockedSalesOrderLineItems.add(salesOrderItem);
        SalesOrderItem__x salesOrderItemObj = SalesOrderDAO.getOrderLineItemByExternalId(salesOrderItem.ExternalId);
        
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
        mock.setStaticResource('soRoutingDatamockResponse');
        
        mock.setStatusCode(400);
        
        mock.setHeader('Content-Type', 'application/json');
        
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponse(404));
        
        SORoutingWrapper.value  soValue = new SORoutingWrapper.value();
        
        
        AccountDeliveryConfirmation.soConfirmData('8888300-164','200462350',23,'G03','2017-05-26 00:00:00.000',1,'2017-05-26 08:45:00.000','2017-05-26 12:45:00.000');
        
        Test.stopTest(); 
    }
}