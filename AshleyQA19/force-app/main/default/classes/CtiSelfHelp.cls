/*******************************************************************************************
* Description – Apex REST service with GET method
* Author – Sudeshna Saha

* CTI SELF HELp
********************************************************************************************/
@RestResource(urlMapping='/v1/CTI-SELFHELP/*')
global class CtiSelfHelp {
    @HttpGet
    global static CaseWrapper doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        CaseWrapper response = new CaseWrapper();
        System.debug('someval-->'+RestContext.request.requestUri);
        String someval = RestContext.request.requestUri.substringAfterLast('/');
        System.debug('someval-->'+someval);
        List<Account> myAcc = new List<Account>();
        List<getCaseDetail> myCase = new List<getCaseDetail>();
        ///services/apexrest/v1/CTI-SELFHELP/phone=8889997771
        someval = getVal(someval);
        System.debug('someval phone-->'+someval);
        if(someval.length() < 10){
            response.status = 'ERROR';
            response.message = 'Please remove spaces on Phone Number';
            res.statusCode = 200;
            System.debug('casessss--->'+myCase.size());
            return response;
        }
        Set<String> phoneNumSet = new Set<String>();
        phoneNumSet.add(someval);
        List<Account> accountList = new List<Account>();
        List<Case> caseList = new List<Case>();
        Boolean finaltechFlag = false;
        Boolean finalCaseFlag = false;
        If(someval !=null || someval != ''){
            accountList = searchAccounts(phoneNumSet);
            caseList = getCaseList(accountList);
            myCase = getCase(caseList);
        }
        System.debug('case--->'+myCase);
        System.debug('case size--->'+myCase.size());
        if(myCase.size()>0){
            System.debug('myCase--->'+myCase);
            for(integer i=0; i<myCase.size(); i++){
                System.debug('myCase--->'+myCase[i].techDate);
                if(myCase[i].techDate != 'Flag true' && myCase[i].techDate != null && myCase[i].techDate != ' '){
                    System.debug('finaltechFlag1--->'+finaltechFlag);
                    finaltechFlag = true;
                    System.debug('finaltechFlag11--->'+finaltechFlag);
                }
            }
            System.debug('finaltechFlag2--->'+finaltechFlag);
            if(finaltechFlag == false){
                response.status = 'Success';
                response.message = 'No Technician Available for future days';
                res.statusCode = 200;
            }
            else{
                response.paramList = myCase ;
                response.status = 'Success';
                response.message = 'Successful';
                res.statusCode = 200;
            }
        }
        else{
            response.status = 'Success';
            response.message = 'No Cases Found';
            res.statusCode = 200;
            System.debug('casessss--->'+myCase.size());
        }
        return response;
    }
    
    //If the request came to /v1/accounts, then we want to execute a search
    private static String getVal(String someval) {
        String myphone;
        String temp;
        String cphone;
        system.debug('myphoneval111..' + someval);
        myphone = someval.substringAfter('=');
        if (myphone != null)   { 
            cphone= myphone.replaceAll( '%20', '-');
            cphone = cphone.replaceAll('\\D','');
        }  
        system.debug('myphoneval..' + cphone);
        if(cphone.length() < 10){
            cphone = '0';
        }
        return cphone;
    }
    private static List<Case> getCaseList(List<Account> accountList) {
        System.debug('accountList-->'+accountList);
        List<Case> caseList = new List<Case>();
        if(accountList.size() > 0){
            caseList = [SELECT AccountId,Id,CaseNumber,Case_Phone_Number__c,TechnicianNameScheduled__c,
                        Tech_Scheduled_Date__c,Technician_Schedule_Date__c,Technician_Name__c	 
                        FROM Case WHERE AccountId IN :accountList];
        }
        System.debug('caseList-->'+caseList);
        return caseList; 
    }
    private static List<getCaseDetail> getCase(List<Case> caseList) {
        List<getCaseDetail> mycase = new List<getCaseDetail>();
        System.debug('getCase-->'+caseList);
        if(caseList.size() > 0)
            mycase = searchCase(caseList);
        return mycase; 
    }
    private static List<getCaseDetail> searchCase(List<Case> caseList) {
        List<getCaseDetail> mycase = new List<getCaseDetail>();
        getCaseDetail C3 = new getCaseDetail();
        Boolean flag = false;
        Boolean flagCase = false;
        System.debug('searchCase-->'+caseList);
        if(caseList.size() > 0)
        {
            for(Case c1:caseList){
                if(c1.Tech_Scheduled_Date__c != null || c1.Technician_Schedule_Date__c != null){
                    Date techdate;
                    if(c1.Technician_Schedule_Date__c != null){
                        techdate = c1.Technician_Schedule_Date__c;
                    }
                    else{
                        techdate = c1.Tech_Scheduled_Date__c;
                    }
                    Date todaysDate = date.today();
                    System.debug('techdate-->'+techdate);
                    System.debug('todaysDate-->'+todaysDate);
                    System.debug('URL-->'+System.URL.getSalesforceBaseUrl());
                    if((techdate > todaysDate) || (techdate == todaysDate)){
                        getCaseDetail C2 = new getCaseDetail();
                    	C2.techDate = String.valueOf(techdate);
                        C2.caseLink = System.URL.getSalesforceBaseUrl()+'/lightning/r/Case/'+ c1.Id + '/view';
                        mycase.add(C2);
                        flag = false;
                        System.debug('C2-->'+C2);
                        System.debug('mycase-->'+mycase);
                    }
                    else if(!(mycase.size()>0)){
                        System.debug('flag-->'+flag);
                        flag = true;
                    }
                }
            }
            
        }
        if(flag){
            C3.techDate = 'Flag true';
            C3.caseLink = '';
            mycase.add(C3);
        }
        System.debug('getCase-->'+mycase);
        return mycase;
    }
    private static List<Account> searchAccounts(Set<String> myphone) {
        // string myphone = someval;
        System.debug('myVal--->'+myphone);        
        List<Account> myaccount = new List<Account>();
        try{
            myaccount = [SELECT AccountNumber,Id,PersonOtherPhone,Phone,Phone_2__pc,Phone_3__pc 
                         FROM Account WHERE Phone IN :myphone OR Phone_2__pc IN :myphone OR Phone_3__pc IN :myphone LIMIT 5000
                        ];
        }
        catch(Exception e){
            
        }
        return myaccount; 
    }
    global class CaseWrapper {
        public getCaseDetail[] paramList;
        public String status;
        public String message;
        public CaseWrapper(){
            paramList = new List<getCaseDetail>();
        }
    }
    global class getCaseDetail {
        public String techDate;
        public String caseLink;
    }
}