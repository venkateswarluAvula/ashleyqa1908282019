public class DispatchTrackOnCaseCtrl {
    @AuraEnabled
    public static String formDispatchTrackUrl(String recId) {
        // Get Dispatch Track Url from custom setting
        String dispatchTrackUrl;
        Integration_Settings__c integSetting = Integration_Settings__c.getInstance('Dispatch_TrackService');
        if(integSetting != null) {
            dispatchTrackUrl = integSetting.End_Point_URL__c;      
        }
        List<Case> caseList = new List<Case>();
        caseList = [SELECT Id, CaseNumber,Legacy_Account_Ship_To__c,Profit_Center__c,Technician_Id__c,Technician_ServiceReqId__c,Legacy_Service_Request_ID__c,
                    Sales_Order__r.phhProfitcenter__c,Sales_Order__r.phhStoreID__c
                    FROM Case WHERE Id =:recId];
        System.debug('value-->'+recId);
        String str = caseList[0].Profit_Center__c;
        String str2 = str.substringAfter('- ');
        String RequestId;
        Decimal ProfitCenterCode = caseList[0].Sales_Order__r.phhProfitcenter__c;//str2.substringAfter('- ');
        String StoreId = caseList[0].Sales_Order__r.phhStoreID__c ;//str2.substringBefore(' -');
        String errReqId = 'RequestId not available.';
        AuraHandledException ex = new AuraHandledException(errReqId);
        ex.setMessage(errReqId);
        if(caseList[0].Technician_ServiceReqId__c !=null && caseList[0].Technician_ServiceReqId__c != ''){
            RequestId = caseList[0].Technician_ServiceReqId__c;
        }else if((caseList[0].Legacy_Service_Request_ID__c !=null && caseList[0].Legacy_Service_Request_ID__c !='')){
            RequestId = caseList[0].Legacy_Service_Request_ID__c;
        }else{
            throw ex;
        }
        dispatchTrackUrl += '/'+ StoreId + 'S-' + ProfitCenterCode +'-' + RequestId;
        system.debug('dispatch--->' + dispatchTrackUrl);
        return dispatchTrackUrl;
    }
}