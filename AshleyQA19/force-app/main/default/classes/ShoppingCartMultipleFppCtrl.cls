public class ShoppingCartMultipleFppCtrl {

     @AuraEnabled
    public static String multipleFppUpdateCart(String fppsku,String[] lineItemIdList){
        try{
            List<Shopping_cart_line_item__c> UpdateItems = new  List<Shopping_cart_line_item__c>();
             List<Shopping_cart_line_item__c> lineItems = [Select Id,WarrantySku__c from Shopping_cart_line_item__c where Id IN :lineItemIdList];
              for(Shopping_cart_line_item__c itemId:lineItems){
                itemId.WarrantySku__c = fppsku;
                UpdateItems.add(itemId);
                }
               
               system.debug('UpdateItems---'+UpdateItems);
               update UpdateItems;
        }
        catch (Exception ex){
            System.debug(LoggingLevel.ERROR, 'Failed to Delete Items: '+ ex.getMessage());
            ErrorLogController.createLogFuture('ShoppingCartMultipleDeleteCtrl', 'multipleDleteFromCart', 'Failed to Delete Items: ' + ex.getMessage() +  ' Stack Trace: ' + ex.getStackTraceString() );         
            throw new AuraHandledException(ex.getMessage());
        } 
        return 'Success';
    }
}