public class ProductLineItemAutoRefresh {
    public ProductLineItem__c PLI = new ProductLineItem__c();
    @AuraEnabled
   // @future(callout=true)
    public static ProductLineItem__c getTrackingNumber(ProductLineItem__c record, String replParts){
        System.debug('My record ID' +record);
        ProductLineItem__c PLIupdate = new ProductLineItem__c();
        String strTest = record.Fulfiller_ID__c;
        System.debug(strTest);
        List<String> arrTest = strTest.split('\\-');
        String PONumber = record.Ashley_Direct_Link_ID__c;
        String customerNumber = arrTest[0];
        String shipTo = arrTest[1];
        System.debug(customerNumber);
        System.debug(shipTo);
        if((strTest != null || strTest != '') && (PONumber != null || PONumber != '' )){
            String accessTkn = replParts;
            system.debug('accesstokenresponse' + accessTkn);
            HttpRequest req = new HttpRequest();
            req.setEndpoint(System.label.ReplacementPartSerialNumber+'/orders/status?ponumber='+PONumber+'&customerNumber='+customerNumber+'&shipTo='+shipTo+'&apikey=NvcXrflst6O2sQurPOzOxytA1bUVvW8t');
            //https://stageapigw.ashleyfurniture.com/replacement-parts/v1/orders/status?ponumber=2622632&customerNumber=700&shipTo=01&apikey=NvcXrflst6O2sQurPOzOxytA1bUVvW8t
            req.setMethod('GET');
            req.setHeader('Content-Type' ,'application/json;charset=utf-8');
            req.setHeader('Authorization', 'Bearer '+accessTkn);
            //req.setHeader('apikey', System.label.TechSchedulingApiKey);
            system.debug('req--->' + req);
            string errMsg;
            Http http = new Http();
    		HttpResponse res = http.send(req);
            // Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
            System.debug('***Tracking Number Response Status: '+ res.getStatus());
            System.debug('***Tracking Number Response Body: '+ res.getBody());
            System.debug('***Tracking Number Response Status Code: '+ res.getStatusCode());
            Integer statusCode = res.getStatusCode();
            //Check the response
            system.debug('body ' + res.getBody()); 
            //POJSON.ExtDetail data = (POJSON.ExtDetail)JSON.deserialize(res.getBody(), POJSON.ExtDetail.class); 
            Type wrapperType = Type.forName('POJSON'); 
            POJSON data = (POJSON)JSON.deserialize(res.getBody(),wrapperType);
            //System.debug('SO--->'+data.Detail.Street);
            if(statusCode == 200){
                System.debug('Success');
                PLIupdate = updateTrackingNumber(record.Id,data);
            }
            else if(statusCode == 401){
                System.debug('Unauthorised');
                ServiceReplacementPartsAuthorization replParts2= new ServiceReplacementPartsAuthorization();
                PLIupdate = ProductLineItemAutoRefresh.getTrackingNumber(record,replParts2.accessTokenData());
            }
            else{
                PLIupdate = null;
                System.debug('FAILED');
            }
        }
        return PLIupdate;
    }
    
    @AuraEnabled
    public static ProductLineItem__c updateTrackingNumber(ID PLIID, POJSON data){ 
        system.debug('PLIID'+PLIID);
        system.debug('data-->'+data);
        ProductLineItem__c PLITrackingNumUpdate = new ProductLineItem__c();
        try{
            PLITrackingNumUpdate.ID = PLIID;
            if(data.ExtDetail.UPSTracking!='' && data.ExtDetail.UPSTracking != null){
                PLITrackingNumUpdate.Part_Order_Tracking_Number__c = data.ExtDetail.UPSTracking;
            }
            if(data.ExtDetail.DefectLocation!='' && data.ExtDetail.DefectLocation != null){
                PLITrackingNumUpdate.Defect_Location__c = data.ExtDetail.DefectLocation;
            }
            if(data.ExtDetail.DefectCode!='' && data.ExtDetail.DefectCode != null){
                PLITrackingNumUpdate.Item_Defect__c = data.ExtDetail.DefectCode;
            }
            if(data.ExtDetail.RPKey!='' && data.ExtDetail.RPKey != null){
                PLITrackingNumUpdate.Part_Order_Number__c = data.ExtDetail.RPKey;
            }
            if(data.ExtDetail.Model!='' && data.ExtDetail.Model != null){
                PLITrackingNumUpdate.Replacement_Part_Item_Number__c = data.ExtDetail.Model;
            }
            
            system.debug('ShipDate-->'+data.ExtDetail.UPSTracking);
            if(data.ExtDetail.ShipDate!='' && data.ExtDetail.ShipDate != null && data.ExtDetail.ShipDate.length() > 7){
                String String1 = data.ExtDetail.ShipDate;
                String String2 = '-';
                String newString = String1.substring(0, 4) + '-';
                newString = newString.substring(0,newString.length()) + 
                    String1.substring(4,6);
                newString = newString.substring(0,newString.length()) + '-';
                newString = newString.substring(0,newString.length()) +
                    String1.substring(6,String1.length());
                System.debug('newString-->'+newString);
                PLITrackingNumUpdate.Part_Order_Shipping_Date__c = Date.valueOf(newString);
            }
            system.debug('PLITrackingNumUpdate-->'+PLITrackingNumUpdate);
            return PLITrackingNumUpdate;
        }
        catch(Exception ex){
            return PLITrackingNumUpdate;
        }
    }
}