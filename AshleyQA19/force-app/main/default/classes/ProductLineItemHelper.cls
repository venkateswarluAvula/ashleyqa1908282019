public class ProductLineItemHelper {
    public static Boolean isFirstTime = true;
    public static void sendEmail(ProductLineItem__c PLI){
        Id templateId;
        try {
            templateId = [select id, name from EmailTemplate where developername = :System.Label.ShippingEmailTemplateName].id;
        }
        catch (Exception e) {
            System.debug('[U-03] Unable to locate EmailTemplate using name: ' + System.Label.ShippingEmailTemplateName + 
                                     ' refer to Setup | Communications Templates ' + System.Label.ShippingEmailTemplateName);
        }
        Id orgWideEmailId = [SELECT DisplayName,id FROM OrgWideEmailAddress WHERE DisplayName = :System.Label.ShippingEmailAddress].id;
        ProductLineItem__c PLIrec = [SELECT ID, case__c, Case__r.Case_Email__c,Part_Order_Tracking_Num__c,Part_Order_Tracking_Number__c from ProductLineItem__c WHERE ID=:PLI.Id];
        String[] Toemail = new String[] {PLIrec.Case__r.Case_Email__c};
        String emailBody = System.Label.ShippingEmailBody + PLIrec.Part_Order_Tracking_Number__c + '. ' + System.Label.ShippingEmailBody2;
   //     List<String> Toemail = new List<String>();
   //     Toemail.add(PLIrec.Case__r.Case_Email__c);
        Id ContactId = [SELECT AccountId,Id FROM Contact WHERE AccountId = :[SELECT AccountId,Id FROM Case WHERE Id=:PLI.Case__c].AccountId ].Id;
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setToAddresses(Toemail);
     //   email.setTargetObjectId(ContactId);
     //   email.setWhatId(PLIrec.id);
        email.setorgWideEmailAddressId(orgWideEmailId);
        email.setHtmlBody(emailBody);
       // email.setTemplateId(templateId);
        email.setSaveAsActivity(true);      
        System.debug(LoggingLevel.INFO,'** entered sendTemplatedEmail, to:' + Toemail  +  ' templateId:' + templateId + ' tagetObjId:' + ContactId + 
                      ' orgWideEmailId: ' + orgWideEmailId);
        System.debug('Email to :-->'+email.getToAddresses());
        
        try {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            List<Messaging.SendEmailResult> results = 
                Messaging.sendEmail(new Messaging.Email[] { email });
            System.debug('results-->'+results);
            if (!results.get(0).isSuccess()) {
                System.StatusCode statusCode = results.get(0).getErrors()[0].getStatusCode();
                String errorMessage = results.get(0).getErrors()[0].getMessage();
                System.debug('Error Message-->'+errorMessage);
                System.debug('statusCode-->'+statusCode);
            }
            return;
        }
        catch (EmailException e) {
            System.debug('[U-02] sendTemplatedEmail error. ' + e.getMessage());
        }
    }
}