@isTest
public class EDQAddressServiceTests {

	static testmethod void CanVerifyAddress() {
		Test.setMock(HttpCalloutMock.class, new EDQHttpResponseMock(200));

		EDQAddressService addressService = new EDQAddressService('AddressToken', EDQHttpResponseMock.AddressSearchEndpoint);
		String result = addressService.SearchAddress('Akron', 'US', 7);
		String expected ='{"count":7,"results":[{"suggestion":"PO Box 10099, Akron OH 44310","matched":[[7,12],[0,6]],"format":"https://api.edq.com/capture/address/v2/format?country=USA&id=95889123-1cea-48cf-8633-da5f46b74a58ql12"},{"suggestion":"PO Box 10099, Albany NY 12201","matched":[[7,12],[0,6]],"format":"https://api.edq.com/capture/address/v2/format?country=USA&id=fdfa85d0-e324-4dad-a001-a02eafde0219ql12"},{"suggestion":"PO Box 10099, Albuquerque NM 87184","matched":[[7,12],[0,6]],"format":"https://api.edq.com/capture/address/v2/format?country=USA&id=e2d741c8-db56-438d-987e-7385f34b842eql12"},{"suggestion":"PO Box 10099, Alexandria VA 22310","matched":[[7,12],[0,6]],"format":"https://api.edq.com/capture/address/v2/format?country=USA&id=17285670-e81a-4062-b130-919de99338acql12"},{"suggestion":"PO Box 10099, Amarillo TX 79116","matched":[[7,12],[0,6]],"format":"https://api.edq.com/capture/address/v2/format?country=USA&id=3e7b0be2-0c9c-45fa-b7ba-4188f58e4087ql12"},{"suggestion":"PO Box 10099, American Canyon CA 94503","matched":[[7,12],[0,6]],"format":"https://api.edq.com/capture/address/v2/format?country=USA&id=cffcdb43-dc15-4ab4-8b1d-ac7be13dbfb1ql12"},{"suggestion":"PO Box 10099, Anaheim CA 92812","matched":[[7,12],[0,6]],"format":"https://api.edq.com/capture/address/v2/format?country=USA&id=08fea279-976c-43ac-8dbf-d7f7f25eb159ql12"}]}';
		System.assertEquals(expected, result);
	}

	static testmethod void CanFormatAddress() {
		Test.setMock(HttpCalloutMock.class, new EDQHttpResponseMock(200));

		EDQAddressService addressService = new EDQAddressService('AddressToken', EDQHttpResponseMock.AddressSearchEndpoint);
		String result = addressService.FormatAddress('https://api.edq.com/capture/address/v2/format?country=AUS&id=700AUS-NOAUSHAHgBwAAAAAIAwEAAAABV3R_gAAAAAAAAAAA..9kAAAAAP....8AAAAAAAAAAAAAAAAASGFtaWx0b24A');
		String expected = '{"address":[{"addressLine1":"38 Lane Cove Rd"},{"addressLine2":"38 Lane Cove Road"},{"addressLine3":""},{"locality":"RYDE"},{"province":"NSW"},{"postalCode":"2112"},{"country":"AUSTRALIA"}],"components":[{"deliveryPointId1":"76077169"},{"streetNumber1":"38"},{"street1":"Lane Cove Rd"},{"locality1":"RYDE"},{"province1":"New South Wales"},{"provinceCode1":"NSW"},{"postalCode1":"2112"},{"country1":"AUSTRALIA"},{"countryISO1":"AUS"}],"metadata":{}}';
		System.assertEquals(expected, result);
	}

	@isTest
	static void canCreateLogObject() {
		EDQAddressService service = new EDQAddressService('token', 'endpoint');
		Integer statusCode = 502;
		HttpResponse response = new HttpResponse();
		response.setStatusCode(statusCode);
		String method = 'TestMethod';
		String classs = 'EDQAddressService';
		String expectedMessage = 'Validation failed. Status code: ' + statusCode + '. Message: Provider failure.';
		ErrorLogController.Log errorLog = service.createAddressValidationLog(response, method);
		System.assertEquals(method, errorLog.Method);
		System.assertEquals(classs, errorLog.ApexClass);
		System.assertEquals(expectedMessage, errorLog.Message);
	}

	@isTest
	static void searchAddressWillReturnMessageFromMapIfResponseIsNot200AndWillLogError() {
		Integer statusCode = 401;
		Test.setMock(HttpCalloutMock.class, new EDQHttpResponseMock(statusCode));
		EDQAddressService addressService = new EDQAddressService('AddressToken', EDQHttpResponseMock.AddressSearchEndpoint);

		String expected = '{"Message": "' + addressService.fullAddressCodeMessageResponse.get(statusCode) + '"}';
		String expectedMethod = 'SearchAddress';
		String expectedClass = 'EDQAddressService';
		String expectedMessage = 'Validation failed. Status code: ' + statusCode + '. Message: Auth-Token provided is incorrect.';
		System.assertEquals(0, [select count() from ErrorLog__c]);
		String result = addressService.SearchAddress('Akron', 'US', 7);
		System.assertEquals(expected, result);
		List<ErrorLog__c> errors = [select Method__c, ApexClass__c, Message__c from ErrorLog__c];
		System.assertEquals(1, errors.size());
		System.assertEquals(expectedMethod, errors[0].Method__c);
		System.assertEquals(expectedClass, errors[0].ApexClass__c);
		System.assertEquals(expectedMessage, errors[0].Message__c);
	}

	@isTest
	static void formatAddressWillReturnMessageFromMapIfResponseIsNot200AndWillLogError() {
		Integer statusCode = 502;
		Test.setMock(HttpCalloutMock.class, new EDQHttpResponseMock(statusCode));
		EDQAddressService addressService = new EDQAddressService('AddressToken', EDQHttpResponseMock.AddressSearchEndpoint);

		String expected = '{"Message": "' + addressService.fullAddressCodeMessageResponse.get(statusCode) + '"}';
		String expectedMethod = 'FormatAddress';
		String expectedClass = 'EDQAddressService';
		String expectedMessage = 'Validation failed. Status code: ' + statusCode + '. Message: Provider failure.';
		System.assertEquals(0, [select count() from ErrorLog__c]);
		String result = addressService.FormatAddress('does not matter');
		System.assertEquals(expected, result);
		List<ErrorLog__c> errors = [select Method__c, ApexClass__c, Message__c from ErrorLog__c];
		System.assertEquals(1, errors.size());
		System.assertEquals(expectedMethod, errors[0].Method__c);
		System.assertEquals(expectedClass, errors[0].ApexClass__c);
		System.assertEquals(expectedMessage, errors[0].Message__c);
	}
}