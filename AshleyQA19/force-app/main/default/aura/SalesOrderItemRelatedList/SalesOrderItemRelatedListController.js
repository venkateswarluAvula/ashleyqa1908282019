({
	doInit : function(component, event, helper) {

		var currentOrderId = component.get("v.recordId");

    var action = component.get("c.getLineItemswithImage");
 			
		action.setParams({
	        salesforceOrderId : currentOrderId
	    });

	    action.setCallback(this, function(response) {
			var state = response.getState();
      if (state === "SUCCESS") {
				var callResponse = response.getReturnValue(); 																					
          		console.log('callResponse1'+JSON.stringify(callResponse));
				component.set("v.lineItems", callResponse);
          		component.set("{!v.showSpinner}", false);
			}
			else{
				var errorToast = $A.get("e.force:showToast");
				errorToast.setParams({"message": response.getError()[0].message, "type":"error", "mode":"sticky"});
				errorToast.fire();
			}

		});

        /*
        //Added by Sudeshna Saha
        var action2 = component.get("c.getval");
 			
		action2.setParams({
	        salesforceOrderId : currentOrderId
	    });

	    action2.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var callResponse = response.getReturnValue(); 	
                console.log('Est Arrival res >--->'+callResponse);
				component.set("v.lineItemsEstArrival", callResponse); 	
                 console.log('Est Arrival >--->'+component.get("v.lineItemsEstArrival"));
			}
			else{
				var errorToast = $A.get("e.force:showToast");
				errorToast.setParams({"message": response.getError()[0].message, "type":"error", "mode":"sticky"});
				errorToast.fire();
			}

    });
       
        //End by Sudeshna Saha
        $A.enqueueAction(action2);
        */
        $A.enqueueAction(action);
  },

    openPop : function(component, event, helper) {
            var cmpTarget = component.find('pop');
            $A.util.addClass(cmpTarget, 'slds-show');
            $A.util.removeClass(cmpTarget, 'slds-hide');
        	var ctarget = event.currentTarget;
            var id_str = ctarget.dataset.value;
            component.set('v.PopupsoId',id_str);   
            var actiondata = component.get("c.getSalesOrderLineItem");
            console.log('id_str-->'+id_str);
              actiondata.setParams({
               "salesOrderItemId" : id_str,
               "salesOrderId" : component.get("v.recordId")
                                     });
        		 actiondata.setCallback(this, function(response){ 
                //  component.find("Id_spinner").set("v.class" , 'slds-hide');
                    var state = response.getState();
                  		 if (state === "SUCCESS") {
                       		 var callResponse = response.getReturnValue();
                        //alert('callResponse'+JSON.stringify(callResponse));
                        console.log('callResponse-->'+JSON.stringify(callResponse));
                        		component.set("v.SalesOrderData", callResponse);
                             console.log('SalesOrderData >->'+component.get("v.SalesOrderData"));
                         }
                      	else{
                   		 var errorToast = $A.get("e.force:showToast");
                    		errorToast.setParams({"message": response.getError()[0].message, "type":"error", "mode":"sticky"});
                    		errorToast.fire();
                			}
                        	});
                    
           $A.enqueueAction(actiondata);
            
    },
    
    closePop : function(component, event, helper) {
            var cmpTarget = component.find('pop');
            $A.util.addClass(cmpTarget, 'slds-hide');
            $A.util.removeClass(cmpTarget, 'slds-show');
           // var selectedItem = event.currentTarget;
           // var Id = selectedItem.dataset.record;
    
    },
    openImage : function(component, event, helper) {
        var cmpTarget = component.find('imagepop');
        $A.util.addClass(cmpTarget, 'slds-show');
        $A.util.removeClass(cmpTarget, 'slds-hide');
        var imagev = component.get("v.lineItems")[0].imageurl;
        component.set("v.imageUrl", event.currentTarget.src);
    },
    closeImage : function(component, event, helper) {
        var cmpTarget = component.find('imagepop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
    },
	launchLineItemDeatil : function(component, event, helper) {
        console.log('here');
		var currentOrderLineId = event.currentTarget.dataset.orderlineid;
		var currentOrderSFDCId = event.currentTarget.dataset.ordersfdcid;
		//alert('currentOrderLineId-----'+currentOrderLineId);
        //alert('currentOrderSFDCId-----'+currentOrderSFDCId);
	    $A.createComponent(
            "c:SalesOrderLineItemDetail",
            
            {
                "lineItemId": currentOrderLineId,
                "orderSfdcId": currentOrderSFDCId
            },
            function(msgBox){                
                if (component.isValid()) {
                    var popupPlaceholder = component.find('lineItemDetailsPlaceHolder');
                    var body = popupPlaceholder.get("v.body");
                    body.push(msgBox);
                    popupPlaceholder.set("v.body", body); 
                }
            }
        );		
	},
    UpdateColor : function(component, event, helper){
        var img = component.find('popup');
        if($A.util.hasClass('pop-up',img)){
            $A.util.removeClass('pop-up',img);
            $A.util.addClass('pop-upimg',img)
        }else{
            $A.util.addClass('pop-up',img);
            $A.util.removeClass('pop-upimg',img);
        }
    }
})