({
	getDetails : function(component) {
        var action = component.get("c.getDetails");
        action.setParams({
    	objectID: component.get("v.recordId")
		});

        action.setCallback(this, function(a){
            var res = a.getReturnValue();
            component.set("v.salesOrder", res);
        });
        $A.enqueueAction(action);
        var action2 = component.get("c.getDeliveryDate");
        action2.setParams({
		objectID: component.get("v.recordId")
		});
        action2.setCallback(this, function(a){
            var res = a.getReturnValue();
            console.log('delDate-->'+res);
            component.set("v.DeliveryDate", res);
        });
        $A.enqueueAction(action2);
	},
    
})