({
	doInit : function(component, event, helper) {        
		var action = component.get("c.formDispatchTrackUrl"); 
        //var pageReference = component.get("v.pageReference");
        //component.set("v.recordId", pageReference.state.c__recordId);
        action.setParam("recId", component.get("v.recordId"));
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            console.log("state" + a.getState());
            if (a.getState() == 'SUCCESS') {
                if (rtnValue != null){
                    component.set("v.dispatchTrackUrl", rtnValue);
                    console.log("dispatchtrackurl" + rtnValue);
                    console.log("url" + $A.get("$Label.c.DispatchTestUrl"));
                }
            } else {
                var errors = a.getError(); 
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.Status", true);
                        console.error(errors[0].message);
                    }
                    if (errors[0] && errors[0].pageErrors && errors[0].pageErrors[0]) {
                        console.error(errors[0].pageErrors[0].message);
                    }
                } else {
                    console.error("Unknown error");
                }  
            }
        });
    	$A.enqueueAction(action);
	},
    
    // when user click on the close buttton on message popup ,
    closeMessage: function(component, event, helper) {
        var recId = component.get("v.recordId");
        component.set("v.Status", false);
        //sforce.console.closeTab();
        //window.self.close();
        window.location.href = '/' + recId;
    },
    
})