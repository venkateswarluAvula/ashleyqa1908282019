({
    doInit : function(component, event, helper) {
        console.log(component.get("v.shipToAddress"));
        
        helper.getDiscountReasonList(component,helper);
        helper.getCheckoutSummary(component, helper);
        helper.getAccountStoreInfo(component,helper);
        helper.getCustomerInfo(component,helper);
        
    },
    OpenDeliveryIcon : function(component, event, helper){
        component.set("v.showDiscountModal",true);
        if(component.get("v.DeliveryIcon")){
            component.set("v.DeliveryIcon",false);   
        }else{
            component.set("v.DeliveryIcon",true);  
        }
    },
    cancelModal : function(component, event, helper){
        var selectedItem = event.currentTarget; 
        var containerName = selectedItem.dataset.record;
        helper.closeModal(component,containerName); 
        component.set("v.DeliveryIcon",false); 
    },
    SaveDiscount: function(component, event, helper){
        helper.SaveDiscount(component, helper); 
    },
    EditDelivery : function(component, event, helper){
        var selectedItem = event.currentTarget; 
        var containerName = selectedItem.dataset.record;       
        var popUp = component.find(containerName);
        $A.util.removeClass(popUp, 'slds-fade-in-close');
        $A.util.addClass(popUp, 'slds-fade-in-open');
        var backdrop = component.find("backdropContainer");
        $A.util.removeClass(backdrop, 'slds-modal-backdrop--close');
        $A.util.addClass(backdrop, 'slds-modal-backdrop--open'); 
    },
    changeDiscount: function(component, event, helper){
        var updatedValue = event.getSource().get("v.value");
        var auraId = event.getSource().get("v.name");
        var itemTotalPrice =  component.get("v.shoppingCartOriginalPrice");
        // alert('itemTotalPrice'+itemTotalPrice);
        if(auraId=='itemsPrice'){
            component.find("flatDiscountAmount").set("v.value",(itemTotalPrice-updatedValue));
            var percentDiscountAmount = Math.round(1000000*(itemTotalPrice-updatedValue)/itemTotalPrice)/10000;
            var opp=component.get("v.opp");
            opp["Shipping_Discount__c"]=percentDiscountAmount;
            component.set("v.opp",opp);
            component.find("percentDiscountAmount").set("v.value",Math.round(100*percentDiscountAmount)/100);
            
        }else if(auraId=='flatDiscountAmount'){
            component.find("itemsPrice").set("v.value",(itemTotalPrice-updatedValue));
            var percentDiscountAmount = Math.round(1000000*(updatedValue)/itemTotalPrice)/10000;
            var opp=component.get("v.opp");
            opp["Shipping_Discount__c"]=percentDiscountAmount;
            component.set("v.opp",opp);
            
            component.find("percentDiscountAmount").set("v.value",Math.round(100*percentDiscountAmount)/100);
        }else if(auraId=='percentDiscountAmount' ){
            component.find("itemsPrice").set("v.value",Math.round(100*itemTotalPrice-updatedValue*itemTotalPrice)/100);
            component.find("flatDiscountAmount").set("v.value",Math.round(updatedValue*itemTotalPrice)/100);   
            var opp=component.get("v.opp");
            opp["Shipping_Discount__c"]=updatedValue;
            component.set("v.opp",opp);
        }
        
        
    },
    reloadSummary : function(component, event, helper){
        console.log('--in reload Summary-- JoJo test');
        console.log(component.get("v.shipToAddress"));
        helper.getCheckoutSummary(component, helper);
    },
    reloadDiscountStatus: function(component, event, helper){
        if(component.get("v.DiscountStatusUpdate")){
            helper.getCustomerInfo(component,helper);
            var eventComponent = component.getEvent("DiscountStatusNotifyToCheckoutSummary");
            eventComponent.setParams({ "notifyParam":  false});
            eventComponent.fire();   
            
        }   
        
    },
    reloadSummaryForDeliveryDateUpdated: function(component, event, helper){
        if(component.get("v.isRenderForUpdate")){
            //alert('enter');
            helper.getCheckoutSummary(component, helper);
            component.set("v.shoppingCartTotalDiscount",component.get("v.tempvalextendedprice"));
           //alert('extended price'+component.get("v.tempvalextendedprice"));
            //event to notify Checkout when refresh finished, then update isRenderForUpdate back to false
            var eventComponent = component.getEvent("DeliveryDateChangeNotifyToCheckoutSummary");
            eventComponent.setParams({ "notifyParam":  false});
            eventComponent.fire(); 
        }
        
    }
})