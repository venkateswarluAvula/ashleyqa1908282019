({
    showToast : function(type, title, component, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            type: type,
            title: title,
            message: message,
        });
        toastEvent.fire();
    },
    
    deleteItems: function(component,helper){
        var seletedItem=new Array();
        var hasError = false;
        var currentItem = component.find("setCurrentItemDelete");
        var currentItemId=  currentItem.get("v.value");
        if(currentItem.get("v.checked")){
            seletedItem.push(currentItemId);
        }
        var allItems =  component.get("v.shoppingCartLineItems");
        var itemTotal=[];
        var totalSize;
        allItems.forEach(function(item){
            totalSize=itemTotal.push(item["item"]);
        });
        if(totalSize>1){
            component.find("setDelete").forEach(function(element){
                var itemId= element.get("v.value");
                if(itemId!=currentItemId ){
                if(element.get("v.checked")){
                    seletedItem.push(itemId);
                }
                }
            });
        }else{
            var itemId= component.find("setDelete").get("v.value");
            if(component.find("setDelete").get("v.checked")){
                seletedItem.push(itemId);
            }  
        }
        if(seletedItem.length==0){
            helper.showToast("error", 'Please Select at least one item to Delete.', component,
                             'Please  Select at least one item to Delete.');   
            hasError = true;
        }
        if(hasError){
            return;
        } 
        
        var action = component.get("c.multipleDleteFromCart");
        var toastErrorHandler = component.find('toastErrorHandler');
        action.setParams({"lineItemIdList" : seletedItem,
                          "AccountId" : component.get('v.accountId')
                         });
        action.setCallback(this, function(response){
            toastErrorHandler.handleResponse(
                response, // handle failure
                function(response){ 
                    var rtnValue = response.getReturnValue();
                    if (rtnValue !== null && rtnValue=='Success') {
                        component.getEvent("NotifyParentCloseDeleteModal").fire();  
                        var event = component.getEvent("shoppingCartLineItemEvent");
                        event.setParams({"action" : component.get("v.CART_ACTION_DELETE_ITEM"), "lineItemIds" : seletedItem});
                        event.fire();
                    } else {
                        helper.showToast("error", 'Failed to Delete Items', component,
                                         'Failed to Delete Items.');                        
                    }
                },
                function(response, message){ // report failure
                    helper.showToast("error", 'Failed to Delete Items', component,
                                     message);
                }
            )            
        });        
        action.setBackground();
        $A.enqueueAction(action); 
    },
})