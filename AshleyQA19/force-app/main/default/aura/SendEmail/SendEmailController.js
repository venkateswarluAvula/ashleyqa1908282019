({
    sendMail: function(component, event, helper) {
        // when user click on Send button 
        // First we get all fields values 	
        var getEmail = component.get("v.email");
        var getSubject = component.get("v.subject");
        var getbody = component.get("v.body");
        var getrecid = component.get("v.recordId");
        var templateId = component.get("v.selTempl");
        var frommail = component.find("From").get("v.value");
              
        // check if Email field is Empty or not contains @ so display a alert message 
        // otherwise call call and pass the fields value to helper method    
        if ($A.util.isEmpty(getEmail) || !getEmail.includes("@")) {
            alert('Please Enter valid Email Address');
        }else if ($A.util.isEmpty(getbody) && $A.util.isEmpty(templateId)) {
            alert('Please fill Mail Body or select a Template.');
        }else {
           
            helper.sendHelper(component, event, helper,getEmail, getSubject, getbody,getrecid);
        }
    },
    loadComponent : function(component, event, helper) {
        helper.getEmailTempaltes(component, event);
    },
    loadTemplate : function(component, event, helper) {
        helper.getTemplate(component, event);
        
    },
 
    // when user click on the close buttton on message popup ,
    closeMessage: function(component, event, helper) {
        component.set("v.mailStatus", false);
        component.set("v.email", null);
        component.set("v.subject", null);
        component.set("v.body", null);
    },
})