({
    doInit : function(component, event, helper) {
        component.set('v.isLoading',true);
        
        var recId = component.get("v.recordId");
        
        var action = component.get("c.UpdateRecord");  
        action.setParams({
            currentrecId : recId
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                
                var RecordValue = response.getReturnValue();
                console.log('RecordValue--'+RecordValue.Status__c);
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": RecordValue.Id,
                    "slideDevName": "detail"
                });
                navEvt.fire();
                
                $A.get('e.force:refreshView').fire();
                
                if(RecordValue.Status__c != 'Complete'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({"message": 'Refresh calculation is successfully Updated.', 
                                          duration:' 5000',"type": "success"});
                    toastEvent.fire();
                    return; 
                }
                if(RecordValue.Status__c == 'Complete'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({"message": 'Refresh calculation is not allwed in Complete Status.', 
                                          duration:' 5000',"type": "info"});
                    toastEvent.fire();
                    return; 
                }
                
            }else if(state == "ERROR"){
                //alert('Error in calling server side action');
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({"message": 'Error in calling server side action.', "type": "Error"});
                toastEvent.fire();
                return; 
            }
        });
        $A.enqueueAction(action);
        
        component.set('v.isLoading',false);
        
    }
})