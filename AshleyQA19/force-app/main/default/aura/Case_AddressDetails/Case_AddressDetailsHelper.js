({
    getDetails : function(component) {
        console.log('recordId---->'+component.get("v.recordId"));
        var action = component.get("c.getDetails");
        action.setParams({
            objectID: component.get("v.recordId")
        });
        action.setCallback(this, function(a){
            if (component.isValid() && a.getState() === "SUCCESS") {
                var res = a.getReturnValue();
                if (res != null) {
                    component.set("v.addressDetail", res);
                }
            }
            else {
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": a.getError()[0].message, "type":"error"});
                errorToast.fire();
            }
        });
        $A.enqueueAction(action);
        
        var action2 = component.get("c.getContactDetails");
        action2.setParams({
            objectID: component.get("v.recordId")
        });
        action2.setCallback(this, function(a){
            if (component.isValid() && a.getState() === "SUCCESS") {
                var res = a.getReturnValue();
                console.log('res--->'+res);
                if (res != null) {
                    var newPhoneNo = this.formatPhoneNumber(component,res.Phone);    
                    res.Phone = newPhoneNo;  
                    newPhoneNo = this.formatPhoneNumber(component,res.Phone_2__c);
                    res.Phone_2__c = newPhoneNo;
                    newPhoneNo = this.formatPhoneNumber(component,res.Phone_3__c);
                    res.Phone_3__c = newPhoneNo;
                    console.log('Phone--->'+res.Phone);
                    component.set("v.ContactDetail", res);
                }
            }
            else {
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": a.getError()[0].message, "type":"error"});
                errorToast.fire();
            }
        });
        $A.enqueueAction(action2);
    },
    formatPhoneNumber: function(component, phone) {
        var s2 = (""+phone).replace(/\D/g, '');
        var m = s2.match(/^(\d{3})(\d{3})(\d{4})$/);
        return (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
    },
})