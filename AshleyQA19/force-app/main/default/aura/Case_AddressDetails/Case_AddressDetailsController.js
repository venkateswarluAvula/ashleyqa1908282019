({
	doInit : function(component, event, helper) {
        helper.getDetails(component);
    },
    navigateToAccount : function(component,event, helper) {
       console.log('on Contact');
       var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.ContactDetail.Id"),
            "slideDevName": "details"
        });
        navEvt.fire();

    } 
})