({
     
    doInit : function(component, event, helper) {
        
        helper.loadSalesOrder(component, event, helper);
        component.set("v.IsSendEmail",false);
    },
    onCheck: function(component, event, helper) {
        var chkbxtxt = event.getSource().get("v.text");
        console.log('chkbxtxt--'+chkbxtxt);
        var chkbx = event.getSource().get("v.value");
        var getSelectedNumber = component.get("v.selectedCount");
        
        if(chkbx==true){
            getSelectedNumber++;
        }else{
            getSelectedNumber--;   
        }
        component.set("v.selectedCount", getSelectedNumber);
        
        if(getSelectedNumber > 0){
            component.set("v.IsSendEmail",true);
        }else{
            component.set("v.IsSendEmail",false);   
        }
        if(getSelectedNumber > 10){
            var successToast = $A.get("e.force:showToast");
                successToast.setParams({"message": "Please select only 10 or less Invoice Orders.", "type":"Error", "mode":"dismissible", "duration":4000});
                successToast.fire();
        }
    },
    
    OpenEmailPop: function(component, event, helper) {
         component.set("v.isOpen", true);
    },
    Discard: function(component, event, helper) {
        component.set("v.isOpen", false);
    },
    
    SendEmail: function(component, event, helper) {
        
        
        var RecId = [];
        // get all checkboxes 
        var getAllId = component.find("Hischeckbox");
        
        // If the local ID is unique[in single record case], find() returns the component. not array
        if(! Array.isArray(getAllId)){
            if (getAllId.get("v.value") == true) {
                RecId.push(getAllId.get("v.text"));
            }
        }else{
            for (var i = 0; i < getAllId.length; i++) {
                if (getAllId[i].get("v.value") == true) {
                    RecId.push(getAllId[i].get("v.text"));
                }
            }
        } 
        console.log('RecId--'+RecId);
        
        var subj = component.find("sub").get("v.value");
        var ebody = component.find("bdy").get("v.value");
        var toemail = component.find("email").get("v.value");
        console.log('toemail--'+toemail);
        
        /*
        if($A.util.isEmpty(toemail) || $A.util.isUndefined(toemail)){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "message": 'please provide details.',
                "type": "Error"
            });
            toastEvent.fire();
            return;
        }
        */    
        
        var recordid = component.get("v.recordId");
        console.log('recordid--'+recordid);
        var action = component.get("c.pdfdata");
        action.setParams({
            "soid":RecId,
            "currentRecId":recordid,
            "messagesub":subj,
            "messagebody":ebody,
            "toemailadd":toemail
        });
        action.setCallback(this,function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                var succesData = response.getReturnValue();
                console.log('succesData--'+JSON.stringify(succesData));
                var successToast = $A.get("e.force:showToast");
                successToast.setParams({"message": "Mail Sent Sucessfully for selected Invoiced Orders only.", "type":"success", "mode":"dismissible", "duration":4000});
                successToast.fire();
                //component.set("v.data", succesData);
                //component.set("v.datalenght",succesData.length);
                
            }else{
                var successToast = $A.get("e.force:showToast");
                successToast.setParams({"message": "Error From server side.", "type":"Error", "mode":"dismissible", "duration":4000});
                successToast.fire();
            }
        }); 
        $A.enqueueAction(action);
        component.set("v.isOpen", false);
        component.set("v.email", null);
        component.set("v.msgsub", null);
        component.set("v.msgbody", null);
        
    },
    
    openPop : function(component, event, helper) {
        
        var cmpTarget = component.find('pop');
        
        $A.util.addClass(cmpTarget, 'slds-show');
        $A.util.removeClass(cmpTarget, 'slds-hide');
        var ctarget = event.currentTarget;
        var id_str = ctarget.dataset.value;
        console.log('id--'+id_str)
        component.set('v.PopupsoId',id_str);   
        var actiondata = component.get("c.getSalesOrderLineItem");
        
        actiondata.setParams({
            "salesOrderId" : id_str
        });
        
        actiondata.setCallback(this, function(response){    
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var callResponse = response.getReturnValue();
                console.log('callResponse'+JSON.stringify(callResponse));
                component.set("v.SalesOrdLineItemData", callResponse);
            }
            else{
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": response.getError()[0].message, "type":"error", "mode":"sticky"});
                errorToast.fire();
            }
        });
        
        $A.enqueueAction(actiondata);
        
    },
    
    closePop : function(component, event, helper) {
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
        // var selectedItem = event.currentTarget;
        // var Id = selectedItem.dataset.record;
        
    },
    navigateToRecord : function (component, event, helper) {
        
        //window.location='/'+component.get("v.data").Id+'';
        var navEvt = $A.get("e.force:navigateToURL");
        //alert('navEvt');
        console.log('navEvt===>'+navEvt);
        navEvt.setParams({
            "url": '/lightning/r/SalesOrder__x/' + component.get("v.PopupsoId") + '/view'
        });
        navEvt.fire();
    },
    
    DownloadFile : function (component, event, helper) {
        component.set("v.openpopup", true);
    },
    doInnerYes : function(component, event, helper) { 
        var RecId = [];
        // get all checkboxes 
        var getAllId = component.find("Hischeckbox");
        if(! Array.isArray(getAllId)){
            if (getAllId.get("v.value") == true) {
                RecId.push(getAllId.get("v.text"));
            }
        }else{
            for (var i = 0; i < getAllId.length; i++) {
                if (getAllId[i].get("v.value") == true) {
                    RecId.push(getAllId[i].get("v.text"));
                }
            }
        } 
        console.log('RecId--'+RecId);
        console.log('RecId--'+RecId.length); 
        
        var recordid = component.get("v.recordId");
        
        console.log('recordid--'+recordid);
        var action = component.get("c.downloadpdf");
        action.setParams({
            "soid":RecId,
            "currentRecId":recordid
            
        });
        action.setCallback(this,function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                var succesData = response.getReturnValue();
                component.set("v.attachmentId", succesData);
                console.log('succesData--'+succesData);
                console.log('succesData--1-'+JSON.stringify(succesData));
                
                var burl = $A.get("{!$Label.c.BaseURL}");
                //alert('url--1-'+burl+'//servlet/servlet.FileDownload?file='+succesData);
                
                var navEvt = $A.get("e.force:navigateToURL");
                navEvt.setParams({
                    //"url": burl+'/servlet/servlet.FileDownload?file='+succesData
                    "url": burl+succesData
                });
                navEvt.fire();
                
                var successToast = $A.get("e.force:showToast");
                successToast.setParams({"message": "File Downloaded Sucessfully.", "type":"success", "mode":"dismissible", "duration":4000});
                successToast.fire();
                                
            }else{
                var successToast = $A.get("e.force:showToast");
                successToast.setParams({"message": "Download error from server.", "type":"Error", "mode":"dismissible", "duration":4000});
                successToast.fire();
            }
        }); 
        $A.enqueueAction(action);
        
    	component.set("v.openpopup", false);
    },
    doinnerNo : function(component, event, helper) {  
        component.set("v.openpopup", false);
    }
})