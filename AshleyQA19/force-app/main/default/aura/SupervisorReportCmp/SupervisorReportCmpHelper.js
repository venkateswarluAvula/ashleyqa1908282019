({
    Intitialmethod : function(component, event,helper){
        component.set("v.Spinner", true); 
      component.set("v.maxPage", 0);  
        var action1 = component.get("c.getManager");
        action1.setCallback(this, function(response1) {
            var state1 = response1.getState();
            if (state1 === "SUCCESS") {
                console.log('sup--'+response1.getReturnValue());
                component.set("v.SupervisorName",response1.getReturnValue());
            }
        }); 
        $A.enqueueAction(action1);
        
        var userName = component.get("v.Username");
        console.log('userName--'+userName);
        if (userName != null){
            console.log('entered');
            component.set("v.UserBool",true);
            var actionuser = component.get("c.getspecificUserData");
            actionuser.setParams({
                'EmployeeName':userName
            });
            actionuser.setCallback(this, function(responseuser) {
                var state = responseuser.getState();
                if (state === "SUCCESS") {
                    var jsonlegnth = responseuser.getReturnValue().length;
                    component.set("v.allData",responseuser.getReturnValue()); 
                   // component.set("v.Userslist1",responseuser.getReturnValue()); 
                    if(jsonlegnth > 0){
                    component.set('v.pageNumber',1);		
                	component.set("v.maxPage", Math.floor((jsonlegnth+9)/10));
                    helper.renderPage(component, event, helper);
                     component.set("v.Spinner", false); 
                    }else{
                        component.set("v.Spinner", false); 
                        var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "Error Message",
                                    "type": "error",
                                    "message": "No Records found" 
                                });
                                toastEvent.fire();
                    }
                }
            }); 
            $A.enqueueAction(actionuser);
            
        }
        else{
            console.log('entered 2');
            component.set('v.UserBool',false);
            var action = component.get("c.getData"); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log('state--'+state);
                if (state === "SUCCESS") {
                    var jsonlegnth = response.getReturnValue().length;
                    component.set("v.allData",response.getReturnValue()); 
                    console.log('response.getReturnValue()--'+response.getReturnValue());
                    if(jsonlegnth > 0){
                    component.set('v.pageNumber',1);		
                	component.set("v.maxPage", Math.floor((jsonlegnth+9)/10));
                    helper.renderPage(component, event, helper);
                     component.set("v.Spinner", false); 
                    }
                    else{
                      component.set("v.Spinner", false);   
                    }
                }else{
                    component.set("v.Spinner", false);  
                   var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "Error Message",
                                    "type": "error",
                                    "message": "Failed to load records" 
                                });
                                toastEvent.fire();
                  
                }
            }); 
            $A.enqueueAction(action);
            
        }
    },
    
    renderPage: function(component, event, helper) {
        var userName = component.get("v.Username");
        var records = component.get("v.allData"),
        pageNumber = component.get("v.pageNumber"),	  	
        pageRecords = records.slice((pageNumber-1)*10, pageNumber*10);
        if (userName != null){
        component.set("v.Userslist1", pageRecords);	      
        }else{
        component.set("v.Userslist", pageRecords);	    
        }
    }
})