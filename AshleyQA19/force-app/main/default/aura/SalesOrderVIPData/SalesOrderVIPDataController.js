({
    /** Client-side Controller **/
    doInit : function(component, event, helper) {
        var StartDate = new Date();
        var dd = StartDate.getDate();
        var mm = StartDate.getMonth() + 1; //January is 0!
        var yyyy = StartDate.getFullYear();
     // if date is less then 10, then append 0 before date   
        if(dd < 10){
            dd = '0' + dd;
        } 
    // if month is less then 10, then append 0 before date    
        if(mm < 10){
            mm = '0' + mm;
        }
        var EndDate = new Date();
        var dd = EndDate.getDate();
        var mm = EndDate.getMonth() + 1; //January is 0!
        var yyyy = EndDate.getFullYear();
      /// var EndDate = new Date("2049-01-01");
        component.set('v.StartDate', yyyy + "-" + mm + "-" + dd);
         component.set('v.EndDate', yyyy + "-" + mm + "-" + dd);
      //  component.set('v.EndDate', EndDate.getFullYear() + "-" + (EndDate.getMonth() + 1) + "-" + EndDate.getDate());
       
        var markets=component.get("c.marketvalues");
        var self = this;
        markets.setCallback(this, function(actionResult) {
            component.set("v.marketoptions", actionResult.getReturnValue());
        });
        $A.enqueueAction(markets);
    },    
    dateUpdate : function(component, event, helper) {
        var startDateField = component.find("StartDate");
        var startDateFieldValue = startDateField.get("v.value");
              
        var endDateField = component.find("EndDate");
        var endDateFieldValue = endDateField.get("v.value");
        
        var marketField = component.find("MarketAccount");
        var marketFieldValue = marketField.get("v.value");
        //alert ('endDateFieldValue	9'+endDateFieldValue);
        //var today = new Date(Date.now()).toISOString().slice(0,10);
       // alert('today'+today);
        
                
           /* if(startDateFieldValue>today)
            {
                component.set("v.dateValidationError" , true);
            }
            else if((endDateFieldValue>today)){
               // alert('entered');
                component.set("v.dateValidationError1" , true);
            }
            else{
                component.set("v.dateValidationError" , false);
                component.set("v.dateValidationError1" , false);
            }*/
        
    },
    handleClick: function(component, event, helper){
      
        component.set('v.myColumns', [
            {type: 'button',initialWidth: 200,
             typeAttributes: {iconName: 'utility:view',label: 'View Sales Order',name: 'viewSalesOrder', disabled: false, value: 'viewBtn'}
            },
            {label: 'Sales Order', fieldName: 'OrderNumber', type: 'text', iconName: 'standard:opportunity'},
            {type:'button', 
             typeAttributes: {iconName:'utility:view', label: 'View Case', name: 'viewCase', title: 'Click to view Case', disabled: {fieldName: 'CaseBtn'}, value:'viewBtn'}
            },
            {label: 'Case', fieldName: 'Case', type: 'URL'},
			{label: 'SKU', fieldName: 'Sku', type: 'text'},
            {label: 'Market', fieldName: 'Market', type: 'text'},
            //{label: 'AccountShipTo', fieldName: 'AccountShipTo', type: 'Date'},
            {label: 'DeliveryDate', fieldName: 'DeliveryDate', type: 'Date', cellAttributes: { iconName: 'utility:date_time', iconAlternativeText: 'Close Date'  }},
            {label: 'VIP', fieldName: 'VIP', type: 'boolean', initialWidth: 100},
            {label: 'LIB', fieldName: 'LIB', type: 'boolean', initialWidth: 100},
            ]);
            
        helper.getvaluesfromUI(component,event,helper);
        
    },
    // Row Action-- Added by praneeth
    handleRowAction :function(component,event,helper){
        var action = event.getParam('action');
        var row = event.getParam('row');
        
        var salesorder = row.OrderNumber;
        var accountId = row.SFContactID;
        var caseid = row.CaseId;
        var extid = salesorder + ':' + accountId ;
            switch (action.name){
            case 'viewSalesOrder':
        var action = component.get("c.searchAccountsSOQL");
        action.setParams({
            'searchString' : extid
        });
        
        action.setCallback(this, function(response) {           
            var state = response.getState();
            if (state === "SUCCESS") {
                var response = response.getReturnValue();
                console.log('response' + response);
                component.set("v.responseId",response);
                console.log('res' + response);
                var navEvent = $A.get("e.force:navigateToURL");
                console.log('here');
                navEvent.setParams({
                    "url": '/lightning/r/SalesOrder__x/' + response + '/view'
                });
                console.log('here');
                navEvent.fire();
            }
            
        });
        $A.enqueueAction(action);
            break;
            case 'viewCase':
            var navEvent = $A.get("e.force:navigateToURL");
			console.log('here');
			navEvent.setParams({
			"url": '/lightning/r/Case/' + caseid + '/view'
			});
			console.log('here');
			navEvent.fire();
			
            break;
            }
	 },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
            
    // ## function call on Click on the "Download As CSV" Button. 
            exportExcel : function(component,event,helper){
        
        // get the Records [contact] list from 'mydata' attribute 
        var stockData = component.get('v.myData');
        
        // call the helper function which "return" the CSV data as a String   
        var csv = helper.convertArrayOfObjectsToCSV(component,stockData);   
         if (csv == null){return;} 
            
        console.log('csv: ' + csv);
        // ####--code for create a temp. <a> html tag [link tag] for download the CSV file--####     
	     var hiddenElement = document.createElement('a');
          hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
          hiddenElement.target = '_self'; // 
          hiddenElement.download = 'VIP/LIB Orders.csv';  // CSV file Name* you can change it.[only name not .csv] 
          document.body.appendChild(hiddenElement); // Required for FireFox browser
    	  hiddenElement.click(); // using click() js function to download csv file
        }
})