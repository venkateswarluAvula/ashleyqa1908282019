({
    // object and set the corresponding aura attribute.
    getvaluesfromUI : function(component,event,helper) {
        var type = component.find("SaleType").get("v.value");
        var asapv = component.get("v.asapchecked");
        var rows = [];
        var eachRow ;
        
        var startDateFieldValue = component.find("StartDate").get("v.value");
        var sdateSplit = startDateFieldValue.split('-');
        var strtdat = new Date(sdateSplit[0], sdateSplit[1]-1, sdateSplit[2]);
        var strt = strtdat.toISOString().slice(0,10);
        var today = new Date(Date.now()).toISOString().slice(0,10);
        
        if (!isNaN(strtdat.getTime())) {
            // Months use 0 index.
            var startdatevalupd;
            if(asapv == true){
            startdatevalupd = '1900-01-01';
            }
            else{
                startdatevalupd = strtdat.getFullYear() + '-' + (strtdat.getMonth() + 1) + '-' + strtdat.getDate();
            }
            component.set('v.updatedstartdate', startdatevalupd);
            //alert('startdatevalupd '+startdatevalupd);
        }
        var endDateFieldValue = component.find("EndDate").get("v.value");
        var edateSplit = endDateFieldValue.split('-');
        var enddate = new Date(edateSplit[0], edateSplit[1]-1, edateSplit[2]);
        var endd = enddate.toISOString().slice(0,10);
        if(startDateFieldValue && endDateFieldValue )
        {
            /*if (strt>today){
           // alert('entered'+today);
            document.getElementById('error1').innerHTML="Start date should be less than or equal to today";
            return;
        }
        else if (endd>today){
           // alert('entered'+today);
             document.getElementById('error1').innerHTML="End date should be less than or equal to today";
            return;
        }*/
            if (strt>endd){
                document.getElementById('error1').innerHTML="Start date should be less than End date";
                return;
            }
            
            else{
                document.getElementById('error1').innerHTML="";
            }
        }
        
        if (!isNaN(enddate.getTime())) {
            // Months use 0 index.
            var enddatevalupd;
            if(asapv == true){
                enddatevalupd = '1990-01-01';
            }
            else{
                enddatevalupd = enddate.getFullYear() + '-' + (enddate.getMonth() + 1) + '-' + enddate.getDate();
            }
            component.set('v.updatedenddate', enddatevalupd);
        }
        var marketField = component.find("MarketAccount");
        var marketFieldValue = marketField.get("v.value");
        
        console.log('startdate---' + component.get('v.updatedstartdate'));
        console.log('enddate---' + component.get('v.updatedenddate'));
        console.log('marketFieldValue---' + marketFieldValue);
        console.log('type---' + type);
        
        var action = component.get("c.SalesOrderVIPStatus");
        action.setParams({
            'startdate':component.get('v.updatedstartdate'),
            'enddate':component.get('v.updatedenddate'),
            'market':marketFieldValue,
            'type' :type
        });
        action.setCallback(this, function(response) {
            console.log('res-----'  + response.getReturnValue());
            var json = response.getReturnValue();
            //var json1 = JSON.stringify(response);
            //var json = JSON.parse(json1);
            console.log('values---'+JSON.stringify(json));
            console.log('json length---'+json.length);
            
            for(var i=0;i<json.length;i++){
                var vipvalue = false;
                if(json[i].VIP == 'True'){
                    vipvalue = true;    
                }
                var libvalue = false;
                if(json[i].LIB == 'True'){
                    libvalue = true;    
                }
                var disableCaseBtn = false;
                if(json[i].CaseNumber == undefined){
                    disableCaseBtn = true;    
                }
                
                eachRow = {
                    SFContactID: json[i].SFContactID,
                    OrderNumber: json[i].OrderNumber,
                    ViewCase: json[i].ViewCase,
                    CaseId: json[i].CaseId,
                    Case: json[i].CaseNumber,
                    Sku: json[i].SKU,
                    Market: json[i].Market,
                    //AccountShipTo: json[i].AccountShipTo,
                    DeliveryDate: json[i].DeliveryDate,
                    Type: json[i].Type,
                    VIP: vipvalue,
                    LIB: libvalue,
                    CaseBtn: disableCaseBtn
                }
                rows.push(eachRow);
            }
            
            component.set('v.myData',rows);
            if(json.length > 0){
                var totalRecordsList = json;
                var totalLength = totalRecordsList.length ;
                component.set("v.isExport",false);
                component.set("v.totalRecordsCount", totalLength);
                component.set("v.bNoRecordsFound" , false);
            }else{
                // if there is no records then display message
                component.set("v.bNoRecordsFound" , true);
            }
        });
        $A.enqueueAction(action);
    },
    
    convertArrayOfObjectsToCSV : function(component,objectRecords){
        // declare variables
        var csvStringResult, counter, keys, columnDivider, lineDivider;
        console.log('objectRecords:' + JSON.stringify(objectRecords));
        // check if "objectRecords" parameter is null, then return from function
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
        
        // in the keys valirable store fields API Names as a key 
        // this labels use in CSV file header  
        keys = ['OrderNumber','Case','Sku','Market','DeliveryDate','VIP','LIB'];
        
        csvStringResult = '';
        csvStringResult += keys.join(columnDivider);
        csvStringResult += lineDivider;
        
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0;
            
            for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
                
                // add , [comma] after every String value,. [except first]
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }
                if (objectRecords[i][skey] != undefined) {
                    csvStringResult += '"'+ objectRecords[i][skey] +'"'; 
                } else {
                    csvStringResult += '""'; 
                }
                counter++;
            } // inner for loop close 
            csvStringResult += lineDivider;
        }// outer main for loop close 
        
        // return the CSV formate String 
        return csvStringResult;        
    },
    
})