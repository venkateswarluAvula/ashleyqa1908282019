({
	getAccountOrders : function(component, event, helper) {
        console.log('here');
			var action = component.get("c.accDelCnf"); 
        action.setParams({
            accID : component.get("v.recordId")
        });
       
        action.setCallback(this, function(response){
            var state = response.getState();
   			console.log('state' + response.getState());
            
            var rows = [];
            var eachRow ;
            var rowjson;
            
			 if (state === "SUCCESS") { 
				var callResponse = response.getReturnValue();
                console.log('res----' + JSON.stringify(callResponse));
                if(JSON.stringify(callResponse).includes('error'))
                {
                    //alert('error---1---' + response.getState());
                    component.set("v.isError",true);
                    component.set("v.errorMsg", 'There is no information Available');
                }
                else
                {
                    var parsedjson = JSON.parse(callResponse);
                    console.log('parsedval' + parsedjson);
                    //var rows = [];
                    
                    for(var i=0;i<parsedjson.value.length;i++){
                      
                        var accID = parsedjson.value[i].SFContactID;
                        var snNumber = parsedjson.value[i].SalesOrderNumber;
                        var store =parsedjson.value[i].StoreID;
                        var transportID = parsedjson.value[i].TransportationOrderID;
                        var isCnf = parsedjson.value[i].IsConfirmed;
                        var begin = parsedjson.value[i].BegunTime;
                        var end = parsedjson.value[i].CompletedTime;
                        var loginuser =parsedjson.value[i].UserName ;
                        var routPass = parsedjson.value[i].RoutingPass;
                        var timchg = parsedjson.value[i].TimeChanged;
                        var prftctr = parsedjson.value[i].ProfitCenter;
                        var delDate =parsedjson.value[i].DeliverDate ;
                        var windOpn = parsedjson.value[i].CustomerWindowOpen;
                        var windCls = parsedjson.value[i].CustomerWindowClose;
                        var trkID =parsedjson.value[i].TruckID;
                        var cnfDateTime = parsedjson.value[i].ConfirmationDateTime;
                        var accShipto = parsedjson.value[i].AccountShipTo;
                        console.log('isCnf'+isCnf);
                        var isConfirmed;
                        if (isCnf == true){
                            isConfirmed = 'Confirmed';
                            console.log('isConfirmed'+isConfirmed);
                        }
                        else{
                            isConfirmed = 'Confirm';
                            console.log('isConfirmed1'+isConfirmed);
                        }
                        var timestampBegin = begin.split('.')[0];
                        var timestampend = end.split('.')[0];
                        console.log ('timestampWOSec---'+timestampBegin);
                        
                        eachRow = {
                            
                            salesorder_number:snNumber,
                            isCnfs:isCnf,
                            inCfrmd:isConfirmed,
                            begin1 : begin,
                            end1 : end,
                            begin:timestampBegin.slice(11,19),
                            end:timestampend.slice(11,19),
                            winOpn:windOpn,
                            winCls:windCls,
                            user:loginuser,
                            rPass:routPass,
                            cnfdt:cnfDateTime,
                            truckID:trkID,
                            storeId:store,
                            accspto:accShipto,
                            deliDate:delDate,
                            pCtr:prftctr  
                        }
                            rowjson = JSON.stringify(eachRow);
                             console.log('json' + rowjson);
                             rows.push(eachRow);
                    }
                      component.set('v.allrows',rows);
                      component.set('v.columns', [
                        {label: 'SALESORDER NUMBER', initialWidth: 125, fieldName: 'salesorder_number', type: 'text',class: 'table_test'},
                        //{label: 'CONFIRMATION STATUS', fieldName: 'inCfrmd', type: 'text'},
                        {label: 'Action', type: 'button', initialWidth: 125, typeAttributes:
                         //{label:'Confirm', title: 'Click to Edit', name: 'edit_status', disabled: {fieldName: 'inCfrmd'}, class: 'btn_next'}},
                        {label: {fieldName: 'inCfrmd'}, title: 'Click to Edit', name: 'edit_status', disabled: {fieldName: 'isCnfs'}, class: 'btn_next'}},
                        {label: 'PASS', fieldName: 'rPass', initialWidth: 70, type: 'number'},
                        {label: 'CUST BEGIN', initialWidth: 100, fieldName: 'winOpn', type: 'text'},
                        {label: 'CUST END', initialWidth: 100, fieldName: 'winCls', type: 'text'},
                        {label: 'USER NAME', initialWidth: 125, fieldName: 'user', type: 'text'},
                        {label: 'CONFIRMATION DATE/TIME', fieldName: 'cnfdt', type: 'text'},
                        // {label: 'Truck ID', fieldName: 'truckID', type: 'text'},
                        //{label: 'Store ID', fieldName: 'storeId', type: 'number'}
                    ]);
                    component.set("v.isError",false);
                }
            }
            else if(response.getState() === "ERROR") { 
                console.log('state' + response.getState());
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.errorMsg", 'There is no information Available');
                    }
                }else {
                    component.set("v.errorMsg", 'Request Failed!' );
                }
                
            }   
	
  			});
                $A.enqueueAction(action);                  
         },
    
    updateSalesOrder2: function(component, event, helper) {
       var soRoute = event.currentTarget.dataset.orderlineid;
       alert('soRoute--'+soRoute);
        var target = event.target;
       // alert('target--'+target);
        var i = target.getAttribute("data-row-index");
       // alert('i--'+i);
        var soData =component.get("v.allrows");
        
        var so;
        var ast;
        var pc;
        var tID;
        var dd;
        var rp;
        var cstop ;
        var cstcls;
        //alert('enetred');
        //for(var i=0;i<soData.length;i++){
            if(soRoute == soData[i].rPass){
                ast =  soData[i].accspto;
                pc =  soData[i].pCtr;   
                tID =  soData[i].truckID;   
                dd =  soData[i].deliDate;   
                rp =  soData[i].rPass;   
                cstop =  soData[i].begin1;
                cstcls =  soData[i].end1;
                so = soData[i].salesorder_number;
                
            }   
        //}
        alert('so--'+so);
        var timestampBegin = cstop.split('.')[0];
        var tsb0 = timestampBegin.slice(0,10);
        var tsb1 = timestampBegin.slice(11,19);
        var tsb3 = ' ';
        var tsb4 = tsb0.concat(tsb3, tsb1);
        
        var timestampend = cstcls.split('.')[0];
        var tse0 = timestampend.slice(0,10);
        var tse1 = timestampend.slice(11,19);
        var tse3 = ' ';
        var tse4 = tse0.concat(tse3, tse1);
        
        console.log('so---'+so);
        console.log('ast---'+ast);
        console.log('pc---'+pc);
        console.log('tID-'+tID);
        console.log('dd---'+dd);
        console.log('rp---'+rp);
        console.log('tsb4---'+tsb4);
        console.log('tse4---'+tse4);
        
        component.set('v.PopupsoId',so);
        
        var actionConfirm = component.get("c.soConfirmData");
        actionConfirm.setParams({"accShipto" : ast,
                                 "soNumber" : so,
                                 "profitCenter" : pc,
                                 "trkID" : tID,
                                 "delDate" : dd,
                                 "rPass" : rp,
                                 "custOpen" : tsb4,
                                 "custClose" : tse4
                                });
        
        actionConfirm.setCallback(this, function(response) {  
            
            var state = response.getState();
            
            if (state === "SUCCESS") {	 
                
                var callResponse = response.getReturnValue();
                console.log('callResponse----'+callResponse);
                for(var i=0;i<soData.length;i++){
                    if(so == soData[i].salesorder_number){
                        soData[i].isCnfs = true;
                        soData[i].inCfrmd = 'Confirmed';
                    }   
                }
                component.set("v.allrows",soData);
                
            }
            else if (response.getState() === "ERROR") {
                //alert('ERROR');
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": response.getError()[0].message, "type":"error"});
                errorToast.fire();                
            }
                
        });
        $A.enqueueAction(actionConfirm); 
    },
    
    navigateToRecord : function (component, event, helper) {
        var AccountId = component.get("v.recordId");
        var actiondata = component.get("c.getSalesOrderLineItem"); 
        var so = event.currentTarget.dataset.orderlineid;
        actiondata.setParams({
            "salesOrderId" : AccountId,
            "salesordernum" : so
        });
        //alert('enterd 2'+so);
        actiondata.setCallback(this, function(response){    
            var state = response.getState();
            //alert('state is '+state);
            
            if (state === "SUCCESS") {
                var callResponse = response.getReturnValue();
                var navEvt = $A.get("e.force:navigateToURL");
                // console.log('navEvt===>'+navEvt);
                navEvt.setParams({
                    "url": '/lightning/r/SalesOrder__x/' + callResponse + '/view'
                });
                navEvt.fire();
            }
});
        $A.enqueueAction(actiondata);
    }  
  
})