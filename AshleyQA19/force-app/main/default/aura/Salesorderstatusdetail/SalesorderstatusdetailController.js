({
	getSalesOrderData : function(component, event, helper) {
        console.log('here');
			var action = component.get("c.sodetailData"); 
        action.setParams({
            soID : component.get("v.recordId")
        });
       
        action.setCallback(this, function(response){
            var state = response.getState();
   			console.log('state' + response.getState());
            
            var rows = [];
            var eachRow ;
            var rowjson;
            
			 if (state === "SUCCESS") { 
				var callResponse = response.getReturnValue();
                console.log('res----' + JSON.stringify(callResponse));
                if(JSON.stringify(callResponse).includes('error'))
                {
                    //alert('error---1---' + response.getState());
                    component.set("v.isError",true);
                    component.set("v.errorMsg", 'There is no information Available');
                }
                else
                {
                    var parsedjson = JSON.parse(callResponse);
                    console.log('parsedval' + parsedjson);
                    //var rows = [];
                 
                    for(var i=0;i<parsedjson.value.length;i++){
                      
                        var snNumber = parsedjson.value[i].SaleOrderNo;
                        var timestamp =parsedjson.value[i].TimeStamp;
                        var loginuser = parsedjson.value[i].LoginUser;
                        var manager = parsedjson.value[i].ManagerApproved;
                        var changetype = parsedjson.value[i].ChangeType;
                        var orgVal = parsedjson.value[i].OriginalValue;
                        var updVal =parsedjson.value[i].UpdatedValue ;
                        var reason = parsedjson.value[i].ReasonCode;
                        var store = parsedjson.value[i].StoreID;
                        
                        var timestampWOSec = timestamp.split('.')[0];
                        var time1 = timestampWOSec.split('T')[1];
                        if (time1.includes('Z')){
                            time1 = time1.toString().replace('Z','');
                            }
                        
                        eachRow = {
                            salesorder_number:snNumber,
                            datstamp :timestampWOSec.split('T')[0],
                           // timeStamp:timestampWOSec.split('T')[1],
                            timeStamp:time1,
                            user:loginuser,
                            manager:manager,
                            type:changetype,
                            orVal:orgVal,
                            upVal:updVal,
                            reasonCode:reason,
                            storeId:store
                        }
                            rowjson = JSON.stringify(eachRow);
                             console.log('json' + rowjson);
                             rows.push(eachRow);
                    }
                      component.set('v.allrows',rows);
                      component.set('v.columns', [
                                      {label: 'ORDER NUMBER', fieldName: 'salesorder_number', type: 'text'},
                                      {label: 'Date', fieldName: 'datstamp', type: 'text'},
                                      {label: 'Time', fieldName: 'timeStamp', type: 'text'},
                                      {label: 'User', fieldName: 'user', type: 'text'},
                                      {label: 'Manager', fieldName: 'manager', type: 'text'},
                                      {label: 'Change Type', fieldName: 'type', type: 'text'},
                                      {label: 'Original Val', fieldName: 'orVal', type: 'text'},
                                      {label: 'Updated Val', fieldName: 'upVal', type: 'text'}
                                  
                     ]);
                    component.set("v.isError",false);
                }
            }
            else if(response.getState() === "ERROR") { 
                console.log('state' + response.getState());
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.errorMsg", 'There is no information Available');
                    }
                }else {
                    component.set("v.errorMsg", 'Request Failed!' );
                }
                
            }   
	
  			});
                $A.enqueueAction(action);                  
         },
    
    display : function(component, event, helper) {
    helper.toggleHelper(component, event);
  },

  displayOut : function(component, event, helper) {
   helper.toggleHelper(component, event);
  }
})