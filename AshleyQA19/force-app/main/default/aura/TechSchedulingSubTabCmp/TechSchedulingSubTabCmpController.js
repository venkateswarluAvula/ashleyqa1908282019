({
    doInit : function(component, event, helper) {
        //check cases PLI's
        console.log("doInit");
        var casePliObj = component.get("c.isValidTechSchedule");
        casePliObj.setParams({
            "recordId" : component.get("v.recordId")
        });
        // Add callback behavior for when response is received
        casePliObj.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                console.log("Tech Schedule validation: " + response.getReturnValue());
                if(response.getReturnValue() && response.getReturnValue().length > 0 ){
                    //either PLI's not exist or case address is empty
	                for(var j=0; j<response.getReturnValue().length; j++){
	                	var res = response.getReturnValue()[j];
	                	if(res == 'AddressErr'){
	                		component.set('v.noCsAddrExist', true);
	                	}
	                	if(res == 'PliErr'){
	                		component.set('v.noPliExist',true);
	                	}

	                	if(res.includes("Closed")) {
	                		var reqNoArray = res.split('|');
	                		component.set('v.message', reqNoArray[1]);
	                		component.set('v.closedCase',true);
	                	}

	                	if(res.includes("Not_Allowed")) {
	                		var reqNoArray = res.split('|');
	                		component.set('v.message', reqNoArray[1]);
	                		component.set('v.NotAllowed',true);
	                	}
	                }
                } else {
                    console.log("PLI's and address available");
                    var rerendTechSch = component.get('c.validateTechSchedule');
					$A.enqueueAction(rerendTechSch);
                }
            }
            else {
                console.log("Failed with state: " + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(casePliObj);
    },

	validateTechSchedule : function(component, event, helper) {
        //check sibling cases
        console.log("validateTechSchedule");
		component.set('v.noPliExist', false);
		component.set('v.noCsAddrExist', false);

        var recordId = component.get("v.recordId");
        var actionCaseObj = component.get("c.getRelatedCaseObj");
        actionCaseObj.setParams({
            "recordId" : recordId
        });
        // Add callback behavior for when response is received
        actionCaseObj.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                if( response.getReturnValue() && response.getReturnValue().length>0 ){
                    //records exist
                    component.set('v.SiblingsExist',true);
                    /*var data = response.getReturnValue();
                    console.log('data' + JSON.stringify(data));
                    for(var i=0;i<data.length;i++){
                        data[i].Id = '/'+data[i].Id;
                    }*/
                    console.log("Tech Scheduled: " + response.getReturnValue());
                    component.set('v.casesAre',response.getReturnValue());
                } else {
                	console.log("No Tech Scheduled");
                    helper.helperInit(component, event, helper);
                }
            }
            else {
                console.log("Failed with state: " + response.getState());
            }
        });
        $A.enqueueAction(actionCaseObj);
    },

    moveTotab : function(component, event, helper){
    	console.log("moveTotab");
        component.set('v.SiblingsExist',false);
        helper.helperInit(component, event, helper);
    },

    GobackToRecord : function (component, event, helper) {
    	console.log("GobackToRecord");
        component.set('v.SiblingsExist',false);
        component.set('v.noPliExist',false);
        component.set('v.noCsAddrExist', false);
        component.set('v.closedCase', false);
        $A.get('e.force:refreshView').fire();
     }
})