({
    showToast : function(type, title, component, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            type: type,
            title: title,
            message: message,
        });
        toastEvent.fire();
    },
    
    saveDelivery : function(component,helper){
	    var seletedItem=new Array();
        var hasError = false;
        var currentItem = component.find("setCurrentItemUpdate");
        var currentItemId=  currentItem.get("v.value");
        if(currentItem.get("v.checked")){
            seletedItem.push(currentItemId);
        }
        var allItems =  component.get("v.shoppingCartLineItems");
        var itemTotal=[];
        var totalSize;
        allItems.forEach(function(item){
            totalSize=itemTotal.push(item["item"]);
        });
        if(totalSize>1){
            component.find("setUpdate").forEach(function(element){
                 
                var itemId= element.get("v.value");
                if(itemId!=currentItemId ){
                if(element.get("v.checked")){
                    seletedItem.push(itemId);
                }
                 }
            });
        }else{
            var itemId= component.find("setUpdate").get("v.value");
            if(component.find("setUpdate").get("v.checked")){
                seletedItem.push(itemId);
            }  
        }
        if(seletedItem.length==0){
            helper.showToast("error", 'Please Select at least one item.', component,
                             'Please  Select at least one item.');   
            hasError = true;
        }
      
        if(hasError){
            return;
        }
        //alert('seletedItem--'+seletedItem);
        console.log('seletedItem--'+seletedItem);
        var action = component.get("c.saveDeliveryMode");
        var toastErrorHandler = component.find('toastErrorHandler');
        action.setParams({"selectedShippingWay" : component.get("v.selectedShippingWay"),
                          "accId":component.get('v.accountId'),
                          "deliveryType":component.get("v.deliveryType"),
                          "lineItemIdList" : seletedItem
                         });
        action.setCallback(this, function(response){
            toastErrorHandler.handleResponse(
                response, // handle failure
                function(response){ 
                    var rtnValue = response.getReturnValue();
                    if (rtnValue !== null && rtnValue=='Success') {
                        component.getEvent("NotifyParentCloseDeliveryModal").fire();  
                        var event = component.getEvent("shoppingCartLineItemEvent");
                        event.setParams({"action" : component.get("v.CART_ACTION_UPDATE_DELIVERY_MODE"), "lineItemIds" :seletedItem});
                        event.fire();
                    } else {
                        helper.showToast("error", 'Failed to Update Delivery Mode', component,
                                         rtnValue);                        
                    }
                },
                function(response, message){ // report failure
                    helper.showToast("error", 'Failed to Update Delivery Mode', component,
                                     message);
                }
            )            
        });        
        action.setBackground();
        $A.enqueueAction(action); 
    
        
	},
    
})