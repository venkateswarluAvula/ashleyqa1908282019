({
	doInit : function(component, event, helper) {
        
        var allItems = component.get("v.shoppingCartLineItems");
        var totalPrice=0;
        var DiscountPrice=0;
        var itemMap=[];
        var itemTotal=[];
        var totalSize;
        
        allItems.forEach(function(item){
            itemMap[item["item"]["Id"]]=item["item"];
            totalPrice +=  Number((item["item"]["List_Price__c"]*item["item"]["Quantity__c"]).toFixed(2));
            DiscountPrice +=  Number((item["item"]["Discount_Price__c"]*item["item"]["Quantity__c"]).toFixed(2));
            totalSize=itemTotal.push(item["item"]);
        });
        
        var SelectedTotalOriginalPrice =0;
        var SelectedTotalDiscountedPrice =0;
        var selectedAll = true;
        
        var currentItem = component.find("setCurrentItemUpdate");
        var currentItemId=  currentItem.get("v.value");
        var changeSelectedItems=[];
        if(currentItem.get("v.checked")){
            SelectedTotalOriginalPrice+=Number((itemMap[currentItemId]["List_Price__c"]*itemMap[currentItemId]["Quantity__c"]).toFixed(2));
            SelectedTotalDiscountedPrice+=Number((itemMap[currentItemId]["Discount_Price__c"]*itemMap[currentItemId]["Quantity__c"]).toFixed(2));
            changeSelectedItems.push(component.get("v.lineItem")["Id"]);
        }else{
            selectedAll = false;
        }
        if(totalSize>1){
            component.find("setUpdate").forEach(function(element){
                var itemId= element.get("v.value");
                if(itemId!=currentItemId && itemMap[itemId]["Product_SKU__c"][0]!='*'){
                    if(!element.get("v.checked")) {
                        selectedAll=false;
                    }else{
                        
                        changeSelectedItems.push(itemId);
                        SelectedTotalOriginalPrice+=itemMap[itemId]["List_Price__c"]*itemMap[itemId]["Quantity__c"];
                        SelectedTotalDiscountedPrice+=itemMap[itemId]["Discount_Price__c"]*itemMap[itemId]["Quantity__c"];
                    } 
                }
            });
        }else{
            var itemId= component.find("setUpdate").get("v.value");
            if(itemId!=currentItemId && itemMap[itemId]["Product_SKU__c"][0]!='*'){
                if(!component.find("setUpdate").get("v.checked")) {
                    selectedAll=false;
                }else{
                    changeSelectedItems.push(itemId);
                    SelectedTotalOriginalPrice+=itemMap[itemId]["List_Price__c"]*itemMap[itemId]["Quantity__c"];
                    SelectedTotalDiscountedPrice+=itemMap[itemId]["Discount_Price__c"]*itemMap[itemId]["Quantity__c"];
                } 
            }
        }
        var deliveryMode = component.get("v.lineItem")["Delivery_Mode__c"];
        component.set("v.selectedShippingWay",deliveryMode);
        component.find("shippingWay").forEach(function(element){
            if(element.get("v.value")==deliveryMode)
                element.set("v.checked", true);
            else
                element.set("v.checked", false);
        });  
        component.set("v.SelectedTotalOriginalPrice", (!$A.util.isUndefinedOrNull(SelectedTotalOriginalPrice) ? SelectedTotalOriginalPrice : 0) );
        component.set("v.SelectedTotalDiscountedPrice", (!$A.util.isUndefinedOrNull(SelectedTotalDiscountedPrice) ? SelectedTotalDiscountedPrice : 0) );
        component.find("setAllUpdate").set("v.checked", selectedAll);
        component.set("v.CartTotalOriginalPrice",totalPrice);
        component.set("v.CartTotalDiscountedPrice",DiscountPrice);
	},
    checkdeliverytype: function(component, event, helper){
     var updatedValue = event.getSource().get("v.value");
        if(updatedValue == 'PDI'){
        component.find("freetreshold").set("v.checked", false);
        component.set("v.deliveryType","PDI");  
        }else{
         component.find("Premiumdelivery").set("v.checked", false);   
         component.set("v.deliveryType","THD");  
        }
    },
    cancelModal: function(component, event, helper){
        component.getEvent("NotifyParentCloseDeliveryModal").fire();  
    },
    operateCheckbox: function(component, event, helper){
        var opp = component.get("v.opp");
        
        var allItems =  component.get("v.shoppingCartLineItems");
        var itemMap=[];
        var itemTotal=[];
        var totalSize;        
        
        allItems.forEach(function(item){
            
            itemMap[item["item"]["Id"]]=item["item"];
            totalSize=itemTotal.push(item["item"]);
        });
        
        var updatedValue = event.getSource().get("v.value");
        var checkFlag = event.getSource().get("v.checked");
        var      SelectedTotalOriginalPrice=component.get("v.SelectedTotalOriginalPrice");
        if(checkFlag){
            SelectedTotalOriginalPrice +=Number((itemMap[updatedValue]["List_Price__c"]*itemMap[updatedValue]["Quantity__c"]).toFixed(2));
        }else{
            SelectedTotalOriginalPrice -=Number((itemMap[updatedValue]["List_Price__c"]*itemMap[updatedValue]["Quantity__c"]).toFixed(2));    
        }
        component.set("v.SelectedTotalOriginalPrice",SelectedTotalOriginalPrice);
        
        var checkSelectedItem=[];
        var selectedAll = true;
        var currentItem = component.find("setCurrentItemUpdate");
        var currentItemId=  currentItem.get("v.value");
        
        if(!currentItem.get("v.checked")) {
            selectedAll=false;
        }else{
            checkSelectedItem.push(currentItemId);
        }
        
        if(totalSize>1){
            component.find("setUpdate").forEach(function(element){
                var itemId= element.get("v.value");
                if(itemId!=currentItemId && itemMap[itemId]["Product_SKU__c"][0]!='*'){
                    if(!element.get("v.checked")) {
                        selectedAll=false;
                    }else{
                        checkSelectedItem.push(itemId);
                    } 
                }
            });
        }else{
            var itemId= component.find("setUpdate").get("v.value");
            if(itemId!=currentItemId && itemMap[itemId]["Product_SKU__c"][0]!='*'){
                if(!component.find("setUpdate").get("v.checked")) {
                    selectedAll=false;
                }else{
                    checkSelectedItem.push(itemId);
                } 
            }
        }
        
        var SelectedDiscountPrice = 0;
        for (var itemId in itemMap) {
            if(checkSelectedItem.includes(itemId)){
                SelectedDiscountPrice += Number((itemMap[itemId]["Discount_Price__c"]*itemMap[itemId]["Quantity__c"]).toFixed(2));
                
            }
        }  
        component.set("v.SelectedTotalDiscountedPrice",SelectedDiscountPrice);
        component.find("setAllUpdate").set("v.checked", selectedAll);
        
    },
     selectAllItems: function(component, event, helper){
        var updatedValue = event.getSource().get("v.value");
        console.log('updatedValue'+updatedValue);
        var checkFlag = event.getSource().get("v.checked");
        console.log('checkFlag'+checkFlag);
        var allItems =  component.get("v.shoppingCartLineItems");
        var itemMap=[];
        var itemTotal=[];
        var totalSize;
        allItems.forEach(function(item){
            itemMap[item["item"]["Id"]]=item["item"];
            totalSize=itemTotal.push(item["item"]);
        });
        
        var SelectedTotalOriginalPrice =0;
        var SelectedTotalDiscountedPrice =0;
        var currentItem = component.find("setCurrentItemUpdate");
        var currentItemId=  currentItem.get("v.value");
        
        currentItem.set("v.checked", checkFlag);
        var changeSelectedItems=[];
        if(checkFlag){
            changeSelectedItems.push(component.get("v.lineItem")["Id"]);
        }
        
        if(totalSize>1){
            component.find("setUpdate").forEach(function(element){
                var itemId= element.get("v.value");
                if(itemMap[itemId]["Product_SKU__c"][0]!='*'){
                    element.set("v.checked", checkFlag);
                    if(checkFlag){
                        SelectedTotalOriginalPrice+=itemMap[itemId]["List_Price__c"]*itemMap[itemId]["Quantity__c"];
                        SelectedTotalDiscountedPrice+=itemMap[itemId]["Discount_Price__c"]*itemMap[itemId]["Quantity__c"]; 
                        changeSelectedItems.push(itemId);
                    }
                }
            });
        }else{
            var itemId=  component.find("setUpdate").get("v.value");
            if( itemMap[itemId]["Product_SKU__c"][0]!='*'){
                component.find("setUpdate").set("v.checked", checkFlag);
                if(checkFlag){
                    SelectedTotalOriginalPrice+=itemMap[itemId]["List_Price__c"]*itemMap[itemId]["Quantity__c"];
                    SelectedTotalDiscountedPrice+=itemMap[itemId]["Discount_Price__c"]*itemMap[itemId]["Quantity__c"];
                    changeSelectedItems.push(itemId);
                }
            }
        }
        component.set("v.SelectedTotalOriginalPrice", (!$A.util.isUndefinedOrNull(SelectedTotalOriginalPrice) ? SelectedTotalOriginalPrice : 0) );
        component.set("v.SelectedTotalDiscountedPrice", (!$A.util.isUndefinedOrNull(SelectedTotalDiscountedPrice) ? SelectedTotalDiscountedPrice : 0) );         
    },
     handleShippingWayRadioClick: function(component, event, helper){
        var currentChoosedShippingWay = event.getSource().get("v.value");
       if(currentChoosedShippingWay=='HD'){
          component.set("v.Tresholdpopup",true);
         component.set("v.againTresholdpopup",true);  
        }else{
          component.set("v.Tresholdpopup",false);  
            component.set("v.againTresholdpopup",false);  
        } 
        component.set("v.selectedShippingWay",currentChoosedShippingWay);
        component.find("shippingWay").forEach(function(element){
            if(element.get("v.value")==currentChoosedShippingWay)
            	element.set("v.checked", true);
            else
                element.set("v.checked", false);
        });
    },
     saveDelivery: function(component, event, helper){ 
       helper.saveDelivery(component, helper);
    
    },
    
})