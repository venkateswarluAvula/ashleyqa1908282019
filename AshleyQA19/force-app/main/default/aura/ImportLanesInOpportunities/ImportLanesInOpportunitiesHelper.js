({
    CSV2JSON: function (component,csv) {
        console.log('Incoming csv : ' + csv);
        var arr = []; 
        arr =  csv.split('\n');;
        console.log('arr = '+arr);
        arr.pop();
        var jsonObj = [];
        var headers = arr[0].split(',');
        for(var i = 1; i < arr.length; i++) {
            var data = arr[i].split(',');
            var obj = {};
            for(var j = 0; j < data.length; j++) {
                obj[headers[j].trim()] = data[j].trim();
                //console.log('obj headers = ' + obj[headers[j].trim()]);
            }
            jsonObj.push(obj);
        }
        var json = JSON.stringify(jsonObj);
        console.log('json = '+ json);
        return json;
	},
 /*   onLoad: function(component, event) {
        console.log('Lane onload');
        var oppId = component.get("v.recordId");
        console.log('@@@ oppId' + oppId);
        var action = component.get("c.fetchLane");
        action.setParams({
            "oppId" : oppId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('Lane response' + response.getReturnValue());
                component.set('v.ListOfLane', response.getReturnValue());
            }
            console.log('Lane to export' + component.get("v.ListOfLane"));
        });
        $A.enqueueAction(action);
    },
    */
    
    convertArrayOfObjectsToCSV : function(component,objectRecords){
        console.log('CSV Conversion');
        var csvStringResult, counter, keys, columnDivider, lineDivider;
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        columnDivider = ',';
        lineDivider =  '\n';
        keys = ['ADS_Lane_Destination_City__c','ADS_Lane_Destination_Market__c','ADS_Lane_Destination_State__c',
                          'ADS_Lane_Destination_Zip__c','ADS_Lane_Origin_City__c','ADS_Lane_Origin_Market__c',
                          'ADS_Lane_Origin_State__c','ADS_Lane_Origin_Zip__c','Awarded__c','Destination_Drop_Type__c',
                          'Destination_Region__c','Id','Minimum_Charge__c','Name','of_Trips_Per_Week__c','Opportunity__c',
                          'Origin_Drop_Type__c','Origin_Region__c'];
        csvStringResult = '';
        csvStringResult += keys.join(columnDivider);
        csvStringResult += lineDivider;
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0;
            for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }   
                csvStringResult += '"'+ objectRecords[i][skey]+'"'; 
                counter++;
            }
            csvStringResult += lineDivider;
        }
        return csvStringResult;        
    },
    CreateLane : function (component,jsonstr){
        console.log('@@@ jsonstr' + jsonstr);
        component.set("v.isLoading",true);
        var oppId = component.get("v.recordId");
        var isError = 'true';
        console.log('@@@ oppId' + oppId);
        var responseState = null;
        var action = component.get("c.insertData");
        action.setParams({
            "strfromlex" : jsonstr,
            "oppId" : oppId
        });
        action.setCallback(this, function(response) {
            var state = response.getReturnValue();
            responseState = state;
            console.log("state:--- " +  state);
            if (state === "SUCCESS") { 
                var isError = 'false';
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: "Success!",
                    message: "Successfully Imported Lanes!",
                    type: "success"
                });
                toastEvent.fire();
                component.set("v.isLoading",false);
                $A.get('e.force:refreshView').fire();         
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log('Error state-->',+responseState);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: "Warning!",
                    message: "Improper Data for Imported Lanes!",
                    type: "warning"
                });
                toastEvent.fire();
                
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    alert('Unknown');
                }
            }
            component.set("v.openModal",false);
        }); 
        $A.enqueueAction(action);
        console.log('selectedItems::::action2',+action);
   /*   if(action){
            console.log('state-->',+responseState);
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Success!",
                message: "Successfully Imported Lanes!",
                type: "success"
            });
                toastEvent.fire();
          }
            else if(responseState ==="ERROR"){
                console.log('state-->',+responseState);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: "WARNING!",
                    message: "Please provide all mandatory fields.",
                    type: "WARNING"
                });
                toastEvent.fire();
            }
            
        } */
           
    }
})