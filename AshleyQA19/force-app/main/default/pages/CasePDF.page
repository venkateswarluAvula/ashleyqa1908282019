<apex:page renderAs="PDF" applyBodyTag="false" controller="CasePDFController"> <!--  extensions="CasePDFController" -->

<head>
    <style>
        body, html{
            display: block;
            margin: 0px;
            padding: 0px;
            width: 100%;
            height: 100%;
            min-height: 100%;
            min-width: 100%;
            font-family: "arial";
            font-size: 0.95em;
        }
        .refImage{
            display: block;
            text-align: center;
            border-bottom: 1px solid;
        }
        .gridContainer{
            display: block;
        }
        .gridContainer>.gridLayout{
            display: block;
            width: 900px;
            margin: 0px auto;
            padding-left: 15px;
            padding-right: 15px;
        }
        .headerSection{
            display: block;
        }
        .headerSection>.logoSection,
        .headerSection>.headerContainer{
            display: inline-block;
            margin-right: -5px;
            vertical-align: bottom;
        }
        .headerSection>.logoSection>.logo{
            display: block;
            width: 120px;
            height: 30px;
            margin-right: 5px;
        }
        .headerSection>.headerContainer{
            width: calc(100% - 100px);
        }
        .headerSection>.headerContainer>.headerContent{
            display: block;
        }
        .headerContent .headerTextContainer{
            display: block;
        }
        .headerContent .headerTextContainer>div{
            display: inline-block;
            margin-right: -22px;
        }
        .headerContent .headerTextContainer>div.heading{
            width: 112mm;
            font-size: 1.5em;
            text-align: center;
            font-weight: bold;
        }
        .headerContent .headerTextContainer>div.dateSection{
            width: 200px;
            
        }
        .headerTextContainer>div.dateSection>ul{
            margin: 0px;
            padding: 0px;
            list-style: none;
        }
        .headerTextContainer>div.dateSection>ul>li{
            display: block;
        }
        .headerTextContainer>div.dateSection>ul>li>span{
            display: inline-block;
            width: 85px;
        }
        .headerTextContainer>div.dateSection>ul>li>span:first-child{
            text-align: right;
            padding: 8px;
            padding-right: 15px;
            font-weight: bold;  
        }
        .headerTextContainer>div.dateSection>ul>li>span:first-child:after{
            <!-- content: " : "; -->
        }
        .headerContent>div.blueBar{
            display: block;
            padding: 4px;
            background-color: #191971;
            color: #ba5439;
            font-size: 1.2em;
            text-align: right;
            font-weight: bold;
            font-style: italic;
        }
        .captionSection{
            display: block;
            padding-top: 10px;
            font-size: 1.2em;
            color: #191971;
        }
        .addressContainer{
            display: inline-block;
            vertical-align: top;
            width: 40%;
            margin-right: 8px;
        }
        hr {
            border-bottom: 4px solid #191971;
            color: #191971;
            
        }
        
        
    </style>
</head>
<body>
    <div class="gridContainer">
        <div class="gridLayout">
            <div class="headerSection">         
                <div class="logoSection">
                    <span class="logo"><img src="{!$Resource.AHSLogo}" width="100%"/></span>
                </div>
                <div class="headerContainer">
                    <div class="headerContent">
                        <div class="headerTextContainer">
                            <div class="heading">
                                Service Request Report
                            </div>                      
                            
                            <div class="dateSection">
                                <ul>
                                    <li>
                                        <span style="color: #191971;">
                                            <!-- From Date -->
                                        </span>
                                        <span>  </span>
                                    </li><li>
                                        <span style="color: #191971;">
                                            <!-- To Date -->
                                        </span>
                                        <span>  </span>
                                    </li>
                                </ul>
                            </div> 
                            
                        </div>  
                        
                        <div class="blueBar">
                            <span>Ashley CSM Reporting</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="captionSection">
                <b>**Order By: Date Opened, Request No</b>
            </div>
            
            <div style="padding-top: 30px; padding-bottom: 10px;">
                <table style="display: inline-block;">
                    <tr>
                        <td style="color: #191971;"><b>Request # &nbsp;</b></td>
                        <td>
                            <apex:outputText value="{!IF(caseRecord.Technician_ServiceReqId__c = null, caseRecord.Legacy_Service_Request_ID__c,caseRecord.Technician_ServiceReqId__c)}"></apex:outputText>
                        </td>
                        
                    </tr>
                </table>
            
                <table style="display: inline-block;">
                    <tr>
                        <td style="color: #191971; padding-left: 87px;"><b>Request Status &nbsp; </b></td>
                        <td>{!caseRecord.status}</td>
                        
                    </tr>
                </table>
            </div>
            
            
            <div style="display: block; width: 78%;">
                <table cellpadding="5" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                        <table style="border-collapse: collapse;" width="100%"> 
                            <tr>
                                <td style="border: 0.5px solid black; padding-left:5px;" height="100" width="60%">
                                    <spam style="color: #191971;"><b>Ship to:</b></spam>
                                    <div style="padding-left:20px;padding-top:5px; ">
                                        {!caseRecord.Address_Line_1__c}<br/>
                                        {!caseRecord.Address_Line_2__c}<br/>
                                        {!caseRecord.City__c},
                                        {!caseRecord.State__c},
                                        {!caseRecord.ZIP__c}
                                    </div>
                                </td>
                            </tr>
                        </table>
                        </td>
                        <td>
                        <table style="border-collapse: collapse;" width="100%"> 
                            <tr >
                                <td style="border: 0.5px solid black; padding-left:5px;" height="100">
                                    <spam style="color: #191971;"><b>Tech info:</b></spam><br/>
                                    <div style="padding-left:20px; ">
                                        <spam style="color: #191971;"><b>Company Name:</b></spam> <apex:outputText value="{!IF(caseRecord.Technician_Company__c = null, caseRecord.Company__c,caseRecord.Technician_Company__c)}"/> <br/>
                                        <spam style="color: #191971;"><b>Service Tech:</b></spam> <apex:outputText value="{!IF(caseRecord.TechnicianNameScheduled__c = null, caseRecord.Legacy_Technician__c,caseRecord.TechnicianNameScheduled__c)}"/><br/>
                                        <spam style="color: #191971;"><b>Ext. Stop Time:</b></spam> <apex:outputText value="{!IF(caseRecord.followup_Priority_EstimatedTime__c = null, caseRecord.Estimated_time_for_stop__c,caseRecord.followup_Priority_EstimatedTime__c)}"/><br/>
                                    </div>
                                    
                                    <spam style="color: #191971;"><b>Tech Scheduled Date:</b></spam> {!TechSchDate}<br/>
                                </td>
                                
                            </tr>
                        </table>
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="2">
                        <table style="border: 0.5px solid black;" width="100%" height="60"> 
                            <thead>
                                <tr >
                                    <th scope="col">
                                        <div class="slds-truncate" style="color: #191971;">Date Opened:</div>
                                    </th>
                                    <th scope="col">
                                        <div class="slds-truncate" style="color: #191971;">Follow Up Date:</div>
                                    </th>
                                    <th scope="col">
                                        <div class="slds-truncate" style="color: #191971;">Resolution:</div>
                                    </th>
                                    <th scope="col">
                                        <div class="slds-truncate" style="color: #191971;">Case Owner:</div>
                                    </th>
                                    <th scope="col">
                                        <div class="slds-truncate" style="color: #191971;">Orig. Created by:</div>
                                    </th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="slds-truncate">{!opendate}</div>
                                    </td>
                                    
                                    <td >
                                        <div class="slds-truncate">{!FolupDate}</div>
                                    </td>
                                    <td>
                                        <div class="slds-truncate">{!caseRecord.Type_of_Resolution__c}</div>
                                    </td>
                                    
                                    <td >
                                        <div class="slds-truncate">{!caseRecord.Owner.Name}</div>
                                    </td>
                                    <td >
                                        <div class="slds-truncate">{!caseRecord.CreatedBy.Name}</div>
                                    </td>
                                </tr>
                            </tbody>
                            
                        </table>
                        </td>
                    </tr>
                </table>
             </div>
            
            <spam style="color: #191971; padding-left:5px;"><b>Parts Info:</b></spam><br/>
            <div style="display: block; width: 50%;">
                <table style="border: 0px solid black;" width="100%" height="60">
                    <thead>
                    <tr >
                        <th scope="col">
                            <div class="slds-truncate" style="color: #191971;">Item #</div>
                        </th>
                        <th scope="col">
                            <div class="slds-truncate" style="color: #191971;">Tracking #</div>
                        </th>   
                    </tr>
                    </thead>
                    <tbody>
                        <apex:repeat value="{!pli}" var="p">
                            <tr>
                                <td>
                                    <div class="slds-truncate">{!p.Item_SKU__c }</div>
                                </td>
                                
                                <td >
                                    <div class="slds-truncate">{!p.Part_Order_Tracking_Number__c }</div>
                                </td>
                            </tr>
                        </apex:repeat>
                    </tbody>
                </table>
            </div>
            <br/>
            
            <spam style="color: #191971; padding-left:5px;"><b>Items Info:</b></spam><br/>
            <div style="display: block; width: 60%; padding-left:5px;">
                <table style="border: 0px solid black;" width="100%" height="60">
                    <thead>
                    <tr >
                        <th scope="col">
                            <div class="slds-truncate" style="color: #191971;">Item ID#</div>
                        </th>
                        <th scope="col">
                            <div class="slds-truncate" style="color: #191971;">Date Invoiced</div>
                        </th>
                        <th scope="col">
                            <div class="slds-truncate" style="color: #191971;">Legacy Service Request Item ID</div>
                        </th>
                        <th scope="col">
                            <div class="slds-truncate" style="color: #191971;">Item Serial Number</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        <apex:repeat value="{!pli}" var="p">
                            <tr>
                                <td>
                                    <div class="slds-truncate">{!p.Item_SKU__c }</div>
                                </td>
                                
                                <td>
                                    <div style="text-overflow:ellipsis;">
                                        <apex:outputText value=" {!DATEVALUE(p.Delivery_Date__c)}"> 
                                            
                                        </apex:outputText>
                                        
                                    </div>
                                </td>
                                <td>
                                    <div class="slds-truncate">{!p.Legacy_Service_Request_Item_ID__c }</div>
                                </td>
                                
                                <td>
                                    <div class="slds-truncate">{!p.Item_Serial_Number__c }</div>
                                </td>
                            </tr>
                        </apex:repeat>
                    </tbody>
                    
                </table>
            </div>
            
            <br/>
            
            <div>
                <table style="border: 0.px solid black;" width="77%">
                    <thead>
                        <tr >
                            <th>
                                <div style="color: #191971;">All Comments:</div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <apex:repeat value="{!Legacycomments}" var="c">
                            <tr>
                                <td>
                                    <div style="text-overflow:ellipsis;">
                                        <apex:outputText value="{0,date,MM/dd/yyyy}"> 
                                            <apex:param value="{!c.LastModifiedDate}" /> 
                                        </apex:outputText>
                                        : {!c.Comment__c}
                                    </div>
                                </td>
                            </tr>
                        </apex:repeat>    
                    </tbody>
                </table>
            </div>
            
            
             
            <!-- footer section -->
            <div style="position: absolute;bottom: 10px;left: 0;width: 100%;">
                <hr/>
                
                <table style="width:100%;">
                    <tr>
                        <td>Ashley Furniture HomeStore-Altamonte</td>
                        <td align="right">As of: {!opendate}</td>
                    </tr>
                </table>
            </div>
            
        </div>
    </div>
    
</body>

</apex:page>