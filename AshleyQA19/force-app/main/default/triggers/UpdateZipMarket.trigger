trigger UpdateZipMarket on ADS_Lane__c (before insert, before update) {
    
    if(Trigger.isBefore){
        if(Trigger.isInsert || Trigger.isUpdate){
            ADSLaneTriggerHelper.LoadZipMarket(Trigger.new);
        }    
    }
}