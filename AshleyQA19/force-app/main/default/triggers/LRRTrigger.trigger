trigger LRRTrigger on Legal_Review_Request__c (after insert, after Update, before update) {
    
     if(trigger.isbefore){
         
        if (trigger.isUpdate){
        
            LRRTriggerhandler.LRRApprovalProcess(Trigger.newMap.Keyset());
            
        }
    }
    
     if(trigger.isAfter){
        if (trigger.isUpdate){
            LRRTriggerhandler.SendingSMS(Trigger.newMap.Keyset());
        }
    }
    
}